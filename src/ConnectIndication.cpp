#include "ConnectIndication.h"
#include <stdio.h>

ConnectIndication::ConnectIndication(mm_data_buffer& buffer_) :
  buffer(buffer_),is_data_export_supported(false),data_export_port_number(0),monitor_ip_address("")
{ 
}

int ConnectIndication::parse_message() 
{
  buffer_initial_offset = buffer.get_offset();
  //read nomenclature
  buffer.read32((uint32_t *)&nomen);
  //read ROapdus
  buffer.read16((uint16_t *)&roapdus, sizeof(roapdus));
  if (roapdus.ro_type != ph_packet_defs::ROIV_APDU) {
    buffer.set_offset(buffer_initial_offset);
    return -1;
  }
  //read ROIVapdu
  buffer.read16((uint16_t *)&roivapdu, sizeof(roivapdu));
  if (roivapdu.command_type != ph_packet_defs::CMD_EVENT_REPORT) {
    buffer.set_offset(buffer_initial_offset);
    return -1;
  }
  //read  Event Report Argument
  buffer.read16((uint16_t *)&event_report_arg.managed_object,sizeof(event_report_arg.managed_object));
  buffer.read32((uint32_t *)&event_report_arg.event_time, sizeof(event_report_arg.event_time));
  buffer.read16((uint16_t *)&event_report_arg.event_type, sizeof(event_report_arg.event_type));
  buffer.read16((uint16_t *)&event_report_arg.length, sizeof(event_report_arg.length));
  if(event_report_arg.managed_object.m_obj_class != ph_packet_defs::NOM_MOC_VMS_MDS_COMPOS_SINGLE_BED ) {
    buffer.set_offset ( buffer_initial_offset);
    return -1;
  }
  if(event_report_arg.event_type != ph_packet_defs::NOM_NOTI_CONN_INDIC) {
    buffer.set_offset ( buffer_initial_offset);
    return -1;
  }
  //read attributeList
  attribute_list = new AttributeListClass(buffer);
  attribute_list->parse_message();

  /** Process Attributes **/
  std::vector<Attribute*>& attributes = attribute_list->get_attributes();
  std::vector<Attribute*>::iterator it;
  for(it=attributes.begin(); it != attributes.end(); it++){
    Attribute* attributeP = *it;
    OIDType type = attributeP->get_attribute_id();
    uint8_t *valueP = (uint8_t *)attributeP->get_valueP();
    switch(type) {
      case NOM_ATTR_PCOL_SUPPORT :  
        {
          ProtoSupport *protoP = (ProtoSupport *)valueP;
          valueP += sizeof(ProtoSupport);
          for(int i=0; i< protoP->count; i++){
            ProtoSupportEntry *protoEntryP = (ProtoSupportEntry *)valueP;
            valueP += sizeof(ProtoSupportEntry);
            if (protoEntryP -> appl_proto == AP_ID_DATA_OUT ||
                protoEntryP -> trans_proto == TP_ID_UDP) {
              printf("PROTO appl=%-12s trans=%-7s port_number=%05d options=0x%04x\n",
                  protoEntryP->appl_proto == AP_ID_DATA_OUT ? "DATA_OUT" : "NOT_DATA_OUT" ,
                  protoEntryP->trans_proto == TP_ID_UDP ? "UDP" : "NOT-UDP",
                  protoEntryP->port_number,
                  protoEntryP->options);
            }
            if (protoEntryP -> appl_proto == AP_ID_DATA_OUT &&
                protoEntryP -> trans_proto == TP_ID_UDP) {
              is_data_export_supported = true;
              data_export_port_number = protoEntryP->port_number;
            }
          }
          break;
        }
      case NOM_ATTR_NET_ADDR_INFO :
        {
          attributeP->set_word_size(1);
          valueP = (uint8_t *)attributeP->get_valueP();
          IpAddressInfo *ipaddrInfoP = (IpAddressInfo *)valueP;
          valueP += sizeof(IpAddressInfo);
          MACAddress mac = ipaddrInfoP->mac_address;
          IPAddress ip =  ipaddrInfoP->ip_address;
          char monitor_ip_addr_cstr[20];
          sprintf(monitor_ip_addr_cstr,"%d.%d.%d.%d",ip.value[0],ip.value[1],ip.value[2],ip.value[3]); 
          monitor_ip_address = monitor_ip_addr_cstr;
          IPAddress mask = ipaddrInfoP->subnet_mask;
          printf("Mac= %02x:%02x:%02x:%02x:%02x:%02x  IP addr= %d.%d.%d.%d subnet_mask=%d.%d.%d.%d\n" ,
              mac.value[0],mac.value[1],mac.value[2], mac.value[3],mac.value[4], mac.value[5],
              ip.value[0],ip.value[1],ip.value[2],ip.value[3],
              mask.value[0],mask.value[1],mask.value[2],mask.value[3] );
          break;
        } 

    }
  }
  printf("----------------------------------------\n");
  printf("----------------------------------------\n");
  printf("----------------------------------------\n");
  
  //printAttributes();

};

void ConnectIndication::printAttributes() {
  std::vector<Attribute*>& attributes = attribute_list->get_attributes();
  std::vector<Attribute*>::iterator it;
  for(it=attributes.begin(); it != attributes.end(); it++){
    Attribute* attributeP = *it;
    printf("Attribute type=0x%04x length=%d\n",
        attributeP->get_attribute_id(),attributeP->get_value_length());
    printf("Attribute value :");
    uint16_t *valueP = (uint16_t *)attributeP->get_valueP();
    for(int i=0; i<int(attributeP->get_value_length()/2); i++){
      printf(" %04x",*(valueP+i));
    }
    printf("\n");
  }



}

bool ConnectIndication::isDataExportSupported() {
  return is_data_export_supported;
};

int ConnectIndication::getDataExportPortNumber() {
  return data_export_port_number;
};

std::string ConnectIndication::getMonitorIpAddress() {
  return monitor_ip_address;
};


