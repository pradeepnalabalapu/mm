#include "MdsCreateEventResult.h"
#include <stdio.h>

MdsCreateEventResult::MdsCreateEventResult(mm_data_buffer& buffer_) :
  buffer(buffer_)
{ 

  sppdu.session_id = 0xE100;
  sppdu.p_context_id = 2;
}

int MdsCreateEventResult::build_message() 
{
  //set SPpdu
  //set ROapdus
  roapdus.ro_type = RORS_APDU;
  roapdus.length = 0; //to be filled up
  //set RORSapdu
  rorsapdu.length =0; //to be filled up
  //set event_report_arg
  event_report_arg.length=0; 

  //fill up lengths
  rorsapdu.length = sizeof(event_report_arg);
  roapdus.length = rorsapdu.length + sizeof(rorsapdu);
  roapdus.length = rorsapdu.length + sizeof(rorsapdu);

  buffer.write16((uint16_t *)&sppdu,sizeof(sppdu));
  buffer.write16((uint16_t *)&roapdus,sizeof(roapdus));
  buffer.write16((uint16_t *)&rorsapdu,sizeof(rorsapdu));
  buffer.write16((uint16_t *)&event_report_arg.managed_object,
      sizeof(event_report_arg.managed_object));
  buffer.write32((uint32_t *)&event_report_arg.event_time,
      sizeof(event_report_arg.event_time));
  buffer.write16((uint16_t *)&event_report_arg.event_type,
      sizeof(event_report_arg.event_type));
  buffer.write16((uint16_t *)&event_report_arg.length,
      sizeof(event_report_arg.length));

};






