#include "AbsoluteTimeClass.h"

AbsoluteTimeClass::AbsoluteTimeClass(AbsoluteTime at_) : buf(""),at(at_) { 
      if(at.month != 0xFF || at.day!=0xFF || at.century != 0xFF ||
          at.year != 0xFF || at.hour != 0xFF || at.minute != 0xFF ||
          at.second != 0xFF || at.sec_fractions != 0xFF )  {
        sprintf(buf,"%02x-%02x-%02x%02x %02x:%02x:%02x",
            at.month, at.day, at.century, at.year,
            at.hour, at.minute, at.second);
      } else {
        sprintf(buf,"N/A");
      }
};
const char* const AbsoluteTimeClass::toString() { 
  return buf;
};

