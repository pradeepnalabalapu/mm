#include "enums.h"

std::map<obj_class_enum_t,std::string> enums::ObjClassType_map;
std::map<AttributeId_enum_t,std::string> enums::AttributeId_map;
std::map<MetricCategory_enum_t,std::string> enums::MetricCategory_map;
std::map<SimpleColor_enum_t,std::string> enums::SimpleColor_map;
std::map<MeasurementState_enum_t,std::string> enums::MeasurementState_map;
std::map<LabelMap_enum_t,LabelMap_enum_t>  enums::WaveLabel_map;
std::map<MetricAccess_enum_t,std::string> enums::MetricAccess_map;
enums::_init enums::s_initializer;
