#include "RelativeTimeClass.h"


RelativeTimeClass :: RelativeTimeClass(RelativeTime rt_) : rt(rt_), buf("") {
  if(rt < 8000 ) {
    snprintf(buf,255,"%.2fms",rt*0.125);
  } else {
    snprintf(buf,255,"%.2fs",(rt/1000)*0.125);
  }
};
const char* const RelativeTimeClass ::toString(){
  return buf;
};

