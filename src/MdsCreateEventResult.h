#ifndef __MDSCREATEEVENTRESULT_H__
#define __MDSCREATEEVENTRESULT_H__
/************************************************************************
  MDSCreateEventResult ::=
  <SPpdu>
  <ROapdus (ro_type := ROIV_APDU)>
  <ROIVapdu (command_type := CMD_CONFIRMED_ACTION) >
  <EventReportArgument >
  <MDSCreateInfo>

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include <stdio.h>
#include <string.h>
using namespace ph_packet_defs;

class MdsCreateEventResult : public mm_message {
  mm_data_buffer& buffer;
  int buffer_initial_offset;


  AttributeListClass *attribute_list;
  public:
  SPpdu sppdu;
  ROapdus roapdus;
  RORSapdu rorsapdu;
  EventReportArgument event_report_arg;
  ManagedObjectId managed_object;
  MdsCreateEventResult(mm_data_buffer& buffer_) ;
  int build_message();

};

#endif // __MDSCREATEEVENTRESULT_H__
