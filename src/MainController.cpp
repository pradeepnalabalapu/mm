/*******************************************************/
/*  Author : Pradeep Nalabalapu                        */
/* This is the MainController for a monitor            */
/* One instance would be instantiated for each montior */
/*******************************************************/
#include <map>
#include "MainController.h"
#include "MeasurementStateClass.h"




MainController::MainController(string server_ip_addr,
     unsigned int _timeout_secs,
    unsigned int _timeout_usecs, unsigned int output_port, unsigned int debug_,
		string _client_ip_addr, unsigned int _client_port_num  )  
  : 
  	mbuf(buffer[0],BUFFER_SIZE),
    wmbuf(write_buffer[0],BUFFER_SIZE),ext_poll_num(0),
	 	output_port_num(output_port), debug(debug_),
		udpP(NULL), timeout_secs(_timeout_secs), timeout_usecs(_timeout_usecs)
{
  //int data_size, num_bytes_sent, num_bytes_recvd;
  int ret_val;
  bzero(buffer,BUFFER_SIZE);
	bzero(write_buffer,BUFFER_SIZE);
	osd = 0;

	//initialize_udp_agent(_client_ip_addr,_client_port_num);

	//setup out_server_addr
  bzero((char *)&out_server_addr,sizeof(out_server_addr));
  out_server_addr.sin_family = AF_INET;
  inet_pton(AF_INET,"localhost",&(out_server_addr.sin_addr));
  out_server_addr.sin_port = htons(output_port_num);
	connect_to_output_server();



};

void MainController::initialize_udp_agent(string _client_ip_addr,unsigned int _client_port_num) {
	client_port_num = _client_port_num;
	client_ip_addr = _client_ip_addr;
  bzero((char *) &cli_addr, sizeof(cli_addr));

	int server_port_num = client_port_num;
  cli_addr.sin_family = AF_INET;
  inet_pton(AF_INET,client_ip_addr.c_str(),&(cli_addr.sin_addr));
  cli_addr.sin_port = htons(client_port_num);
	if (udpP != NULL ) {
		delete(udpP);
	}
	udpP = new Udp(server_port_num) ;
  
  udpP->start();
  if (timeout_secs != 0 || timeout_usecs != 0) {
    if (int ret_val = udpP->settimeout(timeout_secs,timeout_usecs) < 0 ) {
      printf("ERROR : MainController : settimeout failed : returned %d",ret_val);
      exit(-1);
    } 
  }
	udpP->set_to_address(&cli_addr);

}

void MainController::connect_to_output_server() {
  int ostatus;
	printf("Inside connect_to_output_server\n");

  osd = socket(AF_INET, SOCK_STREAM, 0);
  if( connect(osd,(struct sockaddr *)&out_server_addr, sizeof(out_server_addr)) < 0 ) {
    fprintf(stderr, "Error connecting to output socket\n");
		osd = 0;
  }
	last_osd_check_timestamp = time(NULL);

}

int MainController::do_connect_indication() {
  int num_bytes_recvd, num_bytes_sent;
  Udp udp(CONNECT_INDICATION_SERVER_PORT,"");

  udp.start();

	bzero(buffer,BUFFER_SIZE);
  num_bytes_recvd = udp.receive(buffer, BUFFER_SIZE);

	if (num_bytes_recvd < 0 ) {
		perror("ERROR on recvfrom");
	} else if (debug & DEBUG_CONNECTION) {
		printf("Received %d bytes from client\n",num_bytes_recvd);
	}
  if (debug & DEBUG_DATA) {
	  printf("Client data = \n");
	  for(int i=0;i<num_bytes_recvd;i++) {
		  printf("%02x/%03d ",buffer[i],buffer[i]);
		  if ( (i%16) == 15 ) {
		 	  printf("\n");
		  }
	  }
	  printf("\n");
  }

  //mm_data_buffer mbuf(buffer[0],num_bytes_recvd);
  ConnectIndication ci(mbuf);
  ci.parse_message();
  client_port_num = ci.getDataExportPortNumber();
  client_ip_addr = ci.getMonitorIpAddress();
  //strncpy(client_ip_addr,monitor_ip.c_str(),monitor_ip.length());
	initialize_udp_agent(client_ip_addr,client_port_num);

	return 0;
}


int MainController::create_association() {

  int ret_val = sendAssocReq();
  if(ret_val != 0 ) {
    printf("ERROR : MainController::sendAssocReq returned error\n");
    //return ret_val;
		return disconnect();
  }
  
  printf("**********  AssociationResponse  ****************\n");
  int num_bytes_recvd = receive();
  //print_buffer(num_bytes_recvd);
  mm_data_buffer mbuf_assocResp(buffer[0],num_bytes_recvd);
  AssociationResponse ar(mbuf_assocResp);
  ar.parse_message();
  ar.printAttributes();
  printf("********** Done  AssociationResponse  ****************\n");

  printf("*************  MDS Create Event Report received ***********\n");
  num_bytes_recvd = receive();
  mm_data_buffer mbuf_ceReport(buffer[0],num_bytes_recvd);
  MdsCreateEventReport mds_ceReport(mbuf_ceReport);
  //print_buffer(num_bytes_recvd);
  mds_ceReport.parse_message();
  mds_ceReport.print_attributes();

  cur_managed_object = mds_ceReport.event_report_arg.managed_object;
  cur_sppdu  = mds_ceReport.sppdu;
  cur_invoke_id = mds_ceReport.roivapdu.invoke_id;

  RelativeTime event_time = mds_ceReport.event_report_arg.event_time;
  mbuf_ceReport.set_offset(0);
  MdsCreateEventResult mds_ceResult(mbuf_ceReport);
  mds_ceResult.sppdu= cur_sppdu;
  mds_ceResult.rorsapdu.command_type = mds_ceReport.roivapdu.command_type;
  mds_ceResult.rorsapdu.invoke_id = cur_invoke_id;
  mds_ceResult.event_report_arg = mds_ceReport.event_report_arg;
  mds_ceResult.build_message();
  size_t data_size = mbuf_ceReport.get_offset();
  int num_bytes_sent=udpP->send(buffer,data_size);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : send MdsCreateReportResult failed. expected to send %d bytes, sent %d bytes\n",(int)data_size,(int)num_bytes_sent);
    //return -1;
		return disconnect();
  } else {
    printf("Sent %d bytes of MdsCreateReportResult successfully\n",(int)num_bytes_sent);
  }

};

void MainController::setAttribute(Attribute& attribute) {
    AttributeId_enum_t attrId = (AttributeId_enum_t)attribute.get_attribute_id();
  	uint16_t length = attribute.get_value_length();
  	uint8_t  *dataP = (uint8_t *)attribute.get_rawDataP();
		void* valueP = attribute.get_valueP();
		time_t current_time = time(NULL);
		// Check periodically - if not connected to output server, reconnect
		if  (!osd) {
			if (current_time > (last_osd_check_timestamp + OUTPUT_SERVER_CHECK_PERIOD)) {
				connect_to_output_server();
			} else {
				printf("current_time=%d last_osd_check_timestamp=%d OUTPUT_SERVER_CHECK_PERIOD=%d\n",
						current_time,last_osd_check_timestamp,OUTPUT_SERVER_CHECK_PERIOD);
			}
		} 
    switch(attrId) {
      case NOM_ATTR_ID_HANDLE : 
        {
          unsigned int val;
          if(length == 2) {
             val = *(uint16_t *) valueP;
          } else if (length == 4) {
            val = ntohl(*(uint32_t *)dataP);
          } else {
            val = *(uint8_t *)dataP;
          }
          current_wave_info.id_handle = val;
          break;
        }
      case NOM_ATTR_ID_LABEL :
        {
          TextId textId = ntohl(*(uint32_t *)dataP);
          current_wave_info.label = (unsigned int) textId;
          break;
        }
      case NOM_ATTR_SA_SPECN:
        {
          uint16_t array_size = *(uint16_t *)valueP;
          uint8_t  sample_size = *((uint8_t *)dataP+2);
          uint8_t  significant_bits = *((uint8_t *)dataP+3); //TODO - ignored for now
          current_wave_info.array_size = array_size;
          current_wave_info.num_bytes = (sample_size + 7)>>3;
          current_wave_info.significant_bits = significant_bits;
          break;
        }
      case NOM_ATTR_SA_VAL_OBS:
        {
  
          SaObsValue saObjs;
          unsigned int physio_id = saObjs.physio_id = *(uint16_t *)valueP;
          saObjs.state = *((uint16_t *)valueP+1);
          uint16_t length = *((uint16_t *)valueP+2);
          std::string state_str = MeasurementStateClass(saObjs.state).toString();
          unsigned int num_bytes=1;
					if(wave_info_map.find(physio_id) == wave_info_map.end()) {
						current_wave_info.id = physio_id;
						wave_info_map[physio_id] = current_wave_info;
						num_bytes = current_wave_info.num_bytes;
					} else {
						num_bytes = wave_info_map[physio_id].num_bytes;
					}

					if (saObjs.state == 0  ) { // if data is valid TODO : check if necessary
						char *data_to_send = (char *)malloc(3+length); //2 bytes for physio_id, 1 byte for type
						*(uint16_t *)data_to_send = physio_id;
						*(data_to_send+3) = WAVE;
						memcpy(data_to_send+3,dataP+6,length);
						if (osd) {
							int ret_val = send(osd,data_to_send,3+length,MSG_NOSIGNAL);
							if (ret_val < 0 ) {
								printf("ERROR sending data to output server, resetting osd to 0\n");
								osd = 0;
							}
						}
						free(data_to_send);
					} else {
						printf("NOT SENDING DATA because saObjs.state = %d\n",saObjs.state);
					}

          break;
        }

    };
}


int MainController::single_poll_data_request() {

  /// ********** SinglePollDataRequest  *************

  mbuf.set_offset(0);
  mbuf.set_length(BUFFER_SIZE);

  SinglePollDataRequest spdReq(mbuf);
  spdReq.sppdu = cur_sppdu;
  spdReq.action_arg.managed_object = cur_managed_object;
  spdReq.build_message();
  size_t data_size = mbuf.get_offset();
  int num_bytes_sent=udpP->send(buffer,data_size);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : send SinglePollDataRequest failed. expected to send %d bytes, sent %d bytes\n",(int)data_size,(int)num_bytes_sent);
    //return -1;
		return disconnect();
  } else {
    printf("Sent %d bytes of SinglePollDataRequest successfully\n",num_bytes_sent);
  }

  /// ********** SinglePollDataRequest  *************
  SinglePollDataResult *spdResp;
  printf("************* SinglePollDataResult  *********\n");
  do {
    int num_bytes_recvd = receive();
    //print_buffer(num_bytes_recvd);
    mm_data_buffer mbuf_spdResp(buffer[0],num_bytes_recvd);
    printf("num_bytes_recvd=%d\n",num_bytes_recvd);
    spdResp = new SinglePollDataResult(mbuf_spdResp, *this);
    spdResp->parse_message();
  } while( spdResp->roapdus.ro_type == ROLRS_APDU);
  printf("************** DONE SinglePollDataResult *********\n");


};

int MainController::set_priority_list() {
  // ************  Set Priority List  **************
  mbuf.set_offset(0);
  mbuf.set_length(BUFFER_SIZE);

  SetPriorityListRequest splReq(mbuf);
  splReq.sppdu = cur_sppdu;
  splReq.roivapdu.invoke_id = cur_invoke_id;
  splReq.set_arg.managed_object = cur_managed_object;
  splReq.build_message();
  size_t data_size = mbuf.get_offset();
  int num_bytes_sent=udpP->send(buffer,data_size);
  printf("Buffer contents for SetPriorityListRequest => ");
  print_buffer(num_bytes_sent);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : send SetPriorityListRequest failed. expected to send %d bytes, sent %d bytes\n",(int)data_size,(int)num_bytes_sent);
    //return -1;
		//return disconnect();
  } else {
    printf("Sent %d bytes of SetPriorityListRequest successfully\n",num_bytes_sent);
  }


  int num_bytes_recvd = receive();
  print_buffer(num_bytes_recvd);

  // ************  Done Set Priority List  **************



  // ************  Get Priority List  **************
  mbuf.set_offset(0);
  mbuf.set_length(BUFFER_SIZE);

  GetPriorityListRequest gplReq(mbuf);
  gplReq.sppdu = cur_sppdu;
  gplReq.roivapdu.invoke_id = cur_invoke_id;
  gplReq.get_arg.managed_object = cur_managed_object;
  gplReq.build_message();
  data_size = mbuf.get_offset();
  num_bytes_sent=udpP->send(buffer,data_size);
  printf("Buffer contents for GetPriorityListRequest => ");
  print_buffer(num_bytes_sent);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : send GetPriorityListRequest failed. expected to send %d bytes, sent %d bytes\n",(int)data_size,(int)num_bytes_sent);
    //return -1;
		//return disconnect();
  } else {
    printf("Sent %d bytes of GetPriorityListRequest successfully\n",num_bytes_sent);
  }


  num_bytes_recvd = receive();
  print_buffer(num_bytes_recvd);

  // ************  Done Get Priority List  **************
};

int MainController::extended_poll_data_request() {

  /// ********** ExtendedPollDataRequest  *************

  wmbuf.set_offset(0);
  wmbuf.set_length(BUFFER_SIZE);

  ExtendedPollDataRequest epdReq(wmbuf);
  epdReq.sppdu = cur_sppdu;
  epdReq.action_arg.managed_object = cur_managed_object;
  {
    //Temporary request only metric value objects
    epdReq.poll_data_req.polled_attr_grp = 0;
    //epdReq.poll_data_req.polled_attr_grp = NOM_ATTR_GRP_VMO_DYN;
  }
  epdReq.poll_data_req.poll_number = ext_poll_num;
  ext_poll_num++;
  epdReq.build_message();
  size_t data_size = wmbuf.get_offset();
  print_buffer(data_size);
  int num_bytes_sent=udpP->send(write_buffer,data_size);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : send ExtendedPollDataRequest failed. expected to send %d bytes, sent %d bytes\n",(int)data_size,(int)num_bytes_sent);
    return -1;
  } else {
    printf("Sent %d bytes of ExtendedPollDataRequest successfully\n",num_bytes_sent);
  }

}

int MainController::extended_poll_data_result() {
  ExtendedPollDataResult *epdResp;
  printf("\n************* ExtendedPollDataResult  *********\n");
  do {
    int num_bytes_recvd = receive();
    char tmp[10] = { 1, 2, 3, 4, 5, 6, 7 ,8 ,9, 10};
    //print_buffer(num_bytes_recvd);
    mm_data_buffer mbuf_epdResp(buffer[0],num_bytes_recvd);
    //printf("num_bytes_recvd=%d\n",num_bytes_recvd);
    epdResp = new ExtendedPollDataResult(mbuf_epdResp,*this);
    epdResp->parse_message();
  } while( epdResp->roapdus.ro_type == ROLRS_APDU);
  printf("************** DONE ExtendedPollDataResult *********\n");
};


int MainController::whole_process() {

  create_association();

  single_poll_data_request();

  //set_priority_list();

  sleep(60);

  extended_poll_data_request();

  //print_buffer(num_bytes_recvd);
  return 0;
	
};


int MainController::sendAssocReq() {
	mbuf.set_offset(0);
  AssociationRequest assocReq(mbuf);
  assocReq.build_message();
  int data_size = mbuf.get_offset();
  printf("data_size=%d\n",data_size);
  printf("********* AssociationRequest ***********\n");
  //print_buffer(data_size);
  printf("********** Done AssociationRequest *********\n");

  int num_bytes_sent=udpP->send(buffer,data_size);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : sendto failed. expected to send %d bytes, sent %d bytes\n",data_size,num_bytes_sent);
    return -1;
  } else {
    printf("Sent %d bytes successfully\n",num_bytes_sent);
  }
  return 0;
}

int MainController::receive() {
  int num_bytes_recvd = udpP->receive(buffer, BUFFER_SIZE);

	if (num_bytes_recvd < 0 ) {
		printf("ERROR on recvfrom : returned %d\n",num_bytes_recvd);
    _Exit(-1);
	} else if (debug & DEBUG_CONNECTION) {
		printf("Received %d bytes from client\n",num_bytes_recvd);
	}
  return num_bytes_recvd;
};

void MainController::print_buffer(int num_bytes) {

	printf("Client data = \n");
	for(int i=0;i<num_bytes;i++) {
	  //printf("%02x/%03d ",buffer[i],buffer[i]);
	  printf("%02x ",buffer[i]);
	  if ( (i%16) == 15 ) {
	 	  printf("\n");
	  } else if( (i%8) == 7) {
      printf("||");
    }
  }
	printf("\n");
	printf("\n");

}
   
int MainController::disconnect() {
  printf("******** DISCONNECTING ************\n");
	wmbuf.set_offset(0);
	wmbuf.set_length(BUFFER_SIZE);
  ReleaseRequest releaseReq(wmbuf);
  releaseReq.build_message();
  int data_size = wmbuf.get_offset();
  printf("data_size=%d\n",data_size);

  int num_bytes_sent=udpP->send(write_buffer,data_size);
  if( num_bytes_sent != data_size ) {
    printf("ERROR : sendto failed. expected to send %d bytes, sent %d bytes\n",data_size,num_bytes_sent);
    return -1;
  } else {
    printf("Sent %d bytes successfully\n",num_bytes_sent);
  }

  if (osd) {
    close(osd);
  }

  printf("******** DONE DISCONNECTING ************\n\n\n");
  return 0;
}


MainController::~MainController() {
	if (udpP != NULL ) {
		delete(udpP);
	}
};
