#ifndef __RelativeTimeClass_H__
#define __RelativeTimeClass_H__
#include <stdio.h>
#include "ph_packet_defs.h"

using namespace ph_packet_defs;

class RelativeTimeClass {
  RelativeTime rt;
  char buf[256];
  public :
  RelativeTimeClass(RelativeTime rt_);
  const char* const toString(); 
  
};
#endif // __RelativeTimeClass_H__
