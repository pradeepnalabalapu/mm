#include "AttributeListClass.h"
#include <stdio.h>

AttributeListClass::AttributeListClass(mm_data_buffer& buffer_) : buffer(buffer_) {
};

AttributeListClass::~AttributeListClass() {
  for(std::vector<Attribute*>::iterator it=attributes.begin(); it != attributes.end(); ++it){
    delete(*it);
  }
}

int AttributeListClass::parse_message() {
  buffer_initial_offset = buffer.get_offset();
  buffer.read16((uint16_t *)&attribute_list, sizeof(attribute_list));
  attribute_count = attribute_list.count;
  attribute_length = attribute_list.length;
  for(int i=0; i < attribute_count; i ++) {
    Attribute* attributeP = new Attribute(buffer, 2/*word_size*/);
    attributes.push_back(attributeP);
  }
}

void AttributeListClass::print_attributes() {
  std::vector<Attribute*>::iterator it;
  for(it=attributes.begin(); it != attributes.end(); it++){
    (*it)->print(); //call each attributeP->print()
    printf("\n");
  }

};


