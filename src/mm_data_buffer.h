#ifndef __MM_DATA_BUFFER_H__
#define __MM_DATA_BUFFER_H__

#include <iostream>
#include <stdint.h>
#include <arpa/inet.h>
#define BUFFER_SIZE 1536
using namespace std;

class mm_data_buffer {
  uint8_t *data;
  size_t length;
  size_t offset;
  public :
  mm_data_buffer(uint8_t& buffer, size_t length_); 
  mm_data_buffer(uint8_t* buffer, size_t length_); 
    
  uint8_t &operator[](int i); 
  uint8_t *get_ptr();

  size_t read(uint8_t * data_struct, size_t len);
  size_t read32(uint32_t *data_struct, size_t len=4);
  size_t read16(uint16_t *data_struct, size_t len=2);
  size_t write(uint8_t * data_struct, size_t len);
  size_t write32(uint32_t *data_struct, size_t len=4);
  size_t write16(uint16_t *data_struct, size_t len=2);
  size_t get_offset() { 
    return offset;
  };

  void set_offset(size_t offset_) {
    offset=offset_;
  };
  void set_length(size_t length_){
    length=length_;
  }
  size_t get_length() { return length; }
  void print();
};

#endif // __MM_DATA_BUFFER_H__
