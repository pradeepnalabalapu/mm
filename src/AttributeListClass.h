#ifndef __ATTRIBUTELISTCLASS_H__
#define __ATTRIBUTELISTCLASS_H__

#include "ph_packet_defs.h"
#include "mm_data_buffer.h"
#include <cstdlib>
#include "Attribute.h"
#include <iostream>
#include <vector>
#include <functional>

using namespace ph_packet_defs;

class AttributeListClass {
  mm_data_buffer& buffer;
  int buffer_initial_offset;
  AttributeList attribute_list;
  int attribute_count; //count of attributes
  int attribute_length;
  std::vector<Attribute*> attributes;
  public :
  int get_attribute_count() { return attribute_count; };
  size_t get_attribute_length() { return attribute_length; };
  AttributeListClass(mm_data_buffer& buffer_);
  int parse_message();
  ~AttributeListClass();
  std::vector<Attribute*>& get_attributes() {
    return attributes;
  }
  void print_attributes();

};

#endif // __ATTRIBUTELISTCLASS_H__
