#ifndef __WAVE_INFO_H__
#define __WAVE_INFO_H__
typedef struct wave_info_struct{
  unsigned int id;
  unsigned int id_handle;
  unsigned int label;
  char name[256];
  unsigned int array_size;
  unsigned int num_bytes; //number of bytes for each value
  unsigned int significant_bits;
  unsigned int word_count;
} wave_info;
#endif// __WAVE_INFO_H__
