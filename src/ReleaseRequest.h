#ifndef __RELEASEREQUEST_H__
#define __RELEASEREQUEST_H__

/************************************************************************
	ReleaseRequestMessage ::=
 	<ReleaseReqSessionHeader>
 	<ReleaseReqSessionData>
	<ReleaseReqPresentationHeader>
 	<ReleaseReqUserData>
 	<ReleaseReqPresentationTrailer>



  typedef u_32 Nomenclature; //0x00_00_major_minor version
  typedef AttributeList ConnectIndInfo;

Attribute ID: NOM_ATTR_SYS_TYPE //system Type
Attribute Type : TYPE

Attribute ID: NOM_ATTR_PCOL_SUPPORT //Protocol support
Attribute Type: ProtoSupport

Attribute ID:  NOM_ATTR_LOCALIZN //System localization
Attribute Type : SystemLocal

Attribute ID:  NOM_ATTR_NET_ADDR_INFO
Attribute Type: IpAddressInfo

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include "AssociationRequest.h"

class ReleaseRequest : public AssociationRequest  {
  protected : 
    static uint8_t ReleaseReqPresentationHeader[20];
    static uint8_t ReleaseReqPresentationTrailer[4];

  public:
    ReleaseRequest(mm_data_buffer& buffer_) : AssociationRequest(buffer_) {
    };
    int build_message();
};


#endif // __RELEASEREQUEST_H__
