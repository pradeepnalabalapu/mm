#ifndef __ASSOCIATIONREQUEST_H__
#define __ASSOCIATIONREQUEST_H__

/************************************************************************
  AssociationRequestMessage ::=
  <AssocReqSessionHeader>
  <AssocReqSessionData>
  <AssocReqPresentationHeader>
  <AssocReqUserData>
  <AssorReqPresentationTrailer>

  AssocReqSessionHeader ::=
  <SessionHead (type := CN_SPDU_SI)>

  AssocReqUserData ::=
  <ASNLength>
  <UMDSEUserInfoStd>

  typedef u_32 Nomenclature; //0x00_00_major_minor version
  typedef AttributeList ConnectIndInfo;

Attribute ID: NOM_ATTR_SYS_TYPE //system Type
Attribute Type : TYPE

Attribute ID: NOM_ATTR_PCOL_SUPPORT //Protocol support
Attribute Type: ProtoSupport

Attribute ID:  NOM_ATTR_LOCALIZN //System localization
Attribute Type : SystemLocal

Attribute ID:  NOM_ATTR_NET_ADDR_INFO
Attribute Type: IpAddressInfo

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"

class AssociationRequest : public mm_message {
  protected:
 	static uint8_t AssocReqSessionData[14];
 	static uint8_t AssocReqPresentationHeader[133]; 

	static uint8_t AssocReqPresentationTrailer[16];


  mm_data_buffer& buffer;
  int buffer_initial_offset;
  ph_packet_defs::SessionHeader sessionHeader;
	ph_packet_defs::MDSEUserInfoStd userinfo;
  public:
  AssociationRequest(mm_data_buffer& buffer_) ;
  virtual int build_message();
};

#endif // __ASSOCIATIONREQUEST_H__
