//#include "ph_packet_defs.h"
#include "MainController.h"
#include "NetworkUtils.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <thread>
#include <getopt.h>
#include <signal.h>


//#define IF_NAME "enp3s0"
#define IF_NAME "enxf8cab869bc8c"
#define SERVER_IP_ADDR "10.1.1.10"
//#define CLIENT_IP_ADDR "10.1.1.11"
#define DEFAULT_CLIENT_PORT 24105
#define OUTPUT_SERVER_PORT 5000
#define TIMEOUT_SECS  10
#define TIMEOUT_USECS  0

void read_function(MainController& mc, int read_fd, int write_fd);

void write_function(MainController& mc);

extern int get_if_addrs_NUL();
bool seen_interrupt = false;

void sigint_handler (int dummy) {
 seen_interrupt = true;	
 printf("\n*************************************************\n");
 printf("      INTERRUPT SEEN");
 printf("\n*************************************************\n");
}

int main(int argc, char *argv[]) {
	int ret_val;
	unsigned int debug_level = 0;
  bool bootp = false;
  bool connectIndication=false;
  bool createAssociation=false;
  char c;
	string ifname_opt;
	string client_ip_addr;
	printf("Entered main\n");

  while( (c = getopt(argc, argv, "abci:") ) != (char)-1) {
    switch(c) {
      case 'a' :
        createAssociation = true;
        break;
      case 'b' :
        bootp = true;
        break;
      case 'c' :
        connectIndication = true;
        break;
			case 'i' :
				ifname_opt = string(optarg);
				break;
      case '?' :
        printf("getopt found error\n");
        exit(1);
      default :
        break;
    }
  }

	if( signal(SIGINT, sigint_handler) == SIG_ERR) {
		printf("************     WARNING          **********\n");
		printf("WARNING : Could not catch interrupt SIGINT\n\n");
		printf("*******************************************\n");
	}

	
	typedef std::vector<NetworkUtils::iface*> ifPVecType;
	ifPVecType ifPVec;
  ph_packet_defs::MACAddress mac;
	string ifname;



	if (!ifname_opt.empty()) {
		ifname = ifname_opt;
	} else  {
		NetworkUtils::get_ethernet_interfaces(ifPVec);
		if(ifPVec.size() == 1) {
			printf("Found one ethernet interface - %s\n",ifPVec[0]->name.c_str());
			ifname = ifPVec[0]->name;
		} else {
			printf("Could not detect one ethernet interface.\n Here's what we see.\n");
			NetworkUtils::list_network_interfaces();
			exit(0);
		}
	}

  if (bootp) {
    ret_val = NetworkUtils::bootp_server(BOOTP_SERVER_PORT, BOOTP_CLIENT_PORT, ifname,client_ip_addr, string(SERVER_IP_ADDR), debug_level|DEBUG_CONNECTION, mac);
    if (ret_val != 0 ) {
      printf("ERROR bootp_server didn't return success\n"); 
      return ret_val;
    }  else {
      printf("bootp_server successful\n");
    }

    printf("MAC address = %02x:%02x:%02x:%02x:%02x:%02x\n",
        mac.value[0],mac.value[1],mac.value[2],mac.value[3],
        mac.value[4],mac.value[5]);

    char cmd[80]; 
    sprintf(cmd,"arp -s %s %02x:%02x:%02x:%02x:%02x:%02x",client_ip_addr.c_str(),
        mac.value[0],mac.value[1],mac.value[2],mac.value[3],
        mac.value[4],mac.value[5]);
    ret_val = system(cmd);
    if (ret_val != 0 ){
      printf("ERROR arp command (%s) failed with return value %d\n",
          cmd,ret_val);
      return ret_val;
    } else {
			printf("ARP successful\n");
		}
  } 

	MainController mc(SERVER_IP_ADDR,0,0,OUTPUT_SERVER_PORT,debug_level,client_ip_addr,DEFAULT_CLIENT_PORT);
	if (bootp || connectIndication) {

		ret_val = mc.do_connect_indication();
		if (ret_val != 0 ) {
			printf("ERROR connect_indication_server failed\n"); 
			return ret_val;
		} else {
			printf("Connect Indication successful\n");
		}
		sleep(2); //sleep 2 seconds
	}  else if(!ifname.empty()) {
		client_ip_addr = NetworkUtils::get_ip_addr(ifname);
		mc.initialize_udp_agent(client_ip_addr,DEFAULT_CLIENT_PORT);
	} else {
		printf("ERROR : Without -b and -c flags, please specify interface name using -i\n");
		return -1;
	}


	if (bootp || connectIndication || createAssociation) {
		mc.create_association();
		mc.single_poll_data_request();
		mc.set_priority_list();
	}

	if (bootp || connectIndication) {
		sleep(60);
	}

	std::thread t(&write_function, std::ref(mc));

	ret_val = mc.extended_poll_data_request();
	do {
		ret_val = mc.extended_poll_data_result();
	} while(!seen_interrupt);
	t.join();

	mc.disconnect();

}

void write_function(MainController& mc) {
	do { //wait until interrupt
		int ret_val = mc.extended_poll_data_request();
		if (ret_val < 0 ) {
			exit(1);
		}
		sleep(50); //wait a minute
	} while(!seen_interrupt);
}
