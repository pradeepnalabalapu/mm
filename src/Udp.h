#ifndef __Udp_H__
#define __Udp_H__
#include <stdlib.h>
#include <string>
#include <netinet/in.h>
using namespace std;

class Udp
{
    int m_lsd;
    int m_port;
    bool m_listening;
		struct sockaddr_in to_address;
		string m_address;

  public:
    Udp(int port, string address=string(""));
    ~Udp();

    int        start();
		ssize_t receive(uint8_t *buffer, size_t len, struct sockaddr_in* client_address=NULL);
		void set_to_address(struct sockaddr_in* to_addressP);
		ssize_t send(uint8_t *buffer, size_t len,struct sockaddr_in* to_addressP=NULL);
    int enable_broadcast();
    int bind_to_device(const char* if_name);
    int settimeout(int secs, int usecs);

  private :
    void error(const char* msg)
    {
      perror(msg);
      exit(1);
    }
    Udp() {}


};

#endif // __Udp_H__
