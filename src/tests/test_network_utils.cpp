#include "NetworkUtils.h"

typedef std::vector<NetworkUtils::iface*> ifPVecType;

ifPVecType ifPVec;

void free_memory() {
	for(ifPVecType::iterator it=ifPVec.begin(); it != ifPVec.end(); ++it) {
		if( (*it) != NULL) {
			delete(*it);
		}
	}

}


int main(int argc, char **argv) {
	NetworkUtils::get_ethernet_interfaces(ifPVec);
	for(ifPVecType::iterator it=ifPVec.begin(); it != ifPVec.end(); ++it) {
		if(*it != NULL) {
			char mac_addr_cstr[18];
			unsigned char* mac_addrP = (unsigned char*)(**it).mac_addr.value;
			snprintf(mac_addr_cstr,18,"%02x:%02x:%02x:%02x:%02x:%02x",
					mac_addrP[0],mac_addrP[1], mac_addrP[2],
					mac_addrP[3],mac_addrP[4], mac_addrP[5]);
			printf("%s\n\tmac_addr = %s\t\tip_addr = %s\n", (**it).name.c_str(),
					mac_addr_cstr,(*it)->ip_addr.c_str());
		}
	}

	free_memory();
}

