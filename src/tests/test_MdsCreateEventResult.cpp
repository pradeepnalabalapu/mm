#include "MdsCreateEventResult.h"
#include <stdio.h>

uint8_t buffer[1536];
int main(int argc, char *argv[]) {
	mm_data_buffer mbuf(buffer[0], 1536);
	MdsCreateEventResult mds_ceResult(mbuf);
  mds_ceResult.sppdu.session_id = 0xE100;
  mds_ceResult.sppdu.p_context_id = 2;
  mds_ceResult.rorsapdu.command_type = CMD_CONFIRMED_EVENT_REPORT;
  mds_ceResult.rorsapdu.invoke_id = 1;
  mds_ceResult.event_report_arg.managed_object.m_obj_class = NOM_MOC_VMS_MDS ;
  mds_ceResult.event_report_arg.managed_object.m_obj_inst.context_id = 0 ;
  mds_ceResult.event_report_arg.managed_object.m_obj_inst.handle = 0 ;
  mds_ceResult.event_report_arg.event_time = 4736768;
  mds_ceResult.event_report_arg.event_type = NOM_NOTI_MDS_CREAT;
	mds_ceResult.build_message();
  mbuf.print();
  printf("data_size=%d=0x%x\n",mbuf.get_offset(),mbuf.get_offset());
};
