#include "Attribute.h"
#include "AttributeListClass.h"
#include "test_AttributeData.h"
#include "mm_data_buffer.h"
#include "ConnectIndication.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
  int ret_val;

  mm_data_buffer mbuf(test_AttributeData[0], test_AttributeDataLen);
  AttributeListClass al(mbuf);
  al.parse_message();

  std::vector<Attribute*>& attributes = al.get_attributes();
  for(std::vector<Attribute*>::iterator it= attributes.begin(); it != attributes.end(); ++it){
    Attribute* attributeP = *it;
    printf("Attribute type=0x%04x length=%d\n",
        attributeP->get_attribute_id(),attributeP->get_value_length());
    printf("Attribute value :");
    uint16_t *valueP = (uint16_t *)attributeP->get_valueP();
    for(int i=0; i<int(attributeP->get_value_length()/2); i++){
      printf(" %04x",*(valueP+i));
    }
    printf("\n");
  }

}
