#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include "test_ConnectIndicationData.h"
#include "ConnectIndication.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
  int ret_val;

  mm_data_buffer mbuf(test_ConnectIndicationData[0], test_ConnectIndicationDataLen);

  ConnectIndication ci(mbuf);
  ci.parse_message();
  ci.printAttributes();
};
 
