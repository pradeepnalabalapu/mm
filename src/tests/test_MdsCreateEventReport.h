#ifndef __TEST_MDSCREATEEVENTREPORT_H__
#define __TEST_MDSCREATEEVENTREPORT_H__

#include "ph_packet_defs.h"
#include <stdint.h>

uint8_t data[] = {
0xe1, 0x00, 0x00, 0x02, 0x00, 0x01, 0x01, 0x12, 
0x00, 0x01, 0x00, 0x01, 0x01, 0x0c, 0x00, 0x21, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x48, 0x00, 
0x0d, 0x06, 0x00, 0xfe, 0x00, 0x21, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x11, 0x00, 0xf4, 0x09, 0x84, 
0x00, 0x08, 0x00, 0x06, 0x00, 0x09, 0xfb, 0x37, 
0x76, 0x3c, 0x09, 0x86, 0x00, 0x04, 0x00, 0x01, 
0x11, 0x4d, 0x09, 0x1d, 0x00, 0x02, 0x00, 0x06, 
0x09, 0x28, 0x00, 0x12, 0x00, 0x08, 0x50, 0x68, 
0x69, 0x6c, 0x69, 0x70, 0x73, 0x00, 0x00, 0x06, 
0x4d, 0x38, 0x30, 0x30, 0x30, 0x00, 0x09, 0x48, 
0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x09, 0x37, 
0x00, 0x08, 0x06, 0x32, 0x06, 0x32, 0x00, 0x01, 
0x00, 0x0b, 0x09, 0x46, 0x00, 0x02, 0x40, 0x00, 
0x09, 0x0d, 0x00, 0x02, 0x00, 0x03, 0x09, 0x35, 
0x00, 0x02, 0x00, 0x02, 0x09, 0x82, 0x00, 0x02, 
0x00, 0x02, 0x09, 0x0c, 0x00, 0x02, 0x00, 0xc8, 
0xf1, 0xfa, 0x00, 0x14, 0x00, 0x01, 0x00, 0x10, 
0x00, 0x01, 0x00, 0x0c, 0x00, 0x06, 0xff, 0xff, 
0xff, 0xff, 0x00, 0x06, 0xff, 0xff, 0xff, 0xff, 
0x09, 0xa7, 0x00, 0x02, 0x00, 0x06, 0x09, 0x1e, 
0x00, 0x24, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x87, 
0x00, 0x08, 0x19, 0x97, 0x01, 0x16, 0x06, 0x35, 
0x21, 0x00, 0x09, 0x8f, 0x00, 0x04, 0x00, 0x01, 
0x48, 0x00, 0x09, 0x85, 0x00, 0x34, 0x00, 0x01, 
0x00, 0x30, 0x01, 0x02, 0x00, 0x2c, 0x00, 0x05, 
0x00, 0x28, 0x00, 0x01, 0x00, 0x21, 0x00, 0x00, 
0x00, 0x01, 0x00, 0x01, 0x00, 0x06, 0x00, 0x00, 
0x00, 0xc9, 0x00, 0x01, 0x00, 0x09, 0x00, 0x00, 
0x00, 0x3c, 0x00, 0x01, 0x00, 0x2a, 0x00, 0x00, 
0x00, 0x01, 0x00, 0x01, 0x00, 0x36, 0x00, 0x00, 
0x00, 0x01 };



#endif // __TEST_MDSCREATEEVENTREPORT_H__
