#include "test_MdsCreateEventReport.h"
#include "MdsCreateEventReport.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	mm_data_buffer mbuf(data[0], 282);
	MdsCreateEventReport mds_cer(mbuf);
  mds_cer.parse_message();
  mds_cer.print_attributes();
  printf("data_size=%d=0x%x\n",mbuf.get_offset(),mbuf.get_offset());
};
