#include "bootp.h"
#include "connect_indication_server.h"
#include "MainController.h"
#include <unistd.h>
#include <getopt.h>

#define IF_NAME "enp3s0"
#define SERVER_IP_ADDR "10.1.1.10"
#define CLIENT_IP_ADDR "10.1.1.11"
#define DEFAULT_CLIENT_PORT 24105
#define TIMEOUT_SECS  10
#define TIMEOUT_USECS  0


int main(int argc, char *argv[]) {
	int ret_val;
	struct in_addr client_addr; 
	unsigned int debug_level = 3;
  char client_ip_addr[20];
  unsigned int client_port;
  int bootp = 0;
  int connectIndication=0;
  int createAssociation=0;
  char c;

  while( (c = getopt(argc, argv, "abc") ) != -1) {
    switch(c) {
      case 'a' :
        createAssociation = 1;
        break;
      case 'b' :
        bootp = 1;
        break;
      case 'c' :
        connectIndication = 1;
        break;
      case '?' :
        printf("getopt found error\n");
        exit(1);
      default :
        break;
    }
  }


  ret_val = set_ip_address(IF_NAME , SERVER_IP_ADDR);
  if (ret_val != 0 ) {
    printf("ERROR : set_ip_address returned incorrect status\n");
    exit(ret_val);
  }


 
	bzero((char *) &client_addr, sizeof(client_addr));
  ph_packet_defs::MACAddress mac;

  if (bootp) {
    ret_val = bootp_server(BOOTP_SERVER_PORT, BOOTP_CLIENT_PORT, IF_NAME,CLIENT_IP_ADDR, SERVER_IP_ADDR, debug_level, mac);
    if (ret_val != 0 ) {
      printf("ERROR bootp_server didn't return success\n"); 
      return ret_val;
    }  else {
      printf("bootp_server successful\n");
    }

    printf("MAC address = %02x:%02x:%02x:%02x:%02x:%02x\n",
        mac.value[0],mac.value[1],mac.value[2],mac.value[3],
        mac.value[4],mac.value[5]);

    char cmd[80]; 
    sprintf(cmd,"arp -s %s %02x:%02x:%02x:%02x:%02x:%02x",CLIENT_IP_ADDR,
        mac.value[0],mac.value[1],mac.value[2],mac.value[3],
        mac.value[4],mac.value[5]);
    ret_val = system(cmd);
    if (ret_val != 0 ){
      printf("ERROR arp command (%s) failed with return value %d\n",
          cmd,ret_val);
      return ret_val;
    }
  } 

  if (bootp || connectIndication) {

    ret_val = connect_indication_server_uv(CONNECT_INDICATION_SERVER_PORT, &client_port,  IF_NAME, client_ip_addr, debug_level);
    if (ret_val != 0 ) {
      printf("ERROR connect_indication_server failed\n"); 
      return ret_val;
    } 

    sleep(2); //sleep 2 seconds
  } else {
    client_port = DEFAULT_CLIENT_PORT;
    strcpy(client_ip_addr,CLIENT_IP_ADDR);
  }

  return 0;


  unsigned int server_port = client_port;

  printf("client_port=%d\n",client_port);
  MainController mc(server_port,SERVER_IP_ADDR,client_port,CLIENT_IP_ADDR,TIMEOUT_SECS, TIMEOUT_USECS,debug_level);

  if (bootp || connectIndication || createAssociation) {
    mc.create_association();
    mc.single_poll_data_request();
    mc.set_priority_list();
  }

  sleep(60);

  ret_val = mc.extended_poll_data_request();
  sleep(60);
  //ret_val = mc.extended_poll_data_rest();
  return ret_val;

}


