#include "SinglePollDataRequest.h"
#include <stdio.h>

uint8_t buffer[1536];
int main(int argc, char *argv[]) {
	mm_data_buffer mbuf(buffer[0], 1536);
	SinglePollDataRequest spdReq(mbuf);
	spdReq.build_message();
  mbuf.print();
  printf("data_size=%d=0x%x\n",mbuf.get_offset(),mbuf.get_offset());
};
