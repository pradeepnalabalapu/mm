#ifndef __enums_H__
#define __enums_H__
#include "ph_packet_defs.h"
#include <map>
#include <string>
using namespace ph_packet_defs;
class enums {
	public :
		static std::map<obj_class_enum_t,std::string> ObjClassType_map;
	 	static std::map<obj_class_enum_t,std::string> create_obj_class_enum_map () {
		 	std::map<obj_class_enum_t,std::string> m;
			printf("Executing create_obj_class_enum_map\n");
			m[NOM_MOC_VMO]=std::string("NOM_MOC_VMO");
			m[NOM_MOC_VMO_METRIC_NU]=std::string("NOM_MOC_VMO_METRIC_NU");
			m[NOM_MOC_VMO_METRIC_SA_RT]=std::string("NOM_MOC_VMO_METRIC_SA_RT");
			m[NOM_MOC_VMS_MDS]=std::string("NOM_MOC_VMS_MDS");
			m[NOM_MOC_VMS_MDS_COMPOS_SINGLE_BED]=std::string("NOM_MOC_VMS_MDS_COMPOS_SINGLE_BED");
			m[NOM_MOC_VMS_MDS_SIMP]=std::string("NOM_MOC_VMS_MDS_SIMP");
			m[NOM_MOC_BATT]=std::string("NOM_MOC_BATT");
			m[NOM_MOC_PT_DEMOG]=std::string("NOM_MOC_PT_DEMOG");
			m[NOM_MOC_VMO_AL_MON]=std::string("NOM_MOC_VMO_AL_MON");
			m[NOM_ACT_POLL_MDIB_DATA]=std::string("NOM_ACT_POLL_MDIB_DATA");
			m[NOM_NOTI_MDS_CREAT]=std::string("NOM_NOTI_MDS_CREAT");
			m[NOM_NOTI_CONN_INDIC]=std::string("NOM_NOTI_CONN_INDIC");
			m[NOM_DEV_METER_CONC_SKIN_GAS]=std::string("NOM_DEV_METER_CONC_SKIN_GAS");
			m[NOM_DEV_METER_FLOW_BLD]=std::string("NOM_DEV_METER_FLOW_BLD");
			m[NOM_DEV_ANALY_CONC_GAS_MULTI_PARAM_MDS]=std::string("NOM_DEV_ANALY_CONC_GAS_MULTI_PARAM_MDS");
			m[NOM_DEV_ANALY_CONC_GAS_MULTI_PARAM_VMD]=std::string("NOM_DEV_ANALY_CONC_GAS_MULTI_PARAM_VMD");
			m[NOM_DEV_METER_CONC_SKIN_GAS_MDS]=std::string("NOM_DEV_METER_CONC_SKIN_GAS_MDS");
			m[NOM_DEV_MON_PHYSIO_MULTI_PARAM_MDS]=std::string("NOM_DEV_MON_PHYSIO_MULTI_PARAM_MDS");
			m[NOM_DEV_PUMP_INFUS_MDS]=std::string("NOM_DEV_PUMP_INFUS_MDS");
			m[NOM_DEV_SYS_PT_VENT_MDS]=std::string("NOM_DEV_SYS_PT_VENT_MDS");
			m[NOM_DEV_SYS_PT_VENT_VMD]=std::string("NOM_DEV_SYS_PT_VENT_VMD");
			m[NOM_DEV_SYS_MULTI_MODAL_MDS]=std::string("NOM_DEV_SYS_MULTI_MODAL_MDS");
			m[NOM_DEV_SYS_MULTI_MODAL_VMD]=std::string("NOM_DEV_SYS_MULTI_MODAL_VMD");
			m[NOM_DEV_SYS_VS_CONFIG_MDS]=std::string("NOM_DEV_SYS_VS_CONFIG_MDS");
			m[NOM_DEV_SYS_VS_UNCONFIG_MDS]=std::string("NOM_DEV_SYS_VS_UNCONFIG_MDS");
			m[NOM_DEV_ANALY_SAT_O2_VMD]=std::string("NOM_DEV_ANALY_SAT_O2_VMD");
			m[NOM_DEV_ANALY_FLOW_AWAY_VMD]=std::string("NOM_DEV_ANALY_FLOW_AWAY_VMD");
			m[NOM_DEV_ANALY_CARD_OUTPUT_VMD]=std::string("NOM_DEV_ANALY_CARD_OUTPUT_VMD");
			m[NOM_DEV_ANALY_PRESS_BLD_VMD]=std::string("NOM_DEV_ANALY_PRESS_BLD_VMD");
			m[NOM_DEV_ANALY_RESP_RATE_VMD]=std::string("NOM_DEV_ANALY_RESP_RATE_VMD");
			m[NOM_DEV_CALC_VMD]=std::string("NOM_DEV_CALC_VMD");
			m[NOM_DEV_ECG_VMD]=std::string("NOM_DEV_ECG_VMD");
			m[NOM_DEV_METER_CONC_SKIN_GAS_VMD]=std::string("NOM_DEV_METER_CONC_SKIN_GAS_VMD");
			m[NOM_DEV_EEG_VMD]=std::string("NOM_DEV_EEG_VMD");
			m[NOM_DEV_METER_TEMP_BLD_VMD]=std::string("NOM_DEV_METER_TEMP_BLD_VMD");
			m[NOM_DEV_METER_TEMP_VMD]=std::string("NOM_DEV_METER_TEMP_VMD");
			m[NOM_DEV_MON_BLD_CHEM_MULTI_PARAM_VMD]=std::string("NOM_DEV_MON_BLD_CHEM_MULTI_PARAM_VMD");
			m[NOM_DEV_SYS_ANESTH_VMD]=std::string("NOM_DEV_SYS_ANESTH_VMD");
			m[NOM_DEV_GENERAL_VMD]=std::string("NOM_DEV_GENERAL_VMD");
			m[NOM_DEV_ECG_RESP_VMD]=std::string("NOM_DEV_ECG_RESP_VMD");
			m[NOM_DEV_ARRHY_VMD]=std::string("NOM_DEV_ARRHY_VMD");
			m[NOM_DEV_PULS_VMD]=std::string("NOM_DEV_PULS_VMD");
			m[NOM_DEV_ST_VMD]=std::string("NOM_DEV_ST_VMD");
			m[NOM_DEV_CO2_VMD]=std::string("NOM_DEV_CO2_VMD");
			m[NOM_DEV_PRESS_BLD_NONINV_VMD]=std::string("NOM_DEV_PRESS_BLD_NONINV_VMD");
			m[NOM_DEV_CEREB_PERF_VMD]=std::string("NOM_DEV_CEREB_PERF_VMD");
			m[NOM_DEV_CO2_CTS_VMD]=std::string("NOM_DEV_CO2_CTS_VMD");
			m[NOM_DEV_CO2_TCUT_VMD]=std::string("NOM_DEV_CO2_TCUT_VMD");
			m[NOM_DEV_O2_VMD]=std::string("NOM_DEV_O2_VMD");
			m[NOM_DEV_O2_CTS_VMD]=std::string("NOM_DEV_O2_CTS_VMD");
			m[NOM_DEV_O2_TCUT_VMD]=std::string("NOM_DEV_O2_TCUT_VMD");
			m[NOM_DEV_TEMP_DIFF_VMD]=std::string("NOM_DEV_TEMP_DIFF_VMD");
			m[NOM_DEV_CNTRL_VMD]=std::string("NOM_DEV_CNTRL_VMD");
			m[NOM_DEV_WEDGE_VMD]=std::string("NOM_DEV_WEDGE_VMD");
			m[NOM_DEV_O2_VEN_SAT_VMD]=std::string("NOM_DEV_O2_VEN_SAT_VMD");
			m[NOM_DEV_CARD_RATE_VMD]=std::string("NOM_DEV_CARD_RATE_VMD");
			m[NOM_DEV_PLETH_VMD]=std::string("NOM_DEV_PLETH_VMD");
			m[NOM_OBJ_HIF_KEY]=std::string("NOM_OBJ_HIF_KEY");
			m[NOM_OBJ_DISP]=std::string("NOM_OBJ_DISP");
			m[NOM_OBJ_SOUND_GEN]=std::string("NOM_OBJ_SOUND_GEN");
			m[NOM_OBJ_SETTING]=std::string("NOM_OBJ_SETTING");
			m[NOM_OBJ_PRINTER]=std::string("NOM_OBJ_PRINTER");
			m[NOM_OBJ_EVENT]=std::string("NOM_OBJ_EVENT");
			m[NOM_OBJ_BATT_CHARGER]=std::string("NOM_OBJ_BATT_CHARGER");
			m[NOM_OBJ_ECG_OUT]=std::string("NOM_OBJ_ECG_OUT");
			m[NOM_OBJ_INPUT_DEV]=std::string("NOM_OBJ_INPUT_DEV");
			m[NOM_OBJ_NETWORK]=std::string("NOM_OBJ_NETWORK");
			m[NOM_OBJ_QUICKLINK]=std::string("NOM_OBJ_QUICKLINK");
			m[NOM_OBJ_SPEAKER]=std::string("NOM_OBJ_SPEAKER");
			m[NOM_OBJ_PUMP]=std::string("NOM_OBJ_PUMP");
			m[NOM_OBJ_IR]=std::string("NOM_OBJ_IR");
			m[NOM_ACT_POLL_MDIB_DATA_EXT]=std::string("NOM_ACT_POLL_MDIB_DATA_EXT");
			m[NOM_DEV_ANALY_PULS_CONT]=std::string("NOM_DEV_ANALY_PULS_CONT");
			m[NOM_DEV_ANALY_BISPECTRAL_INDEX_VMD]=std::string("NOM_DEV_ANALY_BISPECTRAL_INDEX_VMD");
			m[NOM_DEV_HIRES_TREND]=std::string("NOM_DEV_HIRES_TREND");
			m[NOM_DEV_HIRES_TREND_MDS]=std::string("NOM_DEV_HIRES_TREND_MDS");
			m[NOM_DEV_HIRES_TREND_VMD]=std::string("NOM_DEV_HIRES_TREND_VMD");
			m[NOM_DEV_MON_PT_EVENT_VMD]=std::string("NOM_DEV_MON_PT_EVENT_VMD");
			m[NOM_DEV_DERIVED_MSMT]=std::string("NOM_DEV_DERIVED_MSMT");
			m[NOM_DEV_DERIVED_MSMT_MDS]=std::string("NOM_DEV_DERIVED_MSMT_MDS");
			m[NOM_DEV_DERIVED_MSMT_VMD]=std::string("NOM_DEV_DERIVED_MSMT_VMD");
			m[NOM_OBJ_SENSOR]=std::string("NOM_OBJ_SENSOR");
			m[NOM_OBJ_XDUCR]=std::string("NOM_OBJ_XDUCR");
			m[NOM_OBJ_CHAN_1]=std::string("NOM_OBJ_CHAN_1");
			m[NOM_OBJ_CHAN_2]=std::string("NOM_OBJ_CHAN_2");
			m[NOM_OBJ_AWAY_AGENT_1]=std::string("NOM_OBJ_AWAY_AGENT_1");
			m[NOM_OBJ_AWAY_AGENT_2]=std::string("NOM_OBJ_AWAY_AGENT_2");
			m[NOM_OBJ_HIF_MOUSE]=std::string("NOM_OBJ_HIF_MOUSE");
			m[NOM_OBJ_HIF_TOUCH]=std::string("NOM_OBJ_HIF_TOUCH");
			m[NOM_OBJ_HIF_SPEEDPOINT]=std::string("NOM_OBJ_HIF_SPEEDPOINT");
			m[NOM_OBJ_HIF_ALARMBOX]=std::string("NOM_OBJ_HIF_ALARMBOX");
			m[NOM_OBJ_BUS_I2C]=std::string("NOM_OBJ_BUS_I2C");
			m[NOM_OBJ_CPU_SEC]=std::string("NOM_OBJ_CPU_SEC");
			m[NOM_OBJ_LED]=std::string("NOM_OBJ_LED");
			m[NOM_OBJ_RELAY]=std::string("NOM_OBJ_RELAY");
			m[NOM_OBJ_BATT_1]=std::string("NOM_OBJ_BATT_1");
			m[NOM_OBJ_BATT_2]=std::string("NOM_OBJ_BATT_2");
			m[NOM_OBJ_DISP_SEC]=std::string("NOM_OBJ_DISP_SEC");
			m[NOM_OBJ_AGM]=std::string("NOM_OBJ_AGM");
			m[NOM_OBJ_TELEMON]=std::string("NOM_OBJ_TELEMON");
			m[NOM_OBJ_XMTR]=std::string("NOM_OBJ_XMTR");
			m[NOM_OBJ_CABLE]=std::string("NOM_OBJ_CABLE");
			m[NOM_OBJ_TELEMETRY_XMTR]=std::string("NOM_OBJ_TELEMETRY_XMTR");
			m[NOM_OBJ_MMS]=std::string("NOM_OBJ_MMS");
			m[NOM_OBJ_DISP_THIRD]=std::string("NOM_OBJ_DISP_THIRD");
			m[NOM_OBJ_BATT]=std::string("NOM_OBJ_BATT");
			m[NOM_OBJ_BATT_TELE]=std::string("NOM_OBJ_BATT_TELE");
			m[NOM_DEV_PROT_WATCH_CHAN]=std::string("NOM_DEV_PROT_WATCH_CHAN");
			m[NOM_OBJ_PROT_WATCH_1]=std::string("NOM_OBJ_PROT_WATCH_1");
			m[NOM_OBJ_PROT_WATCH_2]=std::string("NOM_OBJ_PROT_WATCH_2");
			m[NOM_OBJ_PROT_WATCH_3]=std::string("NOM_OBJ_PROT_WATCH_3");
			m[NOM_OBJ_ECG_SYNC]=std::string("NOM_OBJ_ECG_SYNC");
			m[NOM_DEV_METAB_VMD]=std::string("NOM_DEV_METAB_VMD");
			m[NOM_OBJ_SENSOR_O2_CO2]=std::string("NOM_OBJ_SENSOR_O2_CO2");
			m[NOM_OBJ_SRR_IF_1]=std::string("NOM_OBJ_SRR_IF_1");
			m[NOM_OBJ_DISP_REMOTE]=std::string("NOM_OBJ_DISP_REMOTE");
			printf("Done executing create_obj_class_enum_map\n");
			return m;
	 	};

		static std::map<AttributeId_enum_t,std::string> AttributeId_map;
		static std::map<AttributeId_enum_t,std::string> create_AttributeId_map ()
	 	{
		 	std::map<AttributeId_enum_t,std::string> m;
			m[NOM_ATTR_AL_MON_P_AL_LIST]=std::string("NOM_ATTR_AL_MON_P_AL_LIST");
			m[NOM_ATTR_AL_MON_T_AL_LIST]=std::string("NOM_ATTR_AL_MON_T_AL_LIST");
			m[NOM_ATTR_ALTITUDE]=std::string("NOM_ATTR_ALTITUDE");
			m[NOM_ATTR_AREA_APPL]=std::string("NOM_ATTR_AREA_APPL");
			m[NOM_ATTR_COLOR]=std::string("NOM_ATTR_COLOR");
			m[NOM_ATTR_DEV_AL_COND]=std::string("NOM_ATTR_DEV_AL_COND");
			m[NOM_ATTR_DISP_RES]=std::string("NOM_ATTR_DISP_RES");
			m[NOM_ATTR_GRID_VIS_I16]=std::string("NOM_ATTR_GRID_VIS_I16");
			m[NOM_ATTR_ID_ASSOC_NO]=std::string("NOM_ATTR_ID_ASSOC_NO");
			m[NOM_ATTR_ID_BED_LABEL]=std::string("NOM_ATTR_ID_BED_LABEL");
			m[NOM_ATTR_ID_HANDLE]=std::string("NOM_ATTR_ID_HANDLE");
			m[NOM_ATTR_ID_LABEL]=std::string("NOM_ATTR_ID_LABEL");
			m[NOM_ATTR_ID_LABEL_STRING]=std::string("NOM_ATTR_ID_LABEL_STRING");
			m[NOM_ATTR_ID_MODEL]=std::string("NOM_ATTR_ID_MODEL");
			m[NOM_ATTR_ID_PROD_SPECN]=std::string("NOM_ATTR_ID_PROD_SPECN");
			m[NOM_ATTR_ID_TYPE]=std::string("NOM_ATTR_ID_TYPE");
			m[NOM_ATTR_LINE_FREQ]=std::string("NOM_ATTR_LINE_FREQ");
			m[NOM_ATTR_LOCALIZN]=std::string("NOM_ATTR_LOCALIZN");
			m[NOM_ATTR_METRIC_INFO_LABEL]=std::string("NOM_ATTR_METRIC_INFO_LABEL");
			m[NOM_ATTR_METRIC_INFO_LABEL_STR]=std::string("NOM_ATTR_METRIC_INFO_LABEL_STR");
			m[NOM_ATTR_METRIC_SPECN]=std::string("NOM_ATTR_METRIC_SPECN");
			m[NOM_ATTR_METRIC_STAT]=std::string("NOM_ATTR_METRIC_STAT");
			m[NOM_ATTR_MODE_MSMT]=std::string("NOM_ATTR_MODE_MSMT");
			m[NOM_ATTR_MODE_OP]=std::string("NOM_ATTR_MODE_OP");
			m[NOM_ATTR_NOM_VERS]=std::string("NOM_ATTR_NOM_VERS");
			m[NOM_ATTR_NU_CMPD_VAL_OBS]=std::string("NOM_ATTR_NU_CMPD_VAL_OBS");
			m[NOM_ATTR_NU_VAL_OBS]=std::string("NOM_ATTR_NU_VAL_OBS");
			m[NOM_ATTR_PT_BSA]=std::string("NOM_ATTR_PT_BSA");
			m[NOM_ATTR_PT_DEMOG_ST]=std::string("NOM_ATTR_PT_DEMOG_ST");
			m[NOM_ATTR_PT_DOB]=std::string("NOM_ATTR_PT_DOB");
			m[NOM_ATTR_PT_ID]=std::string("NOM_ATTR_PT_ID");
			m[NOM_ATTR_PT_NAME_FAMILY]=std::string("NOM_ATTR_PT_NAME_FAMILY");
			m[NOM_ATTR_PT_NAME_GIVEN]=std::string("NOM_ATTR_PT_NAME_GIVEN");
			m[NOM_ATTR_PT_SEX]=std::string("NOM_ATTR_PT_SEX");
			m[NOM_ATTR_PT_TYPE]=std::string("NOM_ATTR_PT_TYPE");
			m[NOM_ATTR_SA_CALIB_I16]=std::string("NOM_ATTR_SA_CALIB_I16");
			m[NOM_ATTR_SA_CMPD_VAL_OBS]=std::string("NOM_ATTR_SA_CMPD_VAL_OBS");
			m[NOM_ATTR_SA_RANGE_PHYS_I16]=std::string("NOM_ATTR_SA_RANGE_PHYS_I16");
			m[NOM_ATTR_SA_SPECN]=std::string("NOM_ATTR_SA_SPECN");
			m[NOM_ATTR_SA_VAL_OBS]=std::string("NOM_ATTR_SA_VAL_OBS");
			m[NOM_ATTR_SCALE_SPECN_I16]=std::string("NOM_ATTR_SCALE_SPECN_I16");
			m[NOM_ATTR_STD_SAFETY]=std::string("NOM_ATTR_STD_SAFETY");
			m[NOM_ATTR_SYS_ID]=std::string("NOM_ATTR_SYS_ID");
			m[NOM_ATTR_SYS_SPECN]=std::string("NOM_ATTR_SYS_SPECN");
			m[NOM_ATTR_SYS_TYPE]=std::string("NOM_ATTR_SYS_TYPE");
			m[NOM_ATTR_TIME_ABS]=std::string("NOM_ATTR_TIME_ABS");
			m[NOM_ATTR_TIME_PD_SAMP]=std::string("NOM_ATTR_TIME_PD_SAMP");
			m[NOM_ATTR_TIME_REL]=std::string("NOM_ATTR_TIME_REL");
			m[NOM_ATTR_TIME_STAMP_ABS]=std::string("NOM_ATTR_TIME_STAMP_ABS");
			m[NOM_ATTR_TIME_STAMP_REL]=std::string("NOM_ATTR_TIME_STAMP_REL");
			m[NOM_ATTR_UNIT_CODE]=std::string("NOM_ATTR_UNIT_CODE");
			m[NOM_ATTR_VAL_ENUM_OBS]=std::string("NOM_ATTR_VAL_ENUM_OBS");
			m[NOM_ATTR_VMS_MDS_STAT]=std::string("NOM_ATTR_VMS_MDS_STAT");
			m[NOM_ATTR_PT_AGE]=std::string("NOM_ATTR_PT_AGE");
			m[NOM_ATTR_PT_HEIGHT]=std::string("NOM_ATTR_PT_HEIGHT");
			m[NOM_ATTR_PT_WEIGHT]=std::string("NOM_ATTR_PT_WEIGHT");
			m[NOM_ATTR_SA_FIXED_VAL_SPECN]=std::string("NOM_ATTR_SA_FIXED_VAL_SPECN");
			m[NOM_ATTR_PT_PACED_MODE]=std::string("NOM_ATTR_PT_PACED_MODE");
			m[NOM_ATTR_PT_ID_INT]=std::string("NOM_ATTR_PT_ID_INT");
			m[NOM_SAT_O2_TONE_FREQ]=std::string("NOM_SAT_O2_TONE_FREQ");
			m[NOM_ATTR_CMPD_REF_LIST]=std::string("NOM_ATTR_CMPD_REF_LIST");
			m[NOM_ATTR_NET_ADDR_INFO]=std::string("NOM_ATTR_NET_ADDR_INFO");
			m[NOM_ATTR_PCOL_SUPPORT]=std::string("NOM_ATTR_PCOL_SUPPORT");
			m[NOM_ATTR_PT_NOTES1]=std::string("NOM_ATTR_PT_NOTES1");
			m[NOM_ATTR_PT_NOTES2]=std::string("NOM_ATTR_PT_NOTES2");
			m[NOM_ATTR_TIME_PD_POLL]=std::string("NOM_ATTR_TIME_PD_POLL");
			m[NOM_ATTR_PT_BSA_FORMULA]=std::string("NOM_ATTR_PT_BSA_FORMULA");
			m[NOM_ATTR_MDS_GEN_INFO]=std::string("NOM_ATTR_MDS_GEN_INFO");
			m[NOM_ATTR_POLL_OBJ_PRIO_NUM]=std::string("NOM_ATTR_POLL_OBJ_PRIO_NUM");
			m[NOM_ATTR_POLL_NU_PRIO_LIST]=std::string("NOM_ATTR_POLL_NU_PRIO_LIST");
			m[NOM_ATTR_POLL_RTSA_PRIO_LIST]=std::string("NOM_ATTR_POLL_RTSA_PRIO_LIST");
			m[NOM_ATTR_METRIC_MODALITY]=std::string("NOM_ATTR_METRIC_MODALITY");
			return m;
		};

		static std::map<MetricCategory_enum_t,std::string> create_MetricCategory_map () 
		{
			std::map<MetricCategory_enum_t,std::string> m;
			m[MCAT_UNSPEC]=std::string("MCAT_UNSPEC");
			m[AUTO_MEASUREMENT]=std::string("AUTO_MEASUREMENT");
			m[MANUAL_MEASUREMENT]=std::string("MANUAL_MEASUREMENT");
			m[AUTO_SETTING]=std::string("AUTO_SETTING");
			m[MANUAL_SETTING]=std::string("MANUAL_SETTING");
			m[AUTO_CALCULATION]=std::string("AUTO_CALCULATION");
			m[MANUAL_CALCULATION]=std::string("MANUAL_CALCULATION");
			m[MULTI_DYNAMIC_CAPABILITIES]=std::string("MULTI_DYNAMIC_CAPABILITIES");
			m[AUTO_ADJUST_PAT_TEMP]=std::string("AUTO_ADJUST_PAT_TEMP");
			m[MANUAL_ADJUST_PAT_TEMP]=std::string("MANUAL_ADJUST_PAT_TEMP");
			m[AUTO_ALARM_LIMIT_SETTING]=std::string("AUTO_ALARM_LIMIT_SETTING");
			return m;
		};
		static std::map<MetricCategory_enum_t,std::string> MetricCategory_map;

		static std::map<SimpleColor_enum_t,std::string> create_SimpleColor_map ()
		{
			std::map<SimpleColor_enum_t,std::string> m;
			m[COL_BLACK]=std::string("COL_BLACK");
			m[COL_RED]=std::string("COL_RED");
			m[COL_GREEN]=std::string("COL_GREEN");
			m[COL_YELLOW]=std::string("COL_YELLOW");
			m[COL_BLUE]=std::string("COL_BLUE");
			m[COL_MAGENTA]=std::string("COL_MAGENTA");
			m[COL_CYAN]=std::string("COL_CYAN");
			m[COL_WHITE]=std::string("COL_WHITE");
			m[COL_PINK]=std::string("COL_PINK");
			m[COL_ORANGE]=std::string("COL_ORANGE");
			m[COL_LIGHT_GREEN]=std::string("COL_LIGHT_GREEN");
			m[COL_LIGHT_RED]=std::string("COL_LIGHT_RED");
			return m;
		};
		static std::map<SimpleColor_enum_t,std::string> SimpleColor_map;

		static std::map<MeasurementState_enum_t,std::string> create_MeasurementState_map () 
		{
			std::map<MeasurementState_enum_t,std::string> m;
			m[INVALID]=std::string("INVALID");
			m[QUESTIONABLE]=std::string("QUESTIONABLE");
			m[UNAVAILABLE]=std::string("UNAVAILABLE");
			m[CALIBRATION_ONGOING]=std::string("CALIBRATION_ONGOING");
			m[TEST_DATA]=std::string("TEST_DATA");
			m[DEMO_DATA]=std::string("DEMO_DATA");
			m[VALIDATED_DATA]=std::string("VALIDATED_DATA");
			m[EARLY_INDICATION]=std::string("EARLY_INDICATION");
			m[MSMT_ONGOING]=std::string("MSMT_ONGOING");
			m[MSMT_STATE_IN_ALARM]=std::string("MSMT_STATE_IN_ALARM");
			m[MSMT_STATE_AL_INHIBITED]=std::string("MSMT_STATE_AL_INHIBITED");
			return m;
		};
		static std::map<MeasurementState_enum_t,std::string> MeasurementState_map;

		static std::map<LabelMap_enum_t,LabelMap_enum_t>  create_WaveLabel_map () 
		{
			std::map<LabelMap_enum_t,LabelMap_enum_t> m ;
			m[NLS_NOM_PULS_OXIM_PLETH]=NOM_PLETH;
			return m;
		};
		static std::map<LabelMap_enum_t,LabelMap_enum_t>  WaveLabel_map;

		static std::map<MetricAccess_enum_t,std::string> create_MetricAccess_map()
		{
			std::map<MetricAccess_enum_t,std::string> m;
			m[AVAIL_INTERMITTEND]=std::string("AVAIL_INTERMITTEND");
			m[UPD_PERIODIC]=std::string("UPD_PERIODIC");
			m[UPD_EPISODIC]=std::string("UPD_EPISODIC");
			m[MSMT_NONCONTINUOUS]=std::string("MSMT_NONCONTINUOUS");
			return m;
		};
		static std::map<MetricAccess_enum_t,std::string> MetricAccess_map;

		class _init {
			public :
				_init() {
  				ObjClassType_map = create_obj_class_enum_map();
					AttributeId_map = create_AttributeId_map();
					MetricCategory_map = create_MetricCategory_map();
					SimpleColor_map = create_SimpleColor_map();
					MeasurementState_map = create_MeasurementState_map();
					WaveLabel_map = create_WaveLabel_map();
					MetricAccess_map = create_MetricAccess_map();
				}
		};

	private :
		static _init s_initializer;
};
#endif // __enums_H__
