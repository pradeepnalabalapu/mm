#ifndef __PH_PACKET_DEFS_H__
#define __PH_PACKET_DEFS_H__


#include <stdint.h>

namespace ph_packet_defs {
  //Types
#pragma pack(push,1)

  typedef uint8_t  u_8;
  typedef uint16_t u_16;
  typedef uint32_t u_32; 

  typedef struct {
    u_8     century;
    u_8     year;
    u_8     month;
    u_8     day;
    u_8     hour;
    u_8     minute;
    u_8     second;
    u_8     sec_fractions;
  } AbsoluteTime;


  typedef u_32 RelativeTime;

  //Object Identifier type (the nomenclature)
  typedef u_16 OIDType;

  typedef u_16 PrivateOID;
  typedef u_16 NomPartition;
  //#define NOM_PART_OBJ 1     /*Object oriented element, device nomenclature */
  //#define NOM_PART_SCADA 2   /*Types of measurement and place of measurement */
  //#define NOM_PART_EVT 3     /* Codes for alerts */
  //#define NOM_PART_DIM 4     /* Units of measurement */
  //#define NOM_PART_PGRP 6    /* Identification of parameter groups */
  //#define NOM_PART_INFRASTRUCT 8 /*Infrastructure for Data Export applications */
  //#define NOM_PART_EMFC                       1025 /* EMFC */
  //#define NOM_PART_SETTINGS                   1026 /* Settings */


  enum NomPartition_enum_t { NOM_PART_OBJ = 1, NOM_PART_SCADA = 2, NOM_PART_EVT=3,
    NOM_PART_DIM=4, NOM_PART_PGRP=6, NOM_PART_INFRASTRUCT=8, NOM_PART_EMFC =1025,
    NOM_PART_SETTINGS=1026};

  typedef struct {
    NomPartition    partition;
    OIDType         code;     
  } TYPE;

  typedef u_16      Handle; /* Object instance handle */

  typedef u_16 MdsContext; /* Context id of the measurement server */
  typedef struct {
    MdsContext    context_id;
    Handle        handle;
  }   GlbHandle;

  // Managed Object Identifier is a fully qualified object identifier which 
  // contains an identifier for the object class together with a Global handle
  typedef struct {
    OIDType     m_obj_class;
    GlbHandle   m_obj_inst;
  }  ManagedObjectId;

  //Attribute Value Assertion
  typedef struct {
    OIDType     attribute_id;
    u_16        length;
    //u_16        attribute_val; /* this field is only a placeholder here */
  } AVAType;

  //Attribute List
  typedef struct{
    u_16        count; /* Number of Attribute Value Assertion elements in the list*/
    u_16        length; /* size of the list in bytes */
    //AVAType     value[1]; /* placeholder */
  } AttributeList;

  //String
  typedef struct {
    u_16        length; /* number of octects in the value */
    u_16        value[1];
  } String;

  //Variable Label
  typedef struct {
    u_16        length; /* number of octects in the value */
    u_8        value[1];
  } VariableLabel;


  //TextId
  typedef u_32 TextId;

  typedef struct {
    u_16    count;
    u_16    length;
    //TextId  value[1];
  } TextIdList;




  //Session/Presentation Header
  typedef struct {
    u_16 session_id;     /* contains a fixed value 0XE100 */
    u_16 p_context_id;   /* negotiated in association phase */
  } SPpdu;

  //Remote Operation Header

  typedef struct {
    u_16       ro_type; /* ID for operation */
    u_16       length;      /* bytes to follow */
  } ROapdus;
  //ro_type
  //# define   ROIV_APDU 1 //Remote Operation Invoke
  //# define   RORS_APDU 2 //Remote Operation Result
  //# define   ROER_APDU 3 //Remote Operation Error
  //# define   ROLRS_APDU 5 //Remote Operation Linked Result
  enum ROapdus_enum_t {ROIV_APDU=1, RORS_APDU=2, ROER_APDU=3, ROLRS_APDU=5};


  typedef u_16 CMDType;

  //Remote Operation Invoke
  typedef struct {
    u_16    invoke_id;    /* identifies the transaction */
    CMDType command_type; /* identifies type of command */
    u_16    length;       /* no. of bytes in rest of message */
  } ROIVapdu;


  //Remote Operation Result
  typedef struct {
    u_16    invoke_id;    /* mirrored back from op. invoke */
    CMDType command_type; /* identifies type of command */
    u_16    length;       /* no. of bytes in rest of message */
  } RORSapdu;

  //Remote Operation Linked Result ID
  typedef struct {
    u_8     state;
    u_8     count;  /* counter starts with 1 */
  } RorlsId;
  //definitions of state
  //#define RORLS_FIRST 1 /* set in the first message */
  //#define RORLS_NOT_FIRST_NOT_LAST  2
  //#define RORLS_LAST  3 /* last RORLSapdu, one RORSapdu to follow */
  enum RorlsId_state_enum_t {RORLS_FIRST=1, RORLS_NOT_FIRST_NOT_LAST=2, RORLS_LAST=3};

  //Remote Operation Linked Result
  typedef struct {
    RorlsId linked_id;    /* identifies each result messages in a sequence */
    u_16    invoke_id;    /* mirrored invoke identifier from invoke message */
    CMDType command_type; /* identifies the type of command appended to this */
    u_16    length;       /* no. of bytes in rest of message */
  } ROLRSapdu;


  //Remote Operation Error
  typedef struct {
    u_16    invoke_id;
    u_16    error_value;
    u_16    length;
  } ROERapdu;
  //error_value values
  //# define NO_SUCH_OBJECT_CLASS       0  /* No such object class. */
  /* OIDType with class ID is appended to message */
  //# define NO_SUCH_OBJECT_INSTANCE    1  /* No such object instance */
  /* ManagedObjectId of the instance is appended */
  //# define ACCESS_DENIED              2  /* Computer Client has no privileges */
  //# define GET_LIST_ERROR             7  /* Get Operation failed */
  //# define SET_LIST_ERROR             8  /* Set Operation failed */
  //# define NO_SUCH_ACTION             9  /* Unknown action type */
  //# define PROCESSING_FAILURE         10 /* Generic error for invalid request */
  //# define INVALID_ARGUMENT_VALUE     15 /* The arg of ROSE message was invalid*/
  //# define INVALID_SCOPE              16 /* scope is not valid for operation */
  //# define INVALID_OBJECT_INSTANCE    17 /* Wrong object instance */
  enum ROERapdu_enum_t {NO_SUCH_OBJECT_CLASS=0, NO_SUCH_OBJECT_INSTANCE=1,
    ACCESS_DENIED=2, GET_LIST_ERROR=7, SET_LIST_ERROR=8, NO_SUCH_ACTION=9,
    PROCESSING_FAILURE=10, INVALID_ARGUMENT_VALUE=15, INVALID_SCOPE=16,
    INVALID_OBJECT_INSTANCE=17};


  typedef u_16      ErrorStatus;
//#define ATTR_ACCESS_DENIED             2
//#define ATTR_NO_SUCH_ATTRIBUTE         5
//#define ATTR_INVALID_ATTRIBUTE_VALUE   6
//#define ATTR_INVALID_OPERATION        24
//#define ATTR_INVALID_OPERATOR         25
  enum ErrorStatus_enum_t {ATTR_ACCESS_DENIED=2, ATTR_NO_SUCH_ATTRIBUTE=5,
    ATTR_INVALID_ATTRIBUTE_VALUE=6, ATTR_INVALID_OPERATION=24,
    ATTR_INVALID_OPERATOR=25};

  typedef struct {
    ErrorStatus     errorStatus;
    OIDType         attributeId;
  } GetError;

  //GetListError
  typedef struct  {
    ManagedObjectId       managed_object;
    struct {
      u_16          count;
      u_16          length;
      GetError      value[1];
    } getInfoList;
  } GetListError;

  typedef u_16    ModifyOperator;
//#define REPLACE         0
//#define ADD_VALUES      1
//#define REMOVE_VALUES   2
//#define SET_TO_DEFAULT  3
  enum ModifyOperator_enum_t {REPLACE=0, ADD_VALUES=1, REMOVE_VALUES=2,
    SET_TO_DEFAULT=3};

  typedef struct {
    ErrorStatus     errorStatus;
    ModifyOperator  modifyOperator;
    OIDType         attributeId;
  } SetError;

  //SetListError
  typedef struct {
    ManagedObjectId       managed_object;
    struct {
      u_16          count;
      u_16          length;
      SetError      value[1];
    } setInfoList;
  } SetListError;


  //ProcessingFailure
  typedef struct {
    OIDType     error_id;
    u_16        length;
  } ProcessingFailure;


//#define CMD_EVENT_REPORT 0 /* unsolicited event message */
//#define CMD_CONFIRMED_EVENT_REPORT 1 /* unsolicited event message */
  /* that warrants event report result message */
//#define CMD_GET 3 /* to request attribute values of managed objects. */
  /* Receiver responds with Get Result message */
//#define CMD_SET 4 /* set values of managed objects */
//#define CMD_CONFIRMED_SET 5 /* set, but receiver responds with Set Result msg */
//#define CMD_CONFIRMED_ACTION 7 /* to invoke an activity on receiver. */         
  /* Receiver must send an Action Result message */
  enum CMDType_enum_t {CMD_EVENT_REPORT=0, CMD_CONFIRMED_EVENT_REPORT=1,
    CMD_GET=3, CMD_SET=4, CMD_CONFIRMED_SET=5, CMD_CONFIRMED_ACTION=7};


  //Event Report Message Structure
  typedef struct  {
    ManagedObjectId  managed_object; /* ident. of sender */
    RelativeTime     event_time;     /* event time stamp */
    OIDType          event_type;     /* identification of event */
    u_16             length;         /* size of appended data */
  } EventReportArgument;

  //Event Report result
  typedef struct {
    ManagedObjectId   managed_object; /* mirrored from EvRep */
    RelativeTime      current_time;   /* result time stamp */
    OIDType           event_type;     /* identification of event */
    u_16              length;         /* size of appended data */
  } EventReportResult;


  //Action command
  // the protocol uses this command to call the Data Pull method which
  // returns device data
  typedef struct {
    ManagedObjectId   managed_object; /* addressed object */
    u_32              scope;          /* fixed value 0 */
    OIDType           action_type;    /*identification of method */
    u_16              length;         /* size of appended data */
  } ActionArgument;
  //action type
//#define NOM_ACT_POLL_MDIB_DATA      3094 //single poll data request
//#define NOM_ACT_POLL_MDIB_DATA_EXT  61755 //extended poll data request
  enum ActionArgument_enum_t {POLL_DATA = 3094,
    POLL_DATA_EXT=61755};


  //Action result
  typedef struct {
    ManagedObjectId   managed_object;
    OIDType           action_type;     /* identification of method */
    u_16              length;          /* size of appended data */
  } ActionResult;


  typedef struct {
    u_16      count;
    u_16      length;
    //OIDType   value[1];
  } AttributeIdList;

  //Get command
  typedef struct {
    ManagedObjectId   managed_object;/*identifies object to which command is sent*/
    u_32              scope;        /* fixed value 0 */
    AttributeIdList   attributeIdList; 
  } GetArgument;


  //Get Result
  typedef struct {
    ManagedObjectId   managed_object;
    AttributeList     attributeList;
  } GetResult;



  typedef struct {
    ModifyOperator    modifyOperator;
    AVAType           attribute;
  } AttributeModEntry;


  typedef struct {
    u_16              count;
    u_16              length;
    //AttributeModEntry value[1];
  } ModificationList;

  //Set Command
  typedef struct {
    ManagedObjectId   managed_object;
    u_32              scope;           /* fixed value 0 */
    ModificationList  modificationList;/* Contains the attribute ids and values to be modified */
  } SetArgument;


  //Set Result
  typedef struct {
    ManagedObjectId   managed_object; /*identifies the object that responds to the set command */
    AttributeList     attributeList; /* contains all modified attributes */
  } SetResult;



  //Attributes

  typedef u_16 ApplProtoId;
//#define AP_ID_ACSE        1
//#define AP_ID_DATA_OUT    5
  enum ApplProtoId_enum_t {AP_ID_ACSE=1, AP_ID_DATA_OUT=5};

  typedef u_16 TransProtoId;
//#define TP_ID_UDP         1
  enum TransProtoId_enum_t {TP_ID_UDP=1};

  typedef u_16  ProtoOptions;
//#define P_OPT_WIRELESS    0x8000
  enum ProtoOptions_enum_t {P_OPT_WIRELESS=0x8000};

  typedef struct {
    ApplProtoId     appl_proto;
    TransProtoId    trans_proto;
    u_16            port_number;
    ProtoOptions    options;
  } ProtoSupportEntry;

  typedef struct {
    u_16    count;
    u_16    length;
    ProtoSupportEntry value[1];
  } ProtoSupport;



  typedef u_16  Language;
  enum Language_enum_t {
    LANGUAGE_UNSPEC    = 0 ,
    ENGLISH            = 1 ,
    GERMAN             = 2 ,
    FRENCH             = 3 ,
    ITALIAN            = 4 ,
    SPANISH            = 5 ,
    DUTCH              = 6 ,
    SWEDISH            = 7 ,
    FINNISH            = 8 ,
    NORWEG             = 9 ,
    DANISH             = 10,
    JAPANESE           = 11,
    REP_OF_CHINA       = 12,
    PEOPLE_REP_CHINA   = 13,
    PORTUGUESE         = 14,
    RUSSIAN            = 15,
    BYELORUSSIAN       = 16,
    UKRAINIAN          = 17,
    CROATIAN           = 18,
    SERBIAN            = 19,
    MACEDONIAN         = 20,
    BULGARIAN          = 21,
    GREEK              = 22,
    POLISH             = 23,
    CZECH              = 24,
    SLOVAK             = 25,
    SLOVENIAN          = 26,
    HUNGARIAN          = 27,
    ROMANIAN           = 28,
    TURKISH            = 29,
    LATVIAN            = 30,
    LITHUANIAN         = 31,
    ESTONIAN           = 32,
    KOREAN             = 33 };

  typedef u_16      StringFormat;
//#define     STRFMT_UNICODE_NT 11
  enum StringFormat_enum_t {STRFMT_UNICODE_NT=11};

  typedef struct {
    u_32        syslocal_revision;
    Language    language;
    StringFormat  format;
  } SystemLocal;



  typedef struct MACAddress {
    u_8       value[6];
  } MACAddress;

  typedef struct IPAddress {
    u_8     value[4];
  } IPAddress;

  typedef struct IpAddressInfo {
    MACAddress    mac_address;
    IPAddress     ip_address;
    IPAddress     subnet_mask;
  } IPAddressInfo;


//Object Classes

  enum obj_class_enum_t {
		NOM_MOC_VMO=1,
		NOM_MOC_VMO_METRIC_NU=6,
		NOM_MOC_VMO_METRIC_SA_RT=9,
		NOM_MOC_VMS_MDS=33,
		NOM_MOC_VMS_MDS_COMPOS_SINGLE_BED=35,
		NOM_MOC_VMS_MDS_SIMP=37,
		NOM_MOC_BATT=41,
		NOM_MOC_PT_DEMOG=42,
		NOM_MOC_VMO_AL_MON=54,
		NOM_ACT_POLL_MDIB_DATA=3094,
		NOM_NOTI_MDS_CREAT=3334,
		NOM_NOTI_CONN_INDIC=3351,
		NOM_DEV_METER_CONC_SKIN_GAS=4264,
		NOM_DEV_METER_FLOW_BLD=4284,
		NOM_DEV_ANALY_CONC_GAS_MULTI_PARAM_MDS=4113,
		NOM_DEV_ANALY_CONC_GAS_MULTI_PARAM_VMD=4114,
		NOM_DEV_METER_CONC_SKIN_GAS_MDS=4265,
		NOM_DEV_MON_PHYSIO_MULTI_PARAM_MDS=4429,
		NOM_DEV_PUMP_INFUS_MDS=4449,
		NOM_DEV_SYS_PT_VENT_MDS=4465,
		NOM_DEV_SYS_PT_VENT_VMD=4466,
		NOM_DEV_SYS_MULTI_MODAL_MDS=4493,
		NOM_DEV_SYS_MULTI_MODAL_VMD=4494,
		NOM_DEV_SYS_VS_CONFIG_MDS=5209,
		NOM_DEV_SYS_VS_UNCONFIG_MDS=5213,
		NOM_DEV_ANALY_SAT_O2_VMD=4106,
		NOM_DEV_ANALY_FLOW_AWAY_VMD=4130,
		NOM_DEV_ANALY_CARD_OUTPUT_VMD=4134,
		NOM_DEV_ANALY_PRESS_BLD_VMD=4174,
		NOM_DEV_ANALY_RESP_RATE_VMD=4186,
		NOM_DEV_CALC_VMD=4206,
		NOM_DEV_ECG_VMD=4262,
		NOM_DEV_METER_CONC_SKIN_GAS_VMD=4266,
		NOM_DEV_EEG_VMD=4274,
		NOM_DEV_METER_TEMP_BLD_VMD=4350,
		NOM_DEV_METER_TEMP_VMD=4366,
		NOM_DEV_MON_BLD_CHEM_MULTI_PARAM_VMD=4398,
		NOM_DEV_SYS_ANESTH_VMD=4506,
		NOM_DEV_GENERAL_VMD=5122,
		NOM_DEV_ECG_RESP_VMD=5130,
		NOM_DEV_ARRHY_VMD=5134,
		NOM_DEV_PULS_VMD=5138,
		NOM_DEV_ST_VMD=5142,
		NOM_DEV_CO2_VMD=5146,
		NOM_DEV_PRESS_BLD_NONINV_VMD=5150,
		NOM_DEV_CEREB_PERF_VMD=5154,
		NOM_DEV_CO2_CTS_VMD=5158,
		NOM_DEV_CO2_TCUT_VMD=5162,
		NOM_DEV_O2_VMD=5166,
		NOM_DEV_O2_CTS_VMD=5170,
		NOM_DEV_O2_TCUT_VMD=5174,
		NOM_DEV_TEMP_DIFF_VMD=5178,
		NOM_DEV_CNTRL_VMD=5182,
		NOM_DEV_WEDGE_VMD=5190,
		NOM_DEV_O2_VEN_SAT_VMD=5194,
		NOM_DEV_CARD_RATE_VMD=5202,
		NOM_DEV_PLETH_VMD=5238,
		NOM_OBJ_HIF_KEY=61584,
		NOM_OBJ_DISP=61616,
		NOM_OBJ_SOUND_GEN=61648,
		NOM_OBJ_SETTING=61649,
		NOM_OBJ_PRINTER=61650,
		NOM_OBJ_EVENT=61683,
		NOM_OBJ_BATT_CHARGER=61690,
		NOM_OBJ_ECG_OUT=61691,
		NOM_OBJ_INPUT_DEV=61692,
		NOM_OBJ_NETWORK=61693,
		NOM_OBJ_QUICKLINK=61694,
		NOM_OBJ_SPEAKER=61695,
		NOM_OBJ_PUMP=61716,
		NOM_OBJ_IR=61717,
		NOM_ACT_POLL_MDIB_DATA_EXT=61755,
		NOM_DEV_ANALY_PULS_CONT=61800,
		NOM_DEV_ANALY_BISPECTRAL_INDEX_VMD=61806,
		NOM_DEV_HIRES_TREND=61820,
		NOM_DEV_HIRES_TREND_MDS=61821,
		NOM_DEV_HIRES_TREND_VMD=61822,
		NOM_DEV_MON_PT_EVENT_VMD=61826,
		NOM_DEV_DERIVED_MSMT=61828,
		NOM_DEV_DERIVED_MSMT_MDS=61829,
		NOM_DEV_DERIVED_MSMT_VMD=61830,
		NOM_OBJ_SENSOR=61902,
		NOM_OBJ_XDUCR=61903,
		NOM_OBJ_CHAN_1=61916,
		NOM_OBJ_CHAN_2=61917,
		NOM_OBJ_AWAY_AGENT_1=61918,
		NOM_OBJ_AWAY_AGENT_2=61919,
		NOM_OBJ_HIF_MOUSE=61983,
		NOM_OBJ_HIF_TOUCH=61984,
		NOM_OBJ_HIF_SPEEDPOINT=61985,
		NOM_OBJ_HIF_ALARMBOX=61986,
		NOM_OBJ_BUS_I2C=61987,
		NOM_OBJ_CPU_SEC=61988,
		NOM_OBJ_LED=61990,
		NOM_OBJ_RELAY=61991,
		NOM_OBJ_BATT_1=61996,
		NOM_OBJ_BATT_2=61997,
		NOM_OBJ_DISP_SEC=61998,
		NOM_OBJ_AGM=61999,
		NOM_OBJ_TELEMON=62014,
		NOM_OBJ_XMTR=62015,
		NOM_OBJ_CABLE=62016,
		NOM_OBJ_TELEMETRY_XMTR=62053,
		NOM_OBJ_MMS=62070,
		NOM_OBJ_DISP_THIRD=62073,
		NOM_OBJ_BATT=62078,
		NOM_OBJ_BATT_TELE=62091,
		NOM_DEV_PROT_WATCH_CHAN=62095,
		NOM_OBJ_PROT_WATCH_1=62097,
		NOM_OBJ_PROT_WATCH_2=62098,
		NOM_OBJ_PROT_WATCH_3=62099,
		NOM_OBJ_ECG_SYNC=62147,
		NOM_DEV_METAB_VMD=62162,
		NOM_OBJ_SENSOR_O2_CO2=62165,
		NOM_OBJ_SRR_IF_1=62208,
		NOM_OBJ_DISP_REMOTE=62228
  };

	


  //Attribute IDs

  enum AttributeId_enum_t {
		NOM_ATTR_AL_MON_P_AL_LIST=0x0902,
		NOM_ATTR_AL_MON_T_AL_LIST=0x0904,
		NOM_ATTR_ALTITUDE=0x090C,
		NOM_ATTR_AREA_APPL=0x090D,
		NOM_ATTR_COLOR=0x0911,
		NOM_ATTR_DEV_AL_COND=0x0916,
		NOM_ATTR_DISP_RES=0x0917,
		NOM_ATTR_GRID_VIS_I16=0x091A,
		NOM_ATTR_ID_ASSOC_NO=0x091D,
		NOM_ATTR_ID_BED_LABEL=0x091E,
		NOM_ATTR_ID_HANDLE=0x0921,
		NOM_ATTR_ID_LABEL=0x0924,
		NOM_ATTR_ID_LABEL_STRING=0x0927,
		NOM_ATTR_ID_MODEL=0x0928,
		NOM_ATTR_ID_PROD_SPECN=0x092D,
		NOM_ATTR_ID_TYPE=0x092F,
		NOM_ATTR_LINE_FREQ=0x0935,
		NOM_ATTR_LOCALIZN=0x0937,
		NOM_ATTR_METRIC_INFO_LABEL=0x093C,
		NOM_ATTR_METRIC_INFO_LABEL_STR=0x093D,
		NOM_ATTR_METRIC_SPECN=0x093F,
		NOM_ATTR_METRIC_STAT=0x0940,
		NOM_ATTR_MODE_MSMT=0x0945,
		NOM_ATTR_MODE_OP=0x0946,
		NOM_ATTR_NOM_VERS=0x0948,
		NOM_ATTR_NU_CMPD_VAL_OBS=0x094B,
		NOM_ATTR_NU_VAL_OBS=0x0950,
		NOM_ATTR_PT_BSA=0x0956,
		NOM_ATTR_PT_DEMOG_ST=0x0957,
		NOM_ATTR_PT_DOB=0x0958,
		NOM_ATTR_PT_ID=0x095A,
		NOM_ATTR_PT_NAME_FAMILY=0x095C,
		NOM_ATTR_PT_NAME_GIVEN=0x095D,
		NOM_ATTR_PT_SEX=0x0961,
		NOM_ATTR_PT_TYPE=0x0962,
		NOM_ATTR_SA_CALIB_I16=0x0964,
		NOM_ATTR_SA_CMPD_VAL_OBS=0x0967,
		NOM_ATTR_SA_RANGE_PHYS_I16=0x096A,
		NOM_ATTR_SA_SPECN=0x096D,
		NOM_ATTR_SA_VAL_OBS=0x096E,
		NOM_ATTR_SCALE_SPECN_I16=0x096F,
		NOM_ATTR_STD_SAFETY=0x0982,
		NOM_ATTR_SYS_ID=0x0984,
		NOM_ATTR_SYS_SPECN=0x0985,
		NOM_ATTR_SYS_TYPE=0x0986,
		NOM_ATTR_TIME_ABS=0x0987,
		NOM_ATTR_TIME_PD_SAMP=0x098D,
		NOM_ATTR_TIME_REL=0x098F,
		NOM_ATTR_TIME_STAMP_ABS=0x0990,
		NOM_ATTR_TIME_STAMP_REL=0x0991,
		NOM_ATTR_UNIT_CODE=0x0996,
		NOM_ATTR_VAL_ENUM_OBS=0x099E,
		NOM_ATTR_VMS_MDS_STAT=0x09A7,
		NOM_ATTR_PT_AGE=0x09D8,
		NOM_ATTR_PT_HEIGHT=0x09DC,
		NOM_ATTR_PT_WEIGHT=0x09DF,
		NOM_ATTR_SA_FIXED_VAL_SPECN=0x0A16,
		NOM_ATTR_PT_PACED_MODE=0x0A1E,
		NOM_ATTR_PT_ID_INT=0xF001,
		NOM_SAT_O2_TONE_FREQ=0xF008,
		NOM_ATTR_CMPD_REF_LIST=0xF009,
		NOM_ATTR_NET_ADDR_INFO=0xF100,
		NOM_ATTR_PCOL_SUPPORT=0xF101,
		NOM_ATTR_PT_NOTES1=0xF129,
		NOM_ATTR_PT_NOTES2=0xF12A,
		NOM_ATTR_TIME_PD_POLL=0xF13E,
		NOM_ATTR_PT_BSA_FORMULA=0xF1EC,
		NOM_ATTR_MDS_GEN_INFO=0xF1FA,
		NOM_ATTR_POLL_OBJ_PRIO_NUM=0xF228,
		NOM_ATTR_POLL_NU_PRIO_LIST=0xF239,
		NOM_ATTR_POLL_RTSA_PRIO_LIST=0xF23A,
		NOM_ATTR_METRIC_MODALITY=0xF294 
  };

	

  //Attributes are arranged in groups
  enum AttributeGroupId_enum_t {
		NOM_ATTR_GRP_AL_MON=0x0801,
		NOM_ATTR_GRP_METRIC_VAL_OBS=0x0803,
		NOM_ATTR_GRP_PT_DEMOG=0x0807,
		NOM_ATTR_GRP_SYS_APPL=0x080A,
		NOM_ATTR_GRP_SYS_ID=0x080B,
		NOM_ATTR_GRP_SYS_PROD=0x080C,
		NOM_ATTR_GRP_VMO_DYN=0x0810,
		NOM_ATTR_GRP_VMO_STATIC=0x0811
  };

  //component Ids
  enum ComponentId_enum_t {
		ID_COMP_PRODUCT=0x0008,
		ID_COMP_CONFIG=0x0010,
		ID_COMP_BOOT=0x0018,
		ID_COMP_MAIN_BD=0x0050,
		ID_COMP_APPL_SW=0x0058
  };

  typedef u_32 nomenclature;
	typedef u_8 LI;

  //Association Request
  typedef struct {
    u_8 type;
    LI length;
  } SessionHeader;
//# define CN_SPDU_SI 0x0D
//# define AC_SPDU_SI 0x0E
//# define RF_SPDU_SI 0x0C
//# define FN_SPDU_SI 0x09
//# define DN_SPDU_SI 0x0A
//# define AB_SPDU_SI 0x19
  enum SessionHeader_type_enum_t {
    CN_SPDU_SI=0x0D, //session connect header
    AC_SPDU_SI=0x0E, //session accept header
    RF_SPDU_SI=0x0C, //session refuse header
    FN_SPDU_SI=0x09, //session finish header
    DN_SPDU_SI=0x0A, //session disconnect header
    AB_SPDU_SI=0x19 //session abort header
  }; 

	typedef u_32 ProtocolVersion;
  enum ProtocolVersion_enum_t {MDDL_VERSION1=0x80000000};
	typedef u_32 NomenclatureVersion;
  enum NomenclatureVersion_enum_t {NOMEN_VERSION=0x40000000};
	typedef u_32 FunctionalUnits;
	typedef u_32 SystemType;
	enum MDSEUserInfo_systemtype_t {SYST_CLIENT=0x80000000,
	 	SYST_SERVER=0x00800000};
	typedef u_32 StartupMode;
	enum MDSEUserInfo_startupmode_t { HOT_START = 0x80000000,
		WARM_START=0x40000000, COLD_START=0x20000000};

  typedef struct MDSEUserInfoStd { 
    ProtocolVersion       protocol_version;
    NomenclatureVersion   nomenclature_version;
    FunctionalUnits       functional_units;
    SystemType            system_type;
    StartupMode           startup_mode;
    AttributeList         option_list;
    AttributeList         supported_aprofiles;
  } MDSEUserInfoStd;

	typedef u_32 PollProfileRevision;
	enum PollProfileRevision_enum_t {POLL_PROFILE_REV_0=0x80000000};

	typedef u_32 PollProfileOptions;
	enum PollProfileOptions_enum_t {P_OPT_DYN_CREATE_OBJECTS=0x40000000,
		P_OPT_DYN_DELETE_OBJECTS=0x20000000};

	typedef struct PollProfileSupport {
	 	PollProfileRevision poll_profile_revision;
	 	RelativeTime min_poll_period;
	 	u_32 max_mtu_rx;
	 	u_32 max_mtu_tx;
	 	u_32 max_bw_tx;
	 	PollProfileOptions options;
	 	AttributeList optional_packages;
 	} PollProfileSupport;

  enum PollProfileAttribute_enum_t { NOM_POLL_PROFILE_SUPPORT=1,
		NOM_MDIB_OBJ_SUPPORT=258,NOM_ATTR_POLL_PROFILE_EXT=61441};

	typedef u_32 PollProfileExtOptions;
	enum PollProfileExtOptions_enum_t {
	 	POLL_EXT_PERIOD_NU_1SEC      =0x80000000,
	 	POLL_EXT_PERIOD_NU_AVG_12SEC =0x40000000,
	 	POLL_EXT_PERIOD_NU_AVG_60SEC =0x20000000,
	 	POLL_EXT_PERIOD_NU_AVG_300SEC=0x10000000,
	 	POLL_EXT_PERIOD_RTSA         =0x08000000,
	 	POLL_EXT_ENUM                =0x04000000,
	 	POLL_EXT_NU_PRIO_LIST        =0x02000000,
	 	POLL_EXT_DYN_MODALITIES      =0x01000000
	};

	typedef struct {
		PollProfileExtOptions options;
		AttributeList					ext_attr;
	} PollProfileExt;

  //single poll data request
  typedef struct{
    u_16 poll_number;
    TYPE polled_obj_type;
    OIDType polled_attr_grp;
  } PollMdibDataReq;

  typedef struct {
    u_16      poll_number;
    TYPE      polled_obj_type;
    OIDType   polled_attr_grp;
    AttributeList poll_ext_attr;
  } PollMdibDataReqExt;

  typedef struct {
    u_16                count;
    u_16                length;
    //SingleContextPoll   value[1];
  } PollInfoList;


  typedef struct {
    u_16            poll_number;
    RelativeTime    rel_time_stamp;
    AbsoluteTime    abs_time_stamp;
    TYPE            polled_obj_type;
    OIDType         polled_attr_grp;
    PollInfoList    poll_info_list;
  } PollMdibDataReply;

  typedef struct {
    u_16            poll_number;
    u_16            sequence_no;
    RelativeTime    rel_time_stamp;
    AbsoluteTime    abs_time_stamp;
    TYPE            polled_obj_type;
    OIDType         polled_attr_grp;
    PollInfoList    poll_info_list;
  } PollMdibDataReplyExt;



  typedef struct {
    MdsContext    context_id;
    struct {      
      u_16            count;
      u_16            length;    
      //ObservationPoll value[1];
    } poll_info;
  } SingleContextPoll;

  typedef struct {
    Handle          obj_handle;
    AttributeList   attributes;
  } ObservationPoll;

  typedef u_16 MetricCategory;
  enum MetricCategory_enum_t {
			MCAT_UNSPEC=0, //not specified
			AUTO_MEASUREMENT=1, //automatic measurement
			MANUAL_MEASUREMENT=2, //manual measurement
			AUTO_SETTING=3, //automatic setting
			MANUAL_SETTING=4, //manual setting
			AUTO_CALCULATION=5,  //automatic calculation e.g. differential temperature
			MANUAL_CALCULATION=6, //manual calculation
			MULTI_DYNAMIC_CAPABILITIES=50, //this measurement may change category or mode
			AUTO_ADJUST_PAT_TEMP=128, //measurement is automatically adjusted for patient temp
			MANUAL_ADJUST_PAT_TEMP=129, //measurement manually adjusted for patient temperature
			AUTO_ALARM_LIMIT_SETTING=130 //not a measurement, but an alarm limit setting
  };




  typedef u_16 MetricAccess;
  enum MetricAccess_enum_t { 
    AVAIL_INTERMITTEND=0x8000, //observed value not always available 
    UPD_PERIODIC=0x4000, //observed value is updated periodically
    UPD_EPISODIC=0x2000, //observed value is updated episodically
    MSMT_NONCONTINUOUS=0x1000 //measurement is not continuous
  }; 


  typedef struct MetricStructure {
    u_8 ms_struct; // 0 => simple, 1 => compound object
    u_8 ms_comp_no; // max number of components in the compound
  } MetricStructure;

  typedef u_16 MetricRelevance;

  //NOM_ATTR_METRIC_SPECN
  typedef struct
  {
    RelativeTime    update_period;
    MetricCategory  category;
    MetricAccess    access;
    MetricStructure structure;
    MetricRelevance relevance;    
  } MetricSpec;

  typedef u_16 SimpleColor;
  enum SimpleColor_enum_t { COL_BLACK=0, COL_RED=1, COL_GREEN=2, COL_YELLOW=3, 
    COL_BLUE=4, COL_MAGENTA=5, COL_CYAN=6, COL_WHITE=7,
    COL_PINK=20, COL_ORANGE=35, COL_LIGHT_GREEN=50,COL_LIGHT_RED=65};



  typedef u_16 MeasurementState;
  enum MeasurementState_enum_t {
    INVALID=0x8000, //The source detects a sufficient degradation to render the data meaningless.
    QUESTIONABLE=0x4000,//A problem exists, but it is still appropriate to present the data
    UNAVAILABLE=0x2000, //The signal does not permit derivation of the numeric in question
    CALIBRATION_ONGOING=0x1000, //Parameter is currently being calibrated.
    TEST_DATA=0x0800, //The signal is an automatically generated test signal only
    DEMO_DATA=0x0400, //The IntelliVue monitor runs in demonstration mode, the signal is automatically generated
    VALIDATED_DATA=0x0080, //The value has been manually validated
    EARLY_INDICATION=0x0040, //The value represents an early estimate of the actual signal
    MSMT_ONGOING=0x0020, //A new aperiodic measurement is currently ongoing
    MSMT_STATE_IN_ALARM=0x0002, //Indicates that the numeric has an active alarm condition
    MSMT_STATE_AL_INHIBITED=0x0001 //Alarms are switched off for the numeric (crossed bell)
  };

	typedef float FLOATType_field;

  typedef struct {
    OIDType           physio_id;
    MeasurementState  state;
    OIDType           unit_code;
    FLOATType_field   value;    
  } NuObsValue;

  typedef struct {
    OIDType           physio_id;
    MeasurementState  state;
  } SaObsValue;

  enum LabelMap_enum_t {
  NLS_NOM_EMFC_sAVDel = 0x040180CC,
NLS_NOM_SETT_APNEA_ALARM_DELAY = 0x0402F8D9,
NLS_NOM_EMFC_C20_PER_C = 0x04010E78,
NLS_NOM_C20_PER_C_INDEX = 0x0002F81A,
NLS_NOM_EMFC_Rf_V5 = 0x0401075C,
NLS_NOM_ECG_AMPL_ST_BASELINE_V5 = 0x0002F417,
NLS_NOM_EMFC_Urine = 0x04010BD8,
NLS_NOM_FLOW_URINE_PREV_24HR = 0x0002F883,
NLS_NOM_EMFC_PT = 0x040105E4,
NLS_NOM_TIME_PD_PT = 0x0002F18B,
NLS_NOM_EMFC_SerCa = 0x0401059C,
NLS_NOM_CONC_CA_SER = 0x0002F824,
NLS_NOM_EMFC_sBPAl = 0x0401A024,
NLS_NOM_SETT_PRESS_AL_ONOFF = 0x0402F8F7,
NLS_NOM_EMFC_SetTmp = 0x04010AD8,
NLS_NOM_TEMP_BODY = 0x00024B5C,
NLS_NOM_EMFC_sCO2Wm = 0x0401815C,
NLS_NOM_SETT_VENT_CO2_WARMING_MONITOR_ONOFF = 0x0402F915,
NLS_NOM_EMFC_sAPkFl = 0x04018030,
NLS_NOM_SETT_FLOW_AWAY_INSP_APNEA = 0x0402F8ED,
NLS_NOM_EMFC_SerGlc = 0x04010590,
NLS_NOM_CONC_GLU_SER = 0x0002F82A,
NLS_NOM_EMFC_RT_PCT_BE = 0x04010810,
NLS_NOM_EEG_PWR_SPEC_BETA_REL_RIGHT = 0x0002F860,
NLS_NOM_EMFC_T4 = 0x04010414,
NLS_NOM_TEMP_GEN_4 = 0x0002F0CA,
NLS_NOM_EMFC_GOT = 0x0401060C,
NLS_NOM_CONC_GOT = 0x0002F188,
NLS_NOM_EMFC_highO2 = 0x0401A020,
NLS_NOM_SETT_VENT_CONC_AWAY_O2_LIMIT_HI = 0x0402F919,
NLS_NOM_EMFC_MCV = 0x040105D4,
NLS_NOM_VOL_CORP_MEAN = 0x0002F8C4,
NLS_NOM_EMFC_sEnTrg = 0x040180B4,
NLS_NOM_SETT_TRIG_ONOFF = 0x0402F90C,
NLS_NOM_EMFC_Plts = 0x040105D0,
NLS_NOM_PLTS_CNT = 0x0002F167,
NLS_NOM_EMFC_sLInPr = 0x04018100,
NLS_NOM_SETT_PRESS_AWAY_MIN = 0x040250F2,
NLS_NOM_EMFC_GGT = 0x04010608,
NLS_NOM_CONC_GGT = 0x0002F189,
NLS_NOM_EMFC_sAGTWm = 0x0401816C,
NLS_NOM_SETT_VENT_AGENT_WARMING_MONITOR_ONOFF = 0x0402F90D,
NLS_NOM_EMFC_sAPVhP = 0x0401807C,
NLS_NOM_SETT_VENT_PRESS_AWAY_MAX_PV_APNEA = 0x0402F931,
NLS_NOM_EMFC_sfgSEV = 0x040181AC,
NLS_NOM_SETT_CONC_AWAY_SEVOFL = 0x040251E4,
NLS_NOM_EMFC_highMV = 0x0401A02C,
NLS_NOM_SETT_VENT_VOL_MINUTE_AWAY_LIMIT_HI = 0x0402F94B,
NLS_NOM_EMFC_P6_MEAN = 0x04010407,
NLS_NOM_PRESS_GEN_6_MEAN = 0x0002F3FB,
NLS_NOM_EMFC_SpRR = 0x04010BF4,
NLS_NOM_RESP_RATE_SPONT = 0x0002F828,
NLS_NOM_EMFC_Sample = 0x04010AAC,
NLS_NOM_SETT_SAMPLE = 0x0402F956,
NLS_NOM_EMFC_CK_MM = 0x04010604,
NLS_NOM_CONC_CREA_KIN_MM = 0x0002F17F,
NLS_NOM_EMFC_sFlas = 0x040181F8,
NLS_NOM_SETT_VENT_FLOW_AWAY_ASSIST = 0x0402F91C,
NLS_NOM_EMFC_RBC = 0x040105CC,
NLS_NOM_RB_CNT = 0x0002F169,
NLS_NOM_EMFC_TOF4 = 0x04010DCC,
NLS_NOM_TRAIN_OF_FOUR_4 = 0x0002F8AA,
NLS_NOM_EMFC_sSens = 0x04018188,
NLS_NOM_SETT_SENS_LEVEL = 0x0402F904,
NLS_NOM_EMFC_sSIMV = 0x04018118,
NLS_NOM_SETT_VENT_MODE_SYNC_MAND_INTERMIT = 0x0402F924,
NLS_NOM_EMFC_UrCa = 0x04010624,
NLS_NOM_CONC_CA_URINE = 0x0002F19C,
NLS_NOM_EMFC_vECG = 0x0401119C,
NLS_NOM_ELEC_POTL_VECT = 0x0002F874,
NLS_NOM_EMFC_PCO2_ADJ = 0x04010A7C,
NLS_NOM_CONC_PCO2_GEN_ADJ = 0x0002F834,
NLS_NOM_EMFC_BLANK = 0x04010960,
NLS_NOM_METRIC_NOS = 0x0002EFFF,
NLS_NOM_EMFC_sPIP = 0x040180FC,
NLS_NOM_SETT_PRESS_AWAY_INSP_MAX = 0x04025109,
NLS_NOM_EMFC_sALMRT = 0x040180F0,
NLS_NOM_SETT_VENT_TIME_PD_RAMP_AL = 0x0402F946,
NLS_NOM_EMFC_sfgO2 = 0x040181B4,
NLS_NOM_SETT_FLOW_AWAY_O2 = 0x0402F87F,
NLS_NOM_EMFC_UrNaEx = 0x040101B4,
NLS_NOM_CONC_NA_EXCR = 0x0002F830,
NLS_NOM_EMFC_P1_SYS = 0x04010031,
NLS_NOM_PRESS_GEN_1_SYS = 0x0002F0A5,
NLS_NOM_EMFC_LT_MPF = 0x040107F8,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_MEDIAN_LEFT = 0x0002F84B,
NLS_NOM_EMFC_extHR = 0x04010700,
NLS_NOM_CARD_BEAT_RATE_EXT = 0x0002F81B,
NLS_NOM_EMFC_TOF1 = 0x04010DC0,
NLS_NOM_TRAIN_OF_FOUR_1 = 0x0002F8A7,
NLS_NOM_EMFC_L_V4 = 0x04010770,
NLS_NOM_ECG_ELEC_POTL_V4 = 0x00020106,
NLS_NOM_EMFC_PPV = 0x040111E0,
NLS_NOM_PULS_PRESS_VAR = 0x0002F0E3,
NLS_NOM_EMFC_SO2_CALC = 0x04010A90,
NLS_NOM_SAT_O2_CALC = 0x0002F89C,
NLS_NOM_EMFC_TGL = 0x0401061C,
NLS_NOM_CONC_TGL = 0x0002F16F,
NLS_NOM_EMFC_P5 = 0x04010400,
NLS_NOM_PRESS_GEN_5 = 0x0002F3F4,
NLS_NOM_EMFC_PcCO2 = 0x04010A78,
NLS_NOM_CONC_PCO2_CAP = 0x0002F159,
NLS_NOM_EMFC_Fe = 0x04010614,
NLS_NOM_CONC_FE_GEN = 0x0002F160,
NLS_NOM_EMFC_O2EI = 0x0401052C,
NLS_NOM_EXTRACT_O2_INDEX = 0x0002F875,
NLS_NOM_EMFC_sFIO2 = 0x04018010,
NLS_NOM_SETT_VENT_CONC_AWAY_O2_INSP = 0x04027498,
NLS_NOM_EMFC_sAgent = 0x04018178,
NLS_NOM_SETT_CONC_AWAY_AGENT_TYPE = 0x0402F8E0,
NLS_NOM_EMFC_TFI = 0x040111A8,
NLS_NOM_VOL_FLUID_THORAC_INDEX = 0x0002F8C6,
NLS_NOM_EMFC_LT_AL = 0x040107E0,
NLS_NOM_EEG_PWR_SPEC_ALPHA_ABS_LEFT = 0x0002F855,
NLS_NOM_EMFC_Rf_aVF = 0x04010748,
NLS_NOM_ECG_AMPL_ST_BASELINE_AVF = 0x0002F450,
NLS_NOM_EMFC_RRmech = 0x04010850,
NLS_NOM_VENT_RESP_RATE = 0x00025022,
NLS_NOM_EMFC_ESR=0x0401064C,
NLS_NOM_ES_RATE=0x0002F17C,
NLS_NOM_EMFC_Rf_aVL = 0x04010744,
NLS_NOM_ECG_AMPL_ST_BASELINE_AVL = 0x0002F44F,
NLS_NOM_EMFC_BPAPPL = 0x040180BC,
NLS_NOM_SETT_VENT_PRESS_AWAY_BIPAP_LOW = 0x0402F92A,
NLS_NOM_EMFC_sO2Cal = 0x040180D8,
NLS_NOM_SETT_VENT_O2_CAL_MODE = 0x0402F926,
NLS_NOM_EMFC_aPTTWB = 0x04010E14,
NLS_NOM_TIME_PD_aPTT_WB = 0x0002F18D,
NLS_NOM_EMFC_HALLev = 0x0401087C,
NLS_NOM_VOL_LVL_LIQUID_BOTTLE_HALOTH = 0x0002F8CA,
NLS_NOM_EMFC_RT_PCT_DL = 0x04010814,
NLS_NOM_EEG_PWR_SPEC_DELTA_REL_RIGHT = 0x0002F868,
NLS_NOM_EMFC_Pat_T = 0x04010B54,
//NLS_NOM_TEMP_BODY = 0x00024B5C,
NLS_NOM_EMFC_sEnSgh = 0x04018040,
NLS_NOM_SETT_VENT_MODE_SIGH = 0x0402F923,
NLS_NOM_EMFC_sPStat = 0x0401A028,
NLS_NOM_SETT_PUMP_STATUS = 0x0402F8FE,
NLS_NOM_EMFC_BSA_D = 0x04010440,
NLS_NOM_AREA_BODY_SURFACE_ACTUAL_DUBOIS = 0x0002F813,
NLS_NOM_EMFC_Field3 = 0x04010AD0,
NLS_NOM_SETT_FIELD3 = 0x0402F95B,
NLS_NOM_EMFC_VCO2ti = 0x040111C4,
NLS_NOM_FLOW_CO2_PROD_RESP_TIDAL = 0x0002F882,
NLS_NOM_EMFC_EDV = 0x04010534,
NLS_NOM_VOL_VENT_L_END_DIA = 0x00024C00,
NLS_NOM_EMFC_highTV = 0x0401A034,
NLS_NOM_SETT_VENT_VOL_TIDAL_LIMIT_HI = 0x0402F94D,
NLS_NOM_EMFC_PVcP = 0x0401046C,
NLS_NOM_VENT_PRESS_AWAY_PV = 0x0002F8BC,
NLS_NOM_EMFC_Tpat = 0x04010A38,
//NLS_NOM_TEMP_BODY = 0x00024B5C,
NLS_NOM_EMFC_sRisTi = 0x04018284,
NLS_NOM_SETT_VENT_TIME_PD_RAMP = 0x0402F8BD,
NLS_NOM_EMFC_U_PER_SCr = 0x0401019C,
NLS_NOM_RATIO_CONC_URINE_CREA_SER = 0x0002F892,
NLS_NOM_EMFC_BSI = 0x04011198,
NLS_NOM_EEG_BURST_SUPPRN_INDEX = 0x0002F840,
NLS_NOM_EMFC_P4_SYS = 0x0401003D,
NLS_NOM_PRESS_GEN_4_SYS = 0x0002F0B1,
NLS_NOM_EMFC_sPin = 0x04018128,
NLS_NOM_SETT_PRESS_AWAY_INSP = 0x04025108,
NLS_NOM_EMFC_BE_B_CALC = 0x04010AC0,
NLS_NOM_BASE_EXCESS_BLD_ART_CALC = 0x0002F817,
NLS_NOM_EMFC_i_eAGT = 0x040106A0,
NLS_NOM_VENT_CONC_AWAY_AGENT_DELTA = 0x0002F8B2,
NLS_NOM_EMFC_UrDens = 0x04010BC0,
NLS_NOM_FLUID_DENS_URINE = 0x0002F19D,
NLS_NOM_EMFC_U_PER_Cre_CALC = 0x04010AE4,
NLS_NOM_RATIO_CONC_URINE_CREA_CALC = 0x0002F891,
NLS_NOM_EMFC_TVex = 0x040106B4,
NLS_NOM_VOL_AWAY_EXP_TIDAL = 0x0002F0E1,
NLS_NOM_EMFC_MCH = 0x040105D8,
NLS_NOM_HB_CORP_MEAN = 0x0002F885,
NLS_NOM_EMFC_Cartrg = 0x04010AB0,
NLS_NOM_SETT_CARTRG = 0x0402F957,
NLS_NOM_EMFC_SaO2 = 0x04010548,
NLS_NOM_SAT_O2_ART = 0x00024B34,
NLS_NOM_EMFC_P8_DIA = 0x0401040E,
NLS_NOM_PRESS_GEN_8_DIA = 0x0002F402,
NLS_NOM_EMFC_SO2_r = 0x040111B8,
NLS_NOM_SAT_O2_RIGHT = 0x0002F89E,
NLS_NOM_EMFC_RT_MDF = 0x04010830,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_DOM_MEAN_RIGHT = 0x0002F84A,
NLS_NOM_EMFC_Lact = 0x04010AE8,
NLS_NOM_CONC_LACT = 0x0002F174,
NLS_NOM_EMFC_GasCar = 0x040181DC,
NLS_NOM_SETT_VENT_GAS_CARRIER = 0x0402F91F,
NLS_NOM_EMFC_sVolAl = 0x04018158,
NLS_NOM_SETT_VENT_VOL_AWAY_AL_ONOFF = 0x0402F947,
NLS_NOM_EMFC_dBili = 0x04010598,
NLS_NOM_CONC_BILI_DIRECT = 0x0002F17A,
NLS_NOM_EMFC_fgAGT = 0x04010520,
NLS_NOM_FLOW_AWAY_AGENT = 0x0002F876,
NLS_NOM_EMFC_sTrig = 0x04018014,
NLS_NOM_SETT_TRIG_LEVEL = 0x00000000,
NLS_NOM_EMFC_sVmax = 0x04018150,
NLS_NOM_SETT_VENT_VOL_LIMIT_AL_HI_ONOFF = 0x0402F949,
NLS_NOM_EMFC_P3 = 0x04010038,
NLS_NOM_PRESS_GEN_3 = 0x0002F0AC,
NLS_NOM_EMFC_BagVol = 0x04010CFC,
NLS_NOM_VOL_URINE_COL = 0x00026830,
NLS_NOM_EMFC_PvO2_ADJ = 0x04010A68,
NLS_NOM_CONC_PO2_VEN_ADJ = 0x0002F83E,
NLS_NOM_EMFC_ALT = 0x04010BF0,
NLS_NOM_CONC_GPT = 0x0002F187,
NLS_NOM_EMFC_BP_SYS = 0x04010889,
NLS_NOM_PRESS_BLD_SYS = 0x00024A01,
NLS_NOM_EMFC_P7_DIA = 0x0401040A,
NLS_NOM_PRESS_GEN_7_DIA = 0x0002F3FE,
NLS_NOM_EMFC_liPVAT = 0x0401A010,
//NLS_NOM_SETT_APNEA_ALARM_DELAY_PV = 0x0402F8DA,
NLS_NOM_EMFC_T1 = 0x04010064,
NLS_NOM_TEMP_GEN_1 = 0x0002F0C7,
NLS_NOM_EMFC_CH2O = 0x04010118,
NLS_NOM_FREE_WATER_CLR = 0x0002F884,
NLS_NOM_EMFC_r = 0x04010E80,
NLS_NOM_AWAY_CORR_COEF = 0x0002F814,
NLS_NOM_EMFC_RC = 0x04010644,
NLS_NOM_RET_CNT = 0x0002F16A,
NLS_NOM_EMFC_SpAWRR = 0x04010510,
NLS_NOM_AWAY_RESP_RATE_SPONT = 0x0002F815,
NLS_NOM_EMFC_sMV = 0x040180B0,
NLS_NOM_SETT_VOL_MINUTE_AWAY = 0x04025148,
NLS_NOM_EMFC_sPincR = 0x0401814C,
NLS_NOM_SETT_VENT_AWAY_PRESS_RATE_INCREASE = 0x0402F912,
NLS_NOM_EMFC_MCHC = 0x040105DC,
NLS_NOM_CONC_HB_CORP_MEAN = 0x0002F82C,
NLS_NOM_EMFC_CHE = 0x040105F8,
NLS_NOM_CONC_CHE = 0x0002F182,
NLS_NOM_EMFC_P4 = 0x0401003C,
NLS_NOM_PRESS_GEN_4 = 0x0002F0B0,
NLS_NOM_EMFC_WBC = 0x040105C8,
NLS_NOM_WB_CNT = 0x0002F168,
NLS_NOM_EMFC_TOFcnt = 0x04010DAC,
NLS_NOM_TRAIN_OF_FOUR_CNT = 0x0002F8AB,
NLS_NOM_EMFC_HGB_CALC = 0x04010A34,
NLS_NOM_CONC_HB_ART_CALC = 0x0002F82B,
NLS_NOM_EMFC_CO_Hb = 0x04010628,
NLS_NOM_CONC_HB_CO_GEN = 0x00027180,
NLS_NOM_EMFC_GEF = 0x040111E4,
NLS_NOM_FRACT_EJECT = 0x0002F105,
NLS_NOM_EMFC_sExpTi = 0x040180E8,
NLS_NOM_SETT_VENT_TIME_PD_EXP = 0x0402F93F,
NLS_NOM_EMFC_sfgFl = 0x040181B8,
NLS_NOM_SETT_FLOW_AWAY_TOT = 0x0402F881,
NLS_NOM_EMFC_SerGlo = 0x040105BC,
NLS_NOM_CONC_GLO_SER = 0x0002F829,
NLS_NOM_EMFC_AnGap_CALC = 0x04010AA8,
NLS_NOM_CONC_AN_GAP_CALC = 0x0002F1A1,
NLS_NOM_EMFC_cktO2 = 0x040106A8,
NLS_NOM_VENT_CONC_AWAY_O2_CIRCUIT = 0x0002F8B8,
NLS_NOM_EMFC_IUP_SYS = 0x04010055,
NLS_NOM_PRESS_INTRA_UTERAL_SYS = 0x0002F0D9,
NLS_NOM_EMFC_Field2 = 0x04010ACC,
NLS_NOM_SETT_FIELD2 = 0x0402F95A,
NLS_NOM_EMFC_AWV = 0x04010668,
NLS_NOM_VOL_AWAY = 0x0002F0DF,
NLS_NOM_EMFC_P3_MEAN = 0x0401003B,
NLS_NOM_PRESS_GEN_3_MEAN = 0x0002F0AF,
NLS_NOM_EMFC_BagWgt = 0x04010BB8,
NLS_NOM_WEIGHT_URINE_COL = 0x0002F8D3,
NLS_NOM_EMFC_O2_MANUAL = 0x04010AD4,
NLS_NOM_CONC_AWAY_O2 = 0x00025164,
NLS_NOM_EMFC_i_eISO = 0x04010694,
NLS_NOM_VENT_CONC_AWAY_ISOFL_DELTA = 0x0002F8B6,
NLS_NOM_EMFC_P6_DIA = 0x04010406,
NLS_NOM_PRESS_GEN_6_DIA = 0x0002F3FA,
NLS_NOM_EMFC_iCa_N_CALC = 0x04011114,
NLS_NOM_CONC_CA_GEN_NORM_CALC = 0x0002F823,
NLS_NOM_EMFC_BEecf_CALC = 0x04010AA4,
NLS_NOM_CONC_BASE_EXCESS_ECF_CALC = 0x0002F821,
NLS_NOM_EMFC_sATV = 0x04018028,
NLS_NOM_SETT_VOL_AWAY_TIDAL_APNEA = 0x0402F951,
NLS_NOM_EMFC_pH_ADJ = 0x04010A48,
NLS_NOM_CONC_PH_GEN_ADJ = 0x0002F838,
NLS_NOM_EMFC_P2_DIA = 0x04010036,
NLS_NOM_PRESS_GEN_2_DIA = 0x0002F0AA,
NLS_NOM_EMFC_sSghNr = 0x04018024,
NLS_NOM_SETT_VENT_SIGH_MULT_RATE = 0x0402F93B,
NLS_NOM_EMFC_RT_TH = 0x04010828,
NLS_NOM_EEG_PWR_SPEC_THETA_ABS_RIGHT = 0x0002F86A,
NLS_NOM_EMFC_sfmax = 0x0401820C,
NLS_NOM_SETT_VENT_RESP_RATE_LIMIT_HI_PANT = 0x0402F937,
NLS_NOM_EMFC_UrGlc = 0x04010594,
NLS_NOM_CONC_GLU_URINE = 0x0002F19F,
NLS_NOM_EMFC_PTTrat = 0x04010E1C,
NLS_NOM_RATIO_TIME_PD_PTT = 0x0002F896,
NLS_NOM_EMFC_sfgHAL = 0x040181A4,
NLS_NOM_SETT_CONC_AWAY_HALOTH = 0x040251E0,
NLS_NOM_EMFC_sAPVI = 0x0401808C,
NLS_NOM_SETT_RATIO_IE_INSP_PV_APNEA = 0x0402F903,
NLS_NOM_EMFC_PO2_ADJ = 0x04010A60,
NLS_NOM_CONC_PO2_GEN_ADJ = 0x0002F83D,
NLS_NOM_EMFC_PcO2 = 0x04010A5C,
NLS_NOM_CONC_PO2_CAP = 0x0002F15A,
NLS_NOM_EMFC_SerCl = 0x040105B0,
NLS_NOM_CONC_CHLOR_SER = 0x0002F15F,
NLS_NOM_EMFC_UrVol = 0x040101BC,
NLS_NOM_VOL_URINE_BAL_PD = 0x00026824,
NLS_NOM_EMFC_BP_DIA = 0x0401088A,
NLS_NOM_PRESS_BLD_DIA = 0x00024A02,
NLS_NOM_EMFC_L_II = 0x04010780,
NLS_NOM_ECG_ELEC_POTL_II = 0x00020102,
NLS_NOM_EMFC_DET = 0x04010B60,
NLS_NOM_SETT_TEMP = 0x04024B48,
NLS_NOM_EMFC_SerK = 0x040105AC,
NLS_NOM_CONC_K_SER = 0x0002F82F,
NLS_NOM_EMFC_FeNa = 0x0401012C,
NLS_NOM_FRACT_EXCR_NA = 0x0002F194,
NLS_NOM_EMFC_sPmax = 0x040180E0,
NLS_NOM_SETT_VENT_PRESS_AWAY_INSP_MAX = 0x0402F8BB,
NLS_NOM_EMFC_BPAPTL = 0x040180C4,
NLS_NOM_SETT_VENT_TIME_PD_BIPAP_LOW = 0x0402F93E,
NLS_NOM_EMFC_PT_WB = 0x04010E20,
NLS_NOM_TIME_PD_PT_WB = 0x0002F18F,
NLS_NOM_EMFC_sCircl = 0x040181C8,
NLS_NOM_SETT_VENT_CIRCUIT_TYPE = 0x0402F913,
NLS_NOM_EMFC_LSCALE = 0x04010808,
NLS_NOM_EEG_ELEC_POTL_CRTX_GAIN_LEFT = 0x0002F841,
NLS_NOM_EMFC_AccVol = 0x04010680,
NLS_NOM_VOL_INFUS_ACTUAL_TOTAL = 0x000268FC,
NLS_NOM_EMFC_sBkgFl = 0x04018190,
NLS_NOM_SETT_VENT_AWAY_FLOW_BACKGROUND = 0x0402F90F,
NLS_NOM_EMFC_RT_DL = 0x04010824,
NLS_NOM_EEG_PWR_SPEC_DELTA_ABS_RIGHT = 0x0002F864,
NLS_NOM_EMFC_fgDES = 0x04010854,
NLS_NOM_FLOW_AWAY_DESFL = 0x0002F878,
NLS_NOM_EMFC_SerMg = 0x040105A4,
NLS_NOM_CONC_MG_SER = 0x0002F15C,
NLS_NOM_EMFC_AWVex = 0x04010794,
NLS_NOM_VOL_AWAY_EXP = 0x0002F8C1,
NLS_NOM_EMFC_sPltTi = 0x04018018,
NLS_NOM_SETT_TIME_PD_RESP_PLAT = 0x0402F0FF,
NLS_NOM_EMFC_RT_BE = 0x04010820,
NLS_NOM_EEG_PWR_SPEC_BETA_ABS_RIGHT = 0x0002F85C,
NLS_NOM_EMFC_UrpH = 0x04010584,
NLS_NOM_CONC_PH_URINE = 0x00027064,
NLS_NOM_EMFC_T1_T2 = 0x040100AC,
NLS_NOM_TEMP_DIFF = 0x0002E018,
NLS_NOM_EMFC_Patm = 0x040106AC,
NLS_NOM_PRESS_AIR_AMBIENT = 0x0002F06B,
NLS_NOM_EMFC_sPVcP = 0x04018064,
//NLS_NOM_SETT_PRESS_AWAY_INSP = 0x04025108,
NLS_NOM_EMFC_sARR = 0x0401802C,
NLS_NOM_SETT_AWAY_RESP_RATE_APNEA = 0x0402F8DE,
NLS_NOM_EMFC_BUN_PER_cr = 0x04010110,
NLS_NOM_RATIO_BUN_CREA = 0x0002F88F,
NLS_NOM_EMFC_SerPro = 0x040105C0,
NLS_NOM_CONC_PROT_SER = 0x0002F178,
NLS_NOM_EMFC_HbF = 0x0401062C,
NLS_NOM_CONC_HB_FETAL = 0x0002F165,
NLS_NOM_EMFC_i_eDES = 0x0401069C,
NLS_NOM_VENT_CONC_AWAY_DESFL_DELTA = 0x0002F8B3,
NLS_NOM_EMFC_T2 = 0x04010068,
NLS_NOM_TEMP_GEN_2 = 0x0002F0C8,
NLS_NOM_EMFC_loPEEP = 0x0401A004,
NLS_NOM_VENT_PRESS_AWAY_END_EXP_POS_LIMIT_LO = 0x0002F8BA,
NLS_NOM_EMFC_TFC = 0x040111A4,
NLS_NOM_VOL_FLUID_THORAC = 0x0002F8C5,
NLS_NOM_EMFC_Length = 0x04010420,
NLS_NOM_BIRTH_LENGTH = 0x0002F818,
NLS_NOM_EMFC_sfgISO = 0x0401819C,
NLS_NOM_SETT_CONC_AWAY_ISOFL = 0x040251E8,
NLS_NOM_EMFC_i_eSEV = 0x04010698,
NLS_NOM_VENT_CONC_AWAY_SEVOFL_DELTA = 0x0002F8B9,
NLS_NOM_EMFC_RVrat = 0x04010E84,
NLS_NOM_RATIO_AWAY_RATE_VOL_AWAY = 0x0002F88E,
NLS_NOM_EMFC_FIO2_MANUAL = 0x04010ABC,
NLS_NOM_VENT_CONC_AWAY_O2_INSP = 0x00027498,
NLS_NOM_EMFC_tCO2 = 0x04010588,
NLS_NOM_CONC_CO2_TOT = 0x0002F825,
NLS_NOM_EMFC_sVolas = 0x040181F4,
NLS_NOM_SETT_VENT_VOL_AWAY_ASSIST = 0x0402F948,
NLS_NOM_EMFC_REF = 0x04010530,
NLS_NOM_RIGHT_HEART_FRACT_EJECT = 0x0002F89B,
NLS_NOM_EMFC_RiseTi = 0x04010550,
NLS_NOM_VENT_TIME_PD_RAMP = 0x0002F8BD,
NLS_NOM_EMFC_sSghTV = 0x04018020,
NLS_NOM_SETT_VENT_VOL_TIDAL_SIGH  = 0x0402F8C0,
NLS_NOM_EMFC_RemTi = 0x04010DBC,
NLS_NOM_TIME_PD_EVOK_REMAIN = 0x0002F8A0,
NLS_NOM_EMFC_RT_EEG = 0x0401082C,
NLS_NOM_EEG_ELEC_POTL_CRTX_RIGHT = 0x0002F846,
NLS_NOM_EMFC_TT = 0x040105E8,
NLS_NOM_TIME_PD_THROMBIN = 0x0002F191,
NLS_NOM_EMFC_inPkFl = 0x04010674,
NLS_NOM_FLOW_AWAY_INSP_MAX = 0x000250DD,
NLS_NOM_EMFC_PaCO2_ADJ = 0x04010A80,
NLS_NOM_CONC_PCO2_ART_ADJ = 0x0002F832,
NLS_NOM_EMFC_sMMV = 0x0401811C,
NLS_NOM_SETT_VENT_VOL_MINUTE_AWAY_MAND = 0x040251CC,
NLS_NOM_EMFC_RT_PCT_TH = 0x04010818,
NLS_NOM_EEG_PWR_SPEC_THETA_REL_RIGHT = 0x0002F86E,
NLS_NOM_EMFC_sPVE = 0x04018088,
NLS_NOM_SETT_RATIO_IE_EXP_PV = 0x0402F900,
NLS_NOM_EMFC_LT_BE = 0x040107E4,
NLS_NOM_EEG_PWR_SPEC_BETA_ABS_LEFT = 0x0002F85B,
NLS_NOM_EMFC_sAADel = 0x0401813C,
//NLS_NOM_SETT_APNEA_ALARM_DELAY = 0x0402F8D9,
NLS_NOM_EMFC_aPTTPE = 0x04010E18,
NLS_NOM_TIME_PD_aPTT_PE = 0x0002F18E,
NLS_NOM_EMFC_sIPPV = 0x040180A0,
NLS_NOM_SETT_VENT_RESP_RATE_MODE_PPV_INTERMIT_PAP = 0x0402F939,
NLS_NOM_EMFC_P2_MEAN = 0x04010037,
NLS_NOM_PRESS_GEN_2_MEAN = 0x0002F0AB,
NLS_NOM_EMFC_iCa_N = 0x04010E88,
NLS_NOM_CONC_CA_GEN_NORM = 0x0002F822,
NLS_NOM_EMFC_sO2Mon = 0x040180D4,
NLS_NOM_SETT_VENT_ANALY_CONC_GAS_O2_MODE = 0x0402F90E,
NLS_NOM_EMFC_P6_SYS = 0x04010405,
NLS_NOM_PRESS_GEN_6_SYS = 0x0002F3F9,
NLS_NOM_EMFC_DESLev = 0x04010880,
NLS_NOM_VOL_LVL_LIQUID_BOTTLE_DESFL = 0x0002F8C8,
NLS_NOM_EMFC_U_PER_POsm = 0x04010198,
NLS_NOM_RATIO_URINE_SER_OSM = 0x0002F898,
NLS_NOM_EMFC_RT_TP = 0x04010840,
NLS_NOM_EEG_PWR_SPEC_TOT_RIGHT = 0x0002F872,
NLS_NOM_EMFC_NsLoss = 0x040101D4,
NLS_NOM_NSLOSS = 0x0002F16D,
NLS_NOM_EMFC_lowMV = 0x0401A018,
NLS_NOM_SETT_VENT_VOL_MINUTE_AWAY_LIMIT_LO = 0x0402F94C,
NLS_NOM_EMFC_PTC = 0x04010DB8,
NLS_NOM_PTC_CNT = 0x0002F88B,
NLS_NOM_EMFC_sCMV = 0x04018114,
NLS_NOM_SETT_VENT_MODE_MAND_CTS_ONOFF = 0x0402F922,
NLS_NOM_EMFC_BP = 0x04010888,
NLS_NOM_PRESS_BLD = 0x00024A00,
NLS_NOM_EMFC_sChrge = 0x04018200,
NLS_NOM_SETT_EVOK_CHARGE = 0x0402F8E6,
NLS_NOM_EMFC_ESV = 0x04010538,
NLS_NOM_VOL_VENT_L_END_SYS = 0x00024C04,
NLS_NOM_EMFC_sNeblr = 0x04018044,
NLS_NOM_SETT_VENT_NEBULIZER_MODE = 0x0402F925,
NLS_NOM_EMFC_L_III = 0x04010784,
NLS_NOM_ECG_ELEC_POTL_III = 0x0002013D,
NLS_NOM_EMFC_i_eENF = 0x04010690,
NLS_NOM_VENT_CONC_AWAY_ENFL_DELTA = 0x0002F8B4,
NLS_NOM_EMFC_EDVI = 0x0401053C,
NLS_NOM_VOL_VENT_L_END_DIA_INDEX = 0x0002F8D0,
NLS_NOM_EMFC_RSBI = 0x04010EA0,
NLS_NOM_BREATH_RAPID_SHALLOW_INDEX = 0x0002F819,
NLS_NOM_EMFC_UrKEx = 0x040101A8,
NLS_NOM_CONC_K_URINE_EXCR = 0x0002F198,
NLS_NOM_EMFC_Twitch = 0x04010DB4,
NLS_NOM_TWITCH_AMPL = 0x0002F8AC,
NLS_NOM_EMFC_IUP_MEAN = 0x04010057,
NLS_NOM_PRESS_INTRA_UTERAL_MEAN = 0x0002F0DB,
NLS_NOM_EMFC_SerCK = 0x040105FC,
NLS_NOM_CONC_CREA_KIN_SER = 0x0002F180,
NLS_NOM_EMFC_alphaA = 0x040105F4,
NLS_NOM_CONC_ALPHA_AMYLASE = 0x0002F186,
NLS_NOM_EMFC_PT_PE = 0x04010E24,
NLS_NOM_TIME_PD_PT_PE = 0x0002F190,
NLS_NOM_EMFC_ExpTi = 0x0401066C,
NLS_NOM_TIME_PD_EXP = 0x0002F8A1,
NLS_NOM_EMFC_sPtCat = 0x04018164,
NLS_NOM_SETT_PAT_TYPE = 0x0402F8F6,
NLS_NOM_EMFC_fgENF = 0x04010860,
NLS_NOM_FLOW_AWAY_ENFL = 0x0002F879,
NLS_NOM_EMFC_tBili = 0x0401058C,
NLS_NOM_CONC_BILI_TOT = 0x0002F177,
NLS_NOM_EMFC_UrUrea = 0x04010580,
NLS_NOM_CONC_UREA_URINE = 0x0002F195,
NLS_NOM_EMFC_L_aVR = 0x04010788,
NLS_NOM_ECG_ELEC_POTL_AVR = 0x0002013E,
NLS_NOM_EMFC_P2 = 0x04010034,
NLS_NOM_PRESS_GEN_2 = 0x0002F0A8,
NLS_NOM_EMFC_LDH = 0x04010638,
NLS_NOM_CONC_LDH = 0x0002F17B,
NLS_NOM_EMFC_sTrVol = 0x04018138,
NLS_NOM_SETT_VENT_VOL_LUNG_TRAPD = 0x040251B8,
NLS_NOM_EMFC_tProt = 0x04010634,
NLS_NOM_CONC_PROT_TOT = 0x0002F179,
NLS_NOM_EMFC_sOxiAl = 0x04018168,
NLS_NOM_SETT_PULS_OXIM_SAT_O2_AL_ONOFF = 0x0402F8FD,
NLS_NOM_EMFC_B_PER_Cre_CALC = 0x04010AE0,
NLS_NOM_RATIO_CONC_BLD_UREA_NITROGEN_CREA_CALC = 0x0002F890,
NLS_NOM_EMFC_HFMVin = 0x040106D8,
NLS_NOM_VOL_MINUTE_AWAY_INSP_HFV = 0x0002F8CD,
NLS_NOM_EMFC_sTlow = 0x040181E4,
NLS_NOM_SETT_VENT_TIME_PD_EXP_APRV = 0x0402F940,
NLS_NOM_EMFC_TOF2 = 0x04010DC4,
NLS_NOM_TRAIN_OF_FOUR_2 = 0x0002F8A8,
NLS_NOM_EMFC_Rf_III = 0x0401073C,
NLS_NOM_ECG_AMPL_ST_BASELINE_III = 0x0002F44D,
NLS_NOM_EMFC_sGasPr = 0x040181C0,
NLS_NOM_SETT_VENT_GAS_PROBE_POSN = 0x0402F920,
NLS_NOM_EMFC_Met_Hb = 0x04010630,
NLS_NOM_CONC_HB_MET_GEN = 0x0002717C,
NLS_NOM_EMFC_P7_SYS = 0x04010409,
NLS_NOM_PRESS_GEN_7_SYS = 0x0002F3FD,
NLS_NOM_EMFC_L_V5 = 0x04010774,
NLS_NOM_ECG_ELEC_POTL_V5 = 0x00020107,
NLS_NOM_EMFC_T3 = 0x04010410,
NLS_NOM_TEMP_GEN_3 = 0x0002F0C9,
NLS_NOM_EMFC_AGTs = 0x04010CE4,
NLS_NOM_CONC_AWAY_AGENT_SEC = 0x0002F820,
NLS_NOM_EMFC_sPVinT = 0x04018068,
NLS_NOM_SETT_VENT_TIME_PD_INSP_PV = 0x0402F943,
NLS_NOM_EMFC_PatID = 0x04010B68,
NLS_NOM_PAT_ID = 0x0002F88A,
NLS_NOM_EMFC_Rf_V2 = 0x04010750,
NLS_NOM_ECG_AMPL_ST_BASELINE_V2 = 0x0002F414,
NLS_NOM_EMFC_Model = 0x04018110,
NLS_NOM_ID_MODEL = 0x0002F887,
NLS_NOM_EMFC_MinAwP = 0x0401050C,
NLS_NOM_PRESS_AWAY_MIN = 0x000250F2,
NLS_NOM_EMFC_LT_DL = 0x040107E8,
NLS_NOM_EEG_PWR_SPEC_DELTA_ABS_LEFT =  0x0002F863,
NLS_NOM_EMFC_tSerCa = 0x040105A0,
NLS_NOM_CONC_tCA_SER = 0x0002F15D,
NLS_NOM_EMFC_ScO2_CALC = 0x04010A9C,
NLS_NOM_SAT_O2_CAP_CALC = 0x0002F1A0,
NLS_NOM_EMFC_ECTOP = 0x04010090,
NLS_NOM_ECG_STAT_ECT = 0x0002D006,
NLS_NOM_EMFC_sFlCal = 0x04018154,
NLS_NOM_SETT_FLOW_CAL_MODE = 0x0402F8F1,
NLS_NOM_EMFC_L_V3 = 0x0401076C,
NLS_NOM_ECG_ELEC_POTL_V3 = 0x00020105,
NLS_NOM_EMFC_RHYTHM = 0x0401008C,
NLS_NOM_ECG_STAT_RHY = 0x0002D007,
NLS_NOM_EMFC_ACI = 0x040111AC,
NLS_NOM_OUTPUT_CARD_INDEX_ACCEL = 0x0002F889,
NLS_NOM_EMFC_P7_MEAN = 0x0401040B,
NLS_NOM_PRESS_GEN_7_MEAN = 0x0002F3FF,
NLS_NOM_EMFC_sIMV = 0x040180A4,
NLS_NOM_SETT_VENT_RESP_RATE_MODE_MAND_INTERMITT = 0x0402F938,
NLS_NOM_EMFC_SerAlb = 0x040105B4,
NLS_NOM_CONC_ALB_SER = 0x0002F163,
NLS_NOM_EMFC_Pmin = 0x0401067C,
//NLS_NOM_PRESS_AWAY_MIN = 0x000250F2,
NLS_NOM_EMFC_pHa_ADJ = 0x04010A4C,
NLS_NOM_CONC_PH_ART_ADJ = 0x0002F836,
NLS_NOM_EMFC_sHFVRR = 0x04018108,
NLS_NOM_SETT_AWAY_RESP_RATE_HFV = 0x0402F8DF,
NLS_NOM_EMFC_sPWave = 0x0401803C,
NLS_NOM_SETT_AWAY_PRESS_PATTERN = 0x0402F8DC,
NLS_NOM_EMFC_sfgAGT = 0x04018198,
NLS_NOM_SETT_FLOW_AWAY_AGENT = 0x0402F876,
NLS_NOM_EMFC_BPAPPH = 0x040180C0,
NLS_NOM_SETT_VENT_PRESS_AWAY_BIPAP_HIGH = 0x0402F929,
NLS_NOM_EMFC_sAFIO2 = 0x04018034,
NLS_NOM_SETT_VENT_CONC_AWAY_O2_INSP_APNEA = 0x0402F917,
NLS_NOM_EMFC_P6 = 0x04010404,
NLS_NOM_PRESS_GEN_6 = 0x0002F3F8,
NLS_NOM_EMFC_PTrat = 0x04010E28,
NLS_NOM_RATIO_TIME_PD_PT = 0x0002F895,
NLS_NOM_EMFC_IUP_DIA = 0x04010056,
NLS_NOM_PRESS_INTRA_UTERAL_DIA = 0x0002F0DA,
NLS_NOM_EMFC_TVin = 0x040106B0,
NLS_NOM_VOL_AWAY_INSP_TIDAL = 0x0002F0E0,
NLS_NOM_EMFC_PtVent = 0x04010BDC,
NLS_NOM_VENT_ACTIVE = 0x0002F8B0,
NLS_NOM_EMFC_LT_PCT_AL = 0x040107D0,
NLS_NOM_EEG_PWR_SPEC_ALPHA_REL_LEFT = 0x0002F859,
NLS_NOM_EMFC_Rdyn = 0x04010480,
NLS_NOM_RES_AWAY_DYN = 0x0002F899,
NLS_NOM_EMFC_sVMode = 0x04018000,
NLS_NOM_SETT_VENT_MODE = 0x0402F921,
NLS_NOM_EMFC_etAGTs = 0x04010CF0,
NLS_NOM_CONC_AWAY_AGENT_ET_SEC = 0x0002F81E,
NLS_NOM_EMFC_pHv_ADJ = 0x04010A50,
NLS_NOM_CONC_PH_VEN_ADJ = 0x0002F839,
NLS_NOM_EMFC_sHum = 0x04018288,
NLS_NOM_SETT_HUMID = 0x0402F103,
NLS_NOM_EMFC_highP = 0x0401A000,
NLS_NOM_SETT_VENT_PRESS_AWAY_LIMIT_HI = 0x0402F930,
NLS_NOM_EMFC_LT_TP = 0x04010804,
NLS_NOM_EEG_PWR_SPEC_TOT_LEFT = 0x0002F871,
NLS_NOM_EMFC_SCreat = 0x04010180,
NLS_NOM_CONC_CREA_SER = 0x0002F827,
NLS_NOM_EMFC_sExpFl = 0x04018134,
NLS_NOM_SETT_FLOW_AWAY_EXP = 0x0402F8EA,
NLS_NOM_EMFC_HFVTV = 0x040106E8,
NLS_NOM_VENT_VOL_TIDAL_HFV = 0x0002F8BF,
NLS_NOM_EMFC_UrCl = 0x040105B8,
NLS_NOM_CONC_CHLOR_URINE = 0x0002F19A,
NLS_NOM_EMFC_fgSEV = 0x04010858,
NLS_NOM_FLOW_AWAY_SEVOFL = 0x0002F880,
NLS_NOM_EMFC_sPlow = 0x040181EC,
NLS_NOM_SETT_VENT_PRESS_AWAY_EXP_APRV = 0x0402F92D,
NLS_NOM_EMFC_LT_PCT_DL = 0x040107D8,
NLS_NOM_EEG_PWR_SPEC_DELTA_REL_LEFT = 0x0002F867,
NLS_NOM_EMFC_Turine = 0x04010BC4,
NLS_NOM_TEMP_VESICAL = 0x0002F0C4,
NLS_NOM_EMFC_Rf_V1 = 0x0401074C,
NLS_NOM_ECG_AMPL_ST_BASELINE_V1 = 0x0002F413,
NLS_NOM_EMFC_ENFLev = 0x04010878,
NLS_NOM_VOL_LVL_LIQUID_BOTTLE_ENFL = 0x0002F8C9,
NLS_NOM_EMFC_liATi = 0x0401A00C,
//NLS_NOM_SETT_APNEA_ALARM_DELAY = 0x0402F8D9,
NLS_NOM_EMFC_fgHAL = 0x0401085C,
NLS_NOM_FLOW_AWAY_HALOTH = 0x0002F87B,
NLS_NOM_EMFC_AP = 0x040105F0,
NLS_NOM_CONC_AP = 0x0002F185,
NLS_NOM_EMFC_sInsTi = 0x040180E4,
NLS_NOM_SETT_VENT_TIME_PD_INSP = 0x0402F941,
NLS_NOM_EMFC_sThigh = 0x040181E8,
NLS_NOM_SETT_VENT_TIME_PD_INSP_APRV = 0x0402F942,
NLS_NOM_EMFC_sCPAP = 0x040180F4,
NLS_NOM_SETT_PRESS_AWAY_CTS_POS = 0x040250F4,
NLS_NOM_EMFC_sO2Pr = 0x040181C4,
NLS_NOM_SETT_VENT_O2_PROBE_POSN = 0x0402F927,
NLS_NOM_EMFC_loPmax = 0x04018174,
NLS_NOM_SETT_PRESS_AWAY_INSP_MAX_LIMIT_LO = 0x0402F8FB,
NLS_NOM_EMFC_IUP = 0x04010054,
NLS_NOM_PRESS_INTRA_UTERAL = 0x0002F0D8,
NLS_NOM_EMFC_IMV = 0x04010138,
NLS_NOM_VENT_MODE_MAND_INTERMIT = 0x0002D02A,
NLS_NOM_EMFC_sTVap = 0x04018184,
NLS_NOM_SETT_VOL_AWAY_TIDAL_APPLIED = 0x0402F952,
NLS_NOM_EMFC_PVPI = 0x040111F0,
NLS_NOM_PERM_VASC_PULM_INDEX = 0x0002F106,
NLS_NOM_EMFC_OperID = 0x04010AB4,
NLS_NOM_SETT_OPERID = 0x0402F958,
NLS_NOM_EMFC_Ppeak = 0x040106CC,
NLS_NOM_PRESS_AWAY_INSP_MAX = 0x00025109,
NLS_NOM_EMFC_P5_DIA = 0x04010402,
NLS_NOM_PRESS_GEN_5_DIA = 0x0002F3F6,
NLS_NOM_EMFC_sADel = 0x0401817C,
//NLS_NOM_SETT_APNEA_ALARM_DELAY = 0x0402F8D9,
NLS_NOM_EMFC_NIF = 0x04010E9C,
NLS_NOM_PRESS_AWAY_NEG_MAX = 0x000250F9,
NLS_NOM_EMFC_SpO2_APER = 0x040100E0,
NLS_NOM_PULS_OXIM_SAT_O2 = 0x00024BB8,
NLS_NOM_EMFC_sTVin = 0x040181CC,
NLS_NOM_SETT_VOL_AWAY_INSP_TIDAL = 0x0402F0E0,
NLS_NOM_EMFC_RT_MPF = 0x04010834,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_MEDIAN_RIGHT = 0x0002F84C,
NLS_NOM_EMFC_RT_PPF = 0x04010838,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_PEAK_RIGHT = 0x0002F850,
NLS_NOM_EMFC_ALP = 0x04010640,
NLS_NOM_CONC_ALP = 0x0002F81D,
NLS_NOM_EMFC_CO2Cal = 0x040181E0,
NLS_NOM_SETT_VENT_CO2_CAL_MODE = 0x0402F914,
NLS_NOM_EMFC_sFlow = 0x040180F8,
NLS_NOM_SETT_VENT_FLOW = 0x0402F91B,
NLS_NOM_EMFC_sAWRR=0x04018004,
NLS_NOM_SETT_AWAY_RESP_RATE=0x04025012,
NLS_NOM_EMFC_sHInPr = 0x0401818C,
//NLS_NOM_SETT_PRESS_AWAY_INSP_MAX = 0x04025109,
NLS_NOM_EMFC_set_T = 0x040181D0,
//NLS_NOM_SETT_TEMP = 0x04024B48,
NLS_NOM_EMFC_BasePr = 0x04010554,
NLS_NOM_VENT_PRESS_AWAY_END_EXP_POS = 0x000251A8,
NLS_NOM_EMFC_SO2_l = 0x040111B4,
NLS_NOM_SAT_O2_LEFT = 0x0002F89D,
NLS_NOM_EMFC_Age = 0x04010BC8,
NLS_NOM_AGE = 0x0002F810,
NLS_NOM_EMFC_CT = 0x04010648,
NLS_NOM_TIME_PD_COAGULATION = 0x0002F192,
NLS_NOM_EMFC_L_V2 = 0x04010768,
NLS_NOM_ECG_ELEC_POTL_V2 = 0x00020104,
NLS_NOM_EMFC_sO2Suc = 0x04018048,
NLS_NOM_SETT_VENT_O2_SUCTION_MODE = 0x0402F928,
NLS_NOM_EMFC_sTPDel = 0x040180D0,
NLS_NOM_SETT_TACHY_APNEA_DELAY = 0x0402F906,
NLS_NOM_EMFC_Crea = 0x04010ADC,
NLS_NOM_CONC_CREA = 0x0002F173,
NLS_NOM_EMFC_NgInsP = 0x04010484,
//NLS_NOM_PRESS_AWAY_NEG_MAX = 0x000250F9,
NLS_NOM_EMFC_P7 = 0x04010408,
NLS_NOM_PRESS_GEN_7 = 0x0002F3FC,
NLS_NOM_EMFC_MV = 0x040106B8,
NLS_NOM_VOL_MINUTE_AWAY = 0x00025148,
NLS_NOM_EMFC_SEVLev = 0x04010884,
NLS_NOM_VOL_LVL_LIQUID_BOTTLE_SEVOFL = 0x0002F8CC,
NLS_NOM_EMFC_Quick = 0x040105EC,
NLS_NOM_TIME_PD_THROMBOPLAS = 0x0002F193,
NLS_NOM_EMFC_PaFIO2 = 0x04010BE0,
NLS_NOM_RATIO_PaO2_FIO2 = 0x0002F894,
NLS_NOM_EMFC_pHc = 0x04010A44,
NLS_NOM_CONC_PH_CAP = 0x0002F158,
NLS_NOM_EMFC_ESVI = 0x04010540,
NLS_NOM_VOL_VENT_L_END_SYS_INDEX = 0x0002F8D1,
NLS_NOM_EMFC_Rinsp = 0x04010670,
NLS_NOM_RES_AWAY_INSP = 0x00025128,
NLS_NOM_EMFC_i_eN2O = 0x04010688,
NLS_NOM_VENT_CONC_AWAY_N2O_DELTA = 0x0002F8B7,
NLS_NOM_EMFC_Rf_aVR = 0x04010740,
NLS_NOM_ECG_AMPL_ST_BASELINE_AVR = 0x0002F44E,
NLS_NOM_EMFC_LT_TH = 0x040107EC,
NLS_NOM_EEG_PWR_SPEC_THETA_ABS_LEFT = 0x0002F869,
NLS_NOM_EMFC_RT_SEF = 0x0401083C,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_SPECTRAL_EDGE_RIGHT = 0x0002F854,
NLS_NOM_EMFC_RT_PCT_AL = 0x0401080C,
NLS_NOM_EEG_PWR_SPEC_ALPHA_REL_RIGHT = 0x0002F85A,
NLS_NOM_EMFC_Rexp = 0x04010664,
NLS_NOM_RES_AWAY_EXP = 0x00025124,
NLS_NOM_EMFC_P4_MEAN = 0x0401003F,
NLS_NOM_PRESS_GEN_4_MEAN = 0x0002F0B3,
NLS_NOM_EMFC_i_eO2 = 0x040106A4,
NLS_NOM_VENT_CONC_AWAY_O2_DELTA = 0x00025168,
NLS_NOM_EMFC_Rf_V4 = 0x04010758,
NLS_NOM_ECG_AMPL_ST_BASELINE_V4 = 0x0002F416,
NLS_NOM_EMFC_P5_SYS = 0x04010401,
NLS_NOM_PRESS_GEN_5_SYS = 0x0002F3F5,
NLS_NOM_EMFC_PT_INR = 0x04010E2C,
NLS_NOM_PT_INTL_NORM_RATIO = 0x0002F18C,
NLS_NOM_EMFC_Elapse = 0x04010B34,
NLS_NOM_TIME_PD_FROM_LAST_MSMT = 0x0002F8A2,
NLS_NOM_EMFC_ACT = 0x04010E10,
NLS_NOM_TIME_PD_ACT = 0x0002F18A,
NLS_NOM_EMFC_sfgAir = 0x040181B0,
NLS_NOM_SETT_FLOW_AWAY_AIR = 0x0402F877,
NLS_NOM_EMFC_sSilnc = 0x04018080,
NLS_NOM_SETT_AL_SILENCE_ONOFF = 0x0402F8D8,
NLS_NOM_EMFC_TOFrat = 0x04010DB0,
NLS_NOM_RATIO_TRAIN_OF_FOUR = 0x0002F897,
NLS_NOM_EMFC_L_aVL = 0x0401078C,
NLS_NOM_ECG_ELEC_POTL_AVL = 0x0002013F,
NLS_NOM_EMFC_Field1 = 0x04010AC8,
NLS_NOM_SETT_FIELD1 = 0x0402F959,
NLS_NOM_EMFC_HFTVin = 0x040106E4,
NLS_NOM_VENT_VOL_AWAY_INSP_TIDAL_HFV = 0x0002F8BE,
NLS_NOM_EMFC_SvO2_CALC = 0x04010A98,
NLS_NOM_SAT_O2_VEN_CALC = 0x0002F166,
NLS_NOM_EMFC_AAI = 0x04011194,
NLS_NOM_ELEC_EVOK_POTL_CRTX_ACOUSTIC_AAI = 0x0002F873,
NLS_NOM_EMFC_TVPSV = 0x04010E98,
NLS_NOM_VOL_AWAY_TIDAL_PSV = 0x0002F8C3,
NLS_NOM_EMFC_VPB = 0x04010088,
NLS_NOM_ECG_V_P_C_CNT = 0x00024261,
NLS_NOM_EMFC_sMVDel = 0x04018144,
NLS_NOM_SETT_VOL_MINUTE_ALARM_DELAY = 0x0402F953,
NLS_NOM_EMFC_sCO2Al = 0x04018160,
NLS_NOM_SETT_AWAY_CO2_AL_ONOFF = 0x0402F8DB,
NLS_NOM_EMFC_HFVAmp = 0x0401055C,
NLS_NOM_VENT_AMPL_HFV = 0x0002F8B1,
NLS_NOM_EMFC_lowO2 = 0x0401A01C,
NLS_NOM_SETT_VENT_CONC_AWAY_O2_LIMIT_LO = 0x0402F91A,
NLS_NOM_EMFC_BP_MEAN = 0x0401088B,
NLS_NOM_PRESS_BLD_MEAN = 0x00024A03,
NLS_NOM_EMFC_sSenFl = 0x0401805C,
NLS_NOM_SETT_VENT_AWAY_FLOW_SENSE = 0x0402F911,
NLS_NOM_EMFC_sDRate = 0x04018124,
NLS_NOM_SETT_FLOW_FLUID_PUMP = 0x04026858,
NLS_NOM_EMFC_fgISO = 0x04010864,
NLS_NOM_FLOW_AWAY_ISOFL = 0x0002F87C,
NLS_NOM_EMFC_fgAir = 0x040111BC,
NLS_NOM_FLOW_AWAY_AIR = 0x0002F877,
NLS_NOM_EMFC_SaO2_CALC = 0x04010A94,
NLS_NOM_SAT_O2_ART_CALC = 0x0002F164,
NLS_NOM_EMFC_sPVI = 0x04018084,
NLS_NOM_SETT_RATIO_IE_INSP_PV = 0x0402F902,
NLS_NOM_EMFC_Power = 0x04010B5C,
NLS_NOM_HEATING_PWR_INCUBATOR = 0x0002F886,
NLS_NOM_EMFC_sfgDES = 0x040181A8,
NLS_NOM_SETT_CONC_AWAY_DESFL = 0x040251D8,
NLS_NOM_EMFC_i_eHAL = 0x0401068C,
NLS_NOM_VENT_CONC_AWAY_HALOTH_DELTA = 0x0002F8B5,
NLS_NOM_EMFC_sTrgFl = 0x04018148,
NLS_NOM_SETT_VENT_FLOW_INSP_TRIG = 0x0402F91D,
NLS_NOM_EMFC_InsTi = 0x04010E74,
NLS_NOM_TIME_PD_INSP = 0x0002F8A3,
NLS_NOM_EMFC_CrCl = 0x04010124,
NLS_NOM_CONC_CREA_CLR = 0x0002F16C,
NLS_NOM_EMFC_UrNa_PER_K = 0x040101B0,
NLS_NOM_RATIO_CONC_URINE_NA_K = 0x0002F893,
NLS_NOM_EMFC_sCurnt = 0x040181FC,
NLS_NOM_SETT_EVOK_CURR = 0x0402F8E7,
NLS_NOM_EMFC_P3_SYS = 0x04010039,
NLS_NOM_PRESS_GEN_3_SYS = 0x0002F0AD,
NLS_NOM_EMFC_Rf_I = 0x04010734,
NLS_NOM_ECG_AMPL_ST_BASELINE_I = 0x0002F411,
NLS_NOM_EMFC_KCT = 0x04010654,
NLS_NOM_TIME_PD_KAOLIN_CEPHALINE = 0x0002F8A4,
NLS_NOM_EMFC_sPSVrp=0x04018180,
//NLS_NOM_SETT_VENT_TIME_PD_RAMP=0x0402F8BD,
NLS_NOM_EMFC_P8 = 0x0401040C,
NLS_NOM_PRESS_GEN_8 = 0x0002F400,
NLS_NOM_EMFC_P2_SYS = 0x04010035,
NLS_NOM_PRESS_GEN_2_SYS = 0x0002F0A9,
NLS_NOM_EMFC_Air_T = 0x04010B58,
NLS_NOM_TEMP_AMBIENT = 0x0002F0C6,
NLS_NOM_EMFC_GPT = 0x04010610,
//NLS_NOM_CONC_GPT = 0x0002F187,
NLS_NOM_EMFC_CK_MB = 0x04010600,
NLS_NOM_CONC_CREA_KIN_MB = 0x0002F181,
NLS_NOM_EMFC_P1_DIA = 0x04010032,
NLS_NOM_PRESS_GEN_1_DIA = 0x0002F0A6,
NLS_NOM_EMFC_fgFlow = 0x040111C0,
NLS_NOM_FLOW_AWAY_TOT = 0x0002F881,
NLS_NOM_EMFC_sBasFl = 0x04018058,
NLS_NOM_SETT_VENT_AWAY_FLOW_BASE = 0x0402F910,
NLS_NOM_EMFC_PTT = 0x040105E0,
NLS_NOM_TIME_PD_PTT = 0x0002F8A5,
NLS_NOM_EMFC_sAPVE = 0x04018090,
NLS_NOM_SETT_RATIO_IE_EXP_PV_APNEA = 0x0402F901,
NLS_NOM_EMFC_UrPro = 0x04010620,
NLS_NOM_CONC_PRO_URINE = 0x0002F19B,
NLS_NOM_EMFC_UCreat = 0x040101A0,
NLS_NOM_CONC_CREA_URINE = 0x0002F196,
NLS_NOM_EMFC_sfgENF = 0x040181A0,
NLS_NOM_SETT_CONC_AWAY_ENFL = 0x040251DC,
NLS_NOM_EMFC_SrUrea = 0x040105C4,
NLS_NOM_UREA_SER = 0x0002F8AD,
NLS_NOM_EMFC_PlGain = 0x04010514,
NOM_PLETH= 0x00004BB4,
NLS_NOM_PULS_OXIM_PLETH= 0x00024BB4,
NLS_NOM_PULS_OXIM_PLETH_GAIN = 0x0002F88D,
NLS_NOM_EMFC_pHc_ADJ = 0x04010A54,
NLS_NOM_CONC_PH_CAP_ADJ = 0x0002F837,
NLS_NOM_EMFC_TOF3 = 0x04010DC8,
NLS_NOM_TRAIN_OF_FOUR_3 = 0x0002F8A9,
NLS_NOM_EMFC_exPkFl = 0x040111CC,
NLS_NOM_FLOW_AWAY_EXP_MAX = 0x000250D9,
NLS_NOM_EMFC_Rf_V3 = 0x04010754,
NLS_NOM_ECG_AMPL_ST_BASELINE_V3 = 0x0002F415,
NLS_NOM_EMFC_KPLUS = 0x0401065C,
NLS_NOM_CONC_K_GEN = 0x00027110,
NLS_NOM_EMFC_L_I = 0x0401077C,
NLS_NOM_ECG_ELEC_POTL_I = 0x00020101,
NLS_NOM_EMFC_sSghR = 0x0401801C,
NLS_NOM_SETT_VENT_SIGH_RATE = 0x0402F93C,
NLS_NOM_EMFC_BSA_B = 0x0401043C,
NLS_NOM_AREA_BODY_SURFACE_ACTUAL_BOYD = 0x0002F812,
NLS_NOM_EMFC_G_Age = 0x04010428,
NLS_NOM_AGE_GEST = 0x0002F811,
NLS_NOM_EMFC_PlOsm = 0x04010164,
NLS_NOM_PLASMA_OSM = 0x0002F16B,
NLS_NOM_EMFC_fgO2 = 0x0401086C,
NLS_NOM_FLOW_AWAY_O2 = 0x0002F87F,
NLS_NOM_EMFC_PcO2_ADJ = 0x04010A6C,
NLS_NOM_CONC_PO2_CAP_ADJ = 0x0002F83C,
NLS_NOM_EMFC_DABP = 0x0401054C,
NLS_NOM_VENT_TIME_PD_PPV = 0x00025360,
NLS_NOM_EMFC_sAPVcP = 0x0401806C,
NLS_NOM_SETT_VENT_PRESS_AWAY_PV_APNEA = 0x0402F933,
NLS_NOM_EMFC_sUrTi = 0x040181D4,
NLS_NOM_SETT_URINE_BAL_PD = 0x0402F8AF,
NLS_NOM_EMFC_sEnTP = 0x040180B8,
NLS_NOM_SETT_TACHAPNEA_AL_ONOFF = 0x0402F905,
NLS_NOM_EMFC_DPosP = 0x04010848,
//NLS_NOM_VENT_TIME_PD_PPV = 0x00025360,
NLS_NOM_EMFC_sustP = 0x0401A014,
NLS_NOM_SETT_VENT_PRESS_AWAY_SUST_LIMIT_HI = 0x0402F935,
NLS_NOM_EMFC_RRsync = 0x0401084C,
NLS_NOM_RESP_BREATH_ASSIST_CNT = 0x0002F89A,
NLS_NOM_EMFC_sHFVFl = 0x04018104,
NLS_NOM_SETT_FLOW_AWAY_HFV = 0x0402F8EB,
NLS_NOM_EMFC_L_aVF = 0x04010790,
NLS_NOM_ECG_ELEC_POTL_AVF = 0x00020140,
NLS_NOM_EMFC_RT_AL = 0x0401081C,
NLS_NOM_EEG_PWR_SPEC_ALPHA_ABS_RIGHT = 0x0002F856,
NLS_NOM_EMFC_sMode = 0x04018098,
NLS_NOM_SETT_MODE_MSMT = 0x0402F8F5,
NLS_NOM_EMFC_sSPEEP = 0x040180AC,
NLS_NOM_SETT_VENT_PRESS_AWAY_END_EXP_POS_INTERMIT = 0x0402F92C,
NLS_NOM_EMFC_sPhigh = 0x040181F0,
NLS_NOM_SETT_VENT_PRESS_AWAY_INSP_APRV = 0x0402F92E,
NLS_NOM_EMFC_LT_PCT_TH = 0x040107DC,
NLS_NOM_EEG_PWR_SPEC_THETA_REL_LEFT = 0x0002F86D,
NLS_NOM_EMFC_sCycTi = 0x0401809C,
NLS_NOM_SETT_TIME_PD_MSMT = 0x0402F909,
NLS_NOM_EMFC_fgN2O = 0x04010868,
NLS_NOM_FLOW_AWAY_N2O = 0x0002F87E,
NLS_NOM_EMFC_AST = 0x0401063C,
NLS_NOM_CONC_AST = 0x0002F184,
NLS_NOM_EMFC_SpTVex = 0x040106E0,
NLS_NOM_VOL_AWAY_EXP_TIDAL_SPONT = 0x0002F8C2,
NLS_NOM_EMFC_sIE_1 = 0x040180EC,
NLS_NOM_SETT_RATIO_IE = 0x04025118,
NLS_NOM_EMFC_P1_MEAN = 0x04010033,
NLS_NOM_PRESS_GEN_1_MEAN = 0x0002F0A7,
NLS_NOM_EMFC_PvCO2_ADJ = 0x04010A84,
NLS_NOM_CONC_PCO2_VEN_ADJ = 0x0002F835,
NLS_NOM_EMFC_TC = 0x04010E7C,
NLS_NOM_AWAY_TC = 0x0002F816,
NLS_NOM_EMFC_P4_DIA = 0x0401003E,
NLS_NOM_PRESS_GEN_4_DIA = 0x0002F0B2,
NLS_NOM_EMFC_P1 = 0x04010030,
NLS_NOM_PRESS_GEN_1 = 0x0002F0A4,
NLS_NOM_EMFC_hiSghP = 0x0401A008,
NLS_NOM_SETT_VENT_PRESS_AWAY_SIGH_LIMIT_HI = 0x0402F934,
NLS_NOM_EMFC_Rf_V6 = 0x04010760,
NLS_NOM_ECG_AMPL_ST_BASELINE_V6 = 0x0002F418,
NLS_NOM_EMFC_Diff_X = 0x04010224,
//NLS_NOM_TEMP_DIFF = 0x0002E018,
NLS_NOM_EMFC_sMVAl = 0x040180DC,
NLS_NOM_SETT_VOL_MINUTE_AWAY_AL_ONOFF = 0x0402F955,
NLS_NOM_EMFC_P5_MEAN = 0x04010403,
NLS_NOM_PRESS_GEN_5_MEAN = 0x0002F3F7,
NLS_NOM_EMFC_sAPVO2 = 0x04018078,
NLS_NOM_SETT_VENT_CONC_AWAY_O2_INSP_PV_APNEA = 0x0402F918,
NLS_NOM_EMFC_Wave = 0x04018170,
NLS_NOM_WAVE_LBL = 0x0002F8D2,
NLS_NOM_EMFC_UrK = 0x040101A4,
NLS_NOM_CONC_K_URINE = 0x0002F197,
NLS_NOM_EMFC_LT_MDF = 0x040107F4,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_DOM_MEAN_LEFT = 0x0002F849,
NLS_NOM_EMFC_RRaw = 0x040106C4,
//NLS_NOM_VENT_RESP_RATE = 0x00025022,
NLS_NOM_EMFC_sAPVTi = 0x04018074,
NLS_NOM_SETT_VENT_TIME_PD_INSP_PV_APNEA = 0x0402F944,
NLS_NOM_EMFC_HI = 0x040111B0,
NLS_NOM_CARD_CONTRACT_HEATHER_INDEX = 0x0002F81C,
NLS_NOM_EMFC_sPEEP = 0x040180A8,
NLS_NOM_SETT_VENT_PRESS_AWAY_END_EXP_POS = 0x040251A8,
NLS_NOM_EMFC_L_V6=0x04010778,
NLS_NOM_ECG_ELEC_POTL_V6=0x00020108,
NLS_NOM_EMFC_COsm = 0x04010120,
NLS_NOM_CREA_OSM = 0x0002F83F,
NLS_NOM_EMFC_ISOLev = 0x04010874,
NLS_NOM_VOL_LVL_LIQUID_BOTTLE_ISOFL = 0x0002F8CB,
NLS_NOM_EMFC_Rf_II = 0x04010738,
NLS_NOM_ECG_AMPL_ST_BASELINE_II = 0x0002F412,
NLS_NOM_EMFC_tUrVol = 0x04010BBC,
NLS_NOM_VOL_URINE_BAL_PD_INSTANT = 0x0002F8CE,
NLS_NOM_EMFC_PcCO2_ADJ = 0x04010A88,
NLS_NOM_CONC_PCO2_CAP_ADJ = 0x0002F833,
NLS_NOM_EMFC_sfgN2O = 0x040181BC,
NLS_NOM_SETT_FLOW_AWAY_N2O = 0x0402F87E,
NLS_NOM_EMFC_UrFl = 0x04010890,
NLS_NOM_FLOW_URINE_INSTANT = 0x0002680C,
NLS_NOM_EMFC_sAPVRR = 0x04018070,
NLS_NOM_SETT_VENT_RESP_RATE_PV_APNEA = 0x0402F93A,
NLS_NOM_EMFC_LT_SEF = 0x04010800,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_SPECTRAL_EDGE_LEFT = 0x0002F853,
NLS_NOM_EMFC_Chol = 0x04010618,
NLS_NOM_CONC_CHOLESTEROL = 0x0002F16E,
NLS_NOM_EMFC_L_V1 = 0x04010764,
NLS_NOM_ECG_ELEC_POTL_V1 = 0x00020103,
NLS_NOM_EMFC_AwN2O = 0x04010518,
NLS_NOM_CONC_AWAY_N2O = 0x000251F0,
NLS_NOM_EMFC_P8_SYS = 0x0401040D,
NLS_NOM_PRESS_GEN_8_SYS = 0x0002F401,
NLS_NOM_EMFC_ICG = 0x040111A0,
NLS_NOM_IMPED_TTHOR_ECG = 0x0002F888,
NLS_NOM_EMFC_HCO3_CALC = 0x04010AA0,
NLS_NOM_CONC_HCO3_GEN_CALC = 0x0002F82E,
NLS_NOM_EMFC_sRRaw = 0x0401812C,
NLS_NOM_SETT_VENT_RESP_RATE = 0x04025022,
NLS_NOM_EMFC_sO2 = 0x0401810C,
NLS_NOM_SETT_CONC_AWAY_O2 = 0x04025164,
NLS_NOM_EMFC_sTV = 0x04018008,
NLS_NOM_SETT_VOL_AWAY_TIDAL = 0x0402513C,
NLS_NOM_EMFC_PCV = 0x04010650,
NLS_NOM_CONC_HCT_GEN = 0x00027184,
NLS_NOM_EMFC_Pmax = 0x04010678,
NLS_NOM_VENT_PRESS_AWAY_INSP_MAX = 0x0002F8BB,
NLS_NOM_EMFC_LT_PCT_BE = 0x040107D4,
NLS_NOM_EEG_PWR_SPEC_BETA_REL_LEFT = 0x0002F85F,
NLS_NOM_EMFC_sInsFl = 0x04018130,
NLS_NOM_SETT_FLOW_AWAY_INSP = 0x0402F8EC,
NLS_NOM_EMFC_UrVSht = 0x0401088C,
NLS_NOM_VOL_URINE_SHIFT = 0x0002F8CF,
NLS_NOM_EMFC_AGTLev = 0x04010870,
NLS_NOM_VOL_LVL_LIQUID_BOTTLE_AGENT = 0x0002F8C7,
NLS_NOM_EMFC_sPSV = 0x04018038,
NLS_NOM_SETT_VENT_PRESS_AWAY_PV = 0x0402F8BC,
NLS_NOM_EMFC_Urea = 0x04010AB8,
NLS_NOM_CONC_UREA_GEN = 0x0002F172,
NLS_NOM_EMFC_P8_MEAN = 0x0401040F,
NLS_NOM_PRESS_GEN_8_MEAN = 0x0002F403,
NLS_NOM_EMFC_RSCALE = 0x04010844,
NLS_NOM_EEG_ELEC_POTL_CRTX_GAIN_RIGHT = 0x0002F842,
NLS_NOM_EMFC_sRepTi = 0x04018208,
NLS_NOM_SETT_TIME_PD_TRAIN_OF_FOUR = 0x0402F8A6,
NLS_NOM_EMFC_LT_EEG = 0x040107F0,
NLS_NOM_EEG_ELEC_POTL_CRTX_LEFT = 0x0002F845,
NLS_NOM_EMFC_P3_DIA = 0x0401003A,
NLS_NOM_PRESS_GEN_3_DIA = 0x0002F0AE,
NLS_NOM_EMFC_SerPho = 0x040105A8,
NLS_NOM_CONC_P_SER = 0x0002F15E,
NLS_NOM_EMFC_eeFlow = 0x040111D0,
NLS_NOM_FLOW_AWAY_EXP_ET = 0x0002F87A,
NLS_NOM_EMFC_inAGTs = 0x04010CEC,
NLS_NOM_CONC_AWAY_AGENT_INSP_SEC = 0x0002F81F,
NLS_NOM_EMFC_iMg = 0x04010AC4,
NLS_NOM_CONC_MG_ION = 0x0002F15B,
NLS_NOM_EMFC_sFWave = 0x04018120,
NLS_NOM_SETT_VENT_FLOW_PATTERN = 0x0402F91E,
NLS_NOM_EMFC_UrOsm = 0x040101B8,
NLS_NOM_CONC_OSM_URINE = 0x0002F199,
NLS_NOM_EMFC_Paw = 0x040106BC,
NLS_NOM_PRESS_AWAY = 0x000250F0,
NLS_NOM_EMFC_DCO2 = 0x040106DC,
NLS_NOM_COEF_GAS_TRAN = 0x000251D4,
NLS_NOM_EMFC_Pmean = 0x040106C0,
NLS_NOM_PRESS_AWAY_INSP_MEAN = 0x0002510B,
NLS_NOM_EMFC_LT_PPF = 0x040107FC,
NLS_NOM_EEG_FREQ_PWR_SPEC_CRTX_PEAK_LEFT = 0x0002F84F,
NLS_NOM_EMFC_lowTV = 0x0401A030,
NLS_NOM_SETT_VENT_VOL_TIDAL_LIMIT_LO = 0x0402F94E,
NLS_NOM_EMFC_PaO2_ADJ = 0x04010A64,
NLS_NOM_CONC_PO2_ART_ADJ = 0x0002F83B,
NLS_NOM_EMFC_sPkFl = 0x0401800C,
NLS_NOM_SETT_FLOW_AWAY_INSP_MAX = 0x040250DD,
NLS_NOM_EMFC_SpPkFl = 0x0401048C,
NLS_NOM_FLOW_AWAY_MAX_SPONT = 0x0002F87D,
NLS_NOM_EMFC_sPulsD = 0x04018204,
NLS_NOM_SETT_TIME_PD_EVOK = 0x0402F908,
NLS_NOM_EMFC_BPAPTH = 0x040180C8,
NLS_NOM_SETT_VENT_TIME_PD_BIPAP_HIGH = 0x0402F93D,
NLS_NOM_EMFC_iCa = 0x04010A2C,
NLS_NOM_CONC_CA_GEN = 0x00027118,
NLS_NOM_EMFC_tCO2_CALC = 0x04010A8C,
NLS_NOM_CONC_CO2_TOT_CALC = 0x0002F826,
NLS_NOM_EMFC_sHFVAm = 0x04018140,
NLS_NOM_SETT_HFV_AMPL = 0x0402F8F3,
    NOM_PULS_OXIM_SAT_O2=0x4BB8
  };




#pragma pack(pop)
};



#define DEBUG_CONNECTION 1
#define DEBUG_DATA 2



#endif // __PH_PACKET_DEFS_H__

