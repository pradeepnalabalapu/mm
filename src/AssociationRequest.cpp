#include "AssociationRequest.h"
#include <stdio.h>
AssociationRequest::AssociationRequest(mm_data_buffer& buffer_) : buffer(buffer_) {
}

int AssociationRequest::build_message() {

	userinfo.protocol_version = MDDL_VERSION1;
	userinfo.nomenclature_version = NOMEN_VERSION;
	userinfo.functional_units = 0xFFFFFFFF;
	userinfo.system_type=SYST_CLIENT;
	userinfo.startup_mode=COLD_START;
	userinfo.option_list.count = 0;
	userinfo.option_list.length = 0;
	userinfo.supported_aprofiles.count=1;
	userinfo.supported_aprofiles.length=0; //to fill later
	AVAType poll_profile_avatype;
	poll_profile_avatype.attribute_id = NOM_POLL_PROFILE_SUPPORT;
	poll_profile_avatype.length=0;//size of poll_profile_support + optional_packages
	PollProfileSupport ppsupport;
	ppsupport.poll_profile_revision=POLL_PROFILE_REV_0;
	ppsupport.min_poll_period=800000; //800_000*1/8 ms = 100sec
	ppsupport.max_mtu_rx = 1400;
	ppsupport.max_mtu_tx = 1400;
	ppsupport.max_bw_tx = 0xFFFFFFFF;
	ppsupport.options = 0; //P_OPT_DYN_CREATE_OBJECTS|P_OPT_DYN_DELETE_OBJECTS;
	ppsupport.optional_packages.count=1;
	ppsupport.optional_packages.length=0;// size of attributeList
	AVAType ext_poll_profile;
	ext_poll_profile.attribute_id = NOM_ATTR_POLL_PROFILE_EXT;
	ext_poll_profile.length=0;
	PollProfileExt ext_poll_attr_val;
	ext_poll_attr_val.options = POLL_EXT_PERIOD_NU_1SEC | POLL_EXT_PERIOD_RTSA;
	ext_poll_attr_val.ext_attr.count=0;
	ext_poll_attr_val.ext_attr.length=0;

	ext_poll_profile.length=sizeof(ext_poll_attr_val); //8 bytes
	ppsupport.optional_packages.length=ext_poll_profile.length+4; //12 bytes
	poll_profile_avatype.length=sizeof(ppsupport)+ppsupport.optional_packages.length;//size of poll_profile_support + optional_packages
	userinfo.supported_aprofiles.length=poll_profile_avatype.length+4;
	int userDataLength=userinfo.supported_aprofiles.length +
		sizeof(MDSEUserInfoStd);
	printf("Userinfo Length=%d\n",userDataLength);

	int LI_presentation_header = sizeof(AssocReqPresentationHeader)-2 +
		1+userDataLength+
		sizeof(AssocReqPresentationTrailer);
	printf("LI_presentation_header 0x%x\n",LI_presentation_header);
	AssocReqPresentationHeader[1]=LI_presentation_header;

	sessionHeader.type = CN_SPDU_SI;
	sessionHeader.length = sizeof(AssocReqSessionData) +
	 	LI_presentation_header + 2 ;
	printf("sessionHeader.length=0x%x\n",sessionHeader.length);
	buffer.write((uint8_t *)&sessionHeader,sizeof(sessionHeader));
	buffer.write((uint8_t *)&AssocReqSessionData,sizeof(AssocReqSessionData));
	buffer.write((uint8_t *)&AssocReqPresentationHeader,sizeof(AssocReqPresentationHeader));
	buffer.write((uint8_t *)&userDataLength,1);
	buffer.write32((uint32_t *)&userinfo.protocol_version,sizeof(userinfo.protocol_version));
	buffer.write32((uint32_t *)&userinfo.nomenclature_version,
			sizeof(userinfo.nomenclature_version));
	buffer.write32((uint32_t *)&userinfo.functional_units,sizeof(userinfo.functional_units));
	buffer.write32((uint32_t *)&userinfo.system_type,sizeof(userinfo.system_type));
	buffer.write32((uint32_t *)&userinfo.startup_mode,sizeof(userinfo.startup_mode));
	buffer.write16((uint16_t *)&userinfo.option_list,sizeof(userinfo.option_list));
	buffer.write16((uint16_t *)&userinfo.supported_aprofiles,
			sizeof(userinfo.supported_aprofiles));
	buffer.write16((uint16_t *)&poll_profile_avatype,sizeof(poll_profile_avatype));
	buffer.write32((uint32_t *)&ppsupport.poll_profile_revision,4);
	buffer.write32((uint32_t *)&ppsupport.min_poll_period,4);
	buffer.write32((uint32_t *)&ppsupport.max_mtu_rx,4);
	buffer.write32((uint32_t *)&ppsupport.max_mtu_tx,4);
	buffer.write32((uint32_t *)&ppsupport.max_bw_tx,4);
	buffer.write32((uint32_t *)&ppsupport.options,4);
	buffer.write16((uint16_t *)&ppsupport.optional_packages,
			sizeof(ppsupport.optional_packages));
	buffer.write16((uint16_t *)&ext_poll_profile,sizeof(ext_poll_profile));
	buffer.write32((uint32_t *)&ext_poll_attr_val.options,
			sizeof(ext_poll_attr_val.options));
	buffer.write16((uint16_t *)&ext_poll_attr_val.ext_attr,
			sizeof(ext_poll_attr_val.ext_attr));
	buffer.write((uint8_t *)&AssocReqPresentationTrailer,
			sizeof(AssocReqPresentationTrailer));
	return 0;

}

uint8_t AssociationRequest::AssocReqSessionData[14] = {
	 	0x05,0x08,0x13,0x01, 0x00, 0x16,0x01,0x02,
	 	0x80,0x00,0x14,0x02, 0x00, 0x02 };
uint8_t AssociationRequest::AssocReqPresentationHeader[133] = {
	 	0xC1,0x00,0x31,0x80,0xA0,0x80,0x80,0x01,//offset 1 is LI length field
	 	0x01,0x00,0x00,0xA2,0x80,0xA0,0x03,0x00,
	 	0x00,0x01,0xA4,0x80,0x30,0x80,0x02,0x01,
	 	0x01,0x06,0x04,0x52,0x01,0x00,0x01,0x30,
	 	0x80,0x06,0x02,0x51,0x01,0x00,0x00,0x00,
	 	0x00,0x30,0x80,0x02,0x01,0x02,0x06,0x0C,
	 	0x2A,0x86,0x48,0xCE,0x14,0x02,0x01,0x00,
	 	0x00,0x00,0x01,0x01,0x30,0x80,0x06,0x0C,
	 	0x2A,0x86,0x48,0xCE,0x14,0x02,0x01,0x00,
	 	0x00,0x00,0x02,0x01,0x00,0x00,0x00,0x00,
	 	0x00,0x00,0x61,0x80,0x30,0x80,0x02,0x01,
	 	0x01,0xA0,0x80,0x60,0x80,0xA1,0x80,0x06,
	 	0x0C,0x2A,0x86,0x48,0xCE,0x14,0x02,0x01,
	 	0x00,0x00,0x00,0x03,0x01,0x00,0x00,0xBE,
	 	0x80,0x28,0x80,0x06,0x0C,0x2A,0x86,0x48,
	 	0xCE,0x14,0x02,0x01,0x00,0x00,0x00,0x01,
	 	0x01,0x02,0x01,0x02,0x81 };

	uint8_t AssociationRequest::AssocReqPresentationTrailer[16] = {
	 	0,0,0,0,0,0,0,0,
	 	0,0,0,0,0,0,0,0 };


