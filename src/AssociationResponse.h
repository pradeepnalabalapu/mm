#ifndef __ASSOCIATIONRESPONSE_H__
#define __ASSOCIATIONRESPONSE_H__
/************************************************************************
  AssociationResponseMessage ::=
  <AssocRespSessionHeader>
  <AssocRespSessionData>
  <AssocRespPresentationHeader>
  <AssocRespUserData>
  <AssorRespPresentationTrailer>

  AssocRespSessionHeader ::=
  <SessionHead (type := AC_SPDU_SI)>

  AssocReqUserData ::=
  <ASNLength>
  <MDSEUserInfoStd>

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include <stdio.h>
#include <string.h>
using namespace ph_packet_defs;

class AssociationResponse : public mm_message {
  protected : 
  mm_data_buffer& buffer;
  int buffer_initial_offset;
  ph_packet_defs::SessionHeader sessionHeader;
  ph_packet_defs::ROapdus roapdus;
	ph_packet_defs::MDSEUserInfoStd userinfo;


  AttributeListClass *attribute_list;
  public:
  AssociationResponse(mm_data_buffer& buffer_) ;
  void printAttributes();
  int parse_message();
  ProtocolVersion protocol_version;
  NomenclatureVersion nomenclature_version;
  FunctionalUnits functional_units;
  SystemType system_type;
  StartupMode startup_mode;
  AttributeListClass *option_listP;
  AttributeListClass *supported_aprofilesP;
  PollProfileSupport ppsupport;
  PollProfileExt ext_poll_attr_val;

};

#endif // __ASSOCIATIONRESPONSE_H__
