#include "ExtendedPollDataResult.h"
#include <stdio.h>

ExtendedPollDataResult::ExtendedPollDataResult(mm_data_buffer& buffer_, BaseController &mc_) :
  buffer(buffer_),mc(mc_)
{ 
}

int ExtendedPollDataResult::parse_message() 
{
  buffer_initial_offset = buffer.get_offset();
  //read SPpdu
  buffer.read16((uint16_t *)&sppdu,sizeof(sppdu));
  if(sppdu.session_id != 0xE100) {
    printf("ERROR : ExtendedPollDataResult::parse_message sppdu.session_id=0x%02x NOT 0xE100\n",
        sppdu.session_id);
    return -1;
  }

  //read ROapdus
  buffer.read16((uint16_t *)&roapdus, sizeof(roapdus));
  if(roapdus.ro_type != RORS_APDU && roapdus.ro_type != ROLRS_APDU) {
    printf("ERROR : ExtendedPollDataResult::parse_message roapdus.ro_type is not RORS_APDU or ROLRS_APDU, got %d\n",
        roapdus.ro_type);
    buffer.set_offset(buffer_initial_offset);
    return -1;
  }

  //read RORSapdu/ROLRSapdu
  if(roapdus.ro_type == RORS_APDU) {
    buffer.read16((uint16_t *)&rorsapdu, sizeof(rorsapdu));
    if (rorsapdu.command_type != CMD_CONFIRMED_ACTION) {
      printf("ERROR : ExtendedPollDataResult::parse_message rorsapdu.command_type=0x%02x, not CMD_CONFIFMED_ACTION\n",
          rorsapdu.command_type);
      buffer.set_offset(buffer_initial_offset);
      return -1;
    }
  } else if(roapdus.ro_type == ROLRS_APDU) {
    buffer.read((uint8_t *)&rolrsapdu.linked_id, sizeof(rolrsapdu.linked_id));
    buffer.read16((uint16_t *)&rolrsapdu.invoke_id, sizeof(rolrsapdu.invoke_id));
    buffer.read16((uint16_t *)&rolrsapdu.command_type, sizeof(rolrsapdu.command_type));
    buffer.read16((uint16_t *)&rolrsapdu.length, sizeof(rolrsapdu.length));
    if (rolrsapdu.command_type != CMD_CONFIRMED_ACTION) {
      printf("ERROR : ExtendedPollDataResult::parse_message rolrsapdu.command_type=0x%02x, not CMD_CONFIFMED_ACTION\n",
          rolrsapdu.command_type);
      buffer.set_offset(buffer_initial_offset);
      return -1;
    }
  }

  //read  Event Report Argument
  buffer.read16((uint16_t *)&action_res,sizeof(action_res));
  if(action_res.managed_object.m_obj_class != NOM_MOC_VMS_MDS) {
    buffer.set_offset ( buffer_initial_offset);
    printf("ERROR : ExtendedPollDataResult action_res.managed_object class=0x%02x, not NOM_MOC_VMS_MDS\n",
        action_res.managed_object.m_obj_class);
    return -1;
  }
  if(action_res.action_type !=NOM_ACT_POLL_MDIB_DATA_EXT) {
    printf("ERROR : ExtendedPollDataResult action_res.action_type=0x%02x, \
        not NOM_ACT_POLL_MDIB_DATA\n",action_res.action_type);
    buffer.set_offset ( buffer_initial_offset);
    return -1;
  }
  //read PollMdibDataReply
  buffer.read16((uint16_t *)&data_reply.poll_number,sizeof(data_reply.poll_number));
  buffer.read16((uint16_t *)&data_reply.sequence_no,sizeof(data_reply.sequence_no));
  buffer.read32((uint32_t *)&data_reply.rel_time_stamp,sizeof(data_reply.rel_time_stamp));
  buffer.read32((uint32_t *)&data_reply.abs_time_stamp,sizeof(data_reply.abs_time_stamp));
  buffer.read16((uint16_t *)&data_reply.polled_obj_type,sizeof(data_reply.polled_obj_type));
  buffer.read16((uint16_t *)&data_reply.polled_attr_grp,sizeof(data_reply.polled_attr_grp));
  buffer.read16((uint16_t *)&data_reply.poll_info_list,sizeof(data_reply.poll_info_list));

  printf("Poll num:0x%x sequence_num :0x%x rel_time:%s abs_time:%s Info list count=%d length=0x%x\n",
      data_reply.poll_number, data_reply.sequence_no,
      RelativeTimeClass(data_reply.rel_time_stamp).toString(),
      AbsoluteTimeClass(data_reply.abs_time_stamp).toString(),
      data_reply.poll_info_list.count, data_reply.poll_info_list.length);

  //read each singleContext
  for (int i=0; i < data_reply.poll_info_list.count; i++) {
    readSingleContextData();
  }

};

int ExtendedPollDataResult::readSingleContextData() {
  SingleContextPoll context;
  buffer.read16((uint16_t *)&context, sizeof(context));
  Handle obj_handle;
  for (int i=0; i < context.poll_info.count ; i++) {
  	std::vector<Attribute*>::iterator it;
  	std::vector<Attribute*> attributes;
    buffer.read16((uint16_t *)&obj_handle,sizeof(obj_handle));
    AttributeListClass attribute_list(buffer);
    attribute_list.parse_message();
	attributes = attribute_list.get_attributes();
  	for(it=attributes.begin(); it != attributes.end(); it++){
    	mc.setAttribute(**it); //mc.setAttribute on each attribute
		}
    attribute_list.print_attributes();
  }
};
