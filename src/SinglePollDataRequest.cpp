#include "SinglePollDataRequest.h"
#include <stdio.h>

SinglePollDataRequest::SinglePollDataRequest(mm_data_buffer& buffer_) :
  buffer(buffer_)
{ 
  sppdu.session_id = 0xE100;
  sppdu.p_context_id = 2;
  roapdus.ro_type = ROIV_APDU;
  roapdus.length = 0; //to be filled up later
  roivapdu.invoke_id=1;
  roivapdu.command_type = CMD_CONFIRMED_ACTION;
  roivapdu.length= 0; //to be filled up later
  action_arg.managed_object.m_obj_class = NOM_MOC_VMS_MDS;
  action_arg.managed_object.m_obj_inst.context_id = 0;
  action_arg.managed_object.m_obj_inst.handle = 0;
  action_arg.scope = 0;
  action_arg.action_type = NOM_ACT_POLL_MDIB_DATA;
  action_arg.length=0; //to be filled up later
  poll_data_req.poll_number = 1; //default
  poll_data_req.polled_obj_type.partition = NOM_PART_OBJ; //default
  poll_data_req.polled_obj_type.code = NOM_MOC_VMO_METRIC_NU;
  poll_data_req.polled_attr_grp = 0;

}

int SinglePollDataRequest::build_message() 
{

  //fill up lengths
  action_arg.length = sizeof(poll_data_req);
  roivapdu.length = action_arg.length + sizeof(action_arg);
  roapdus.length = roivapdu.length + sizeof(roivapdu);

  buffer.write16((uint16_t *)&sppdu,sizeof(sppdu));
  buffer.write16((uint16_t *)&roapdus,sizeof(roapdus));
  buffer.write16((uint16_t *)&roivapdu,sizeof(roivapdu));
  buffer.write16((uint16_t *)&action_arg.managed_object,
      sizeof(action_arg.managed_object));
  buffer.write32((uint32_t *)&action_arg.scope,
      sizeof(action_arg.scope));
  buffer.write16((uint16_t *)&action_arg.action_type,
      sizeof(action_arg.action_type));
  buffer.write16((uint16_t *)&action_arg.length,
      sizeof(action_arg.length));
  buffer.write16((uint16_t *)&poll_data_req,sizeof(poll_data_req));

};






