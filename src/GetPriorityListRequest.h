#ifndef __GETPRIORITYLISTREQUEST_H__
#define __GETPRIORITYLISTREQUEST_H__
/************************************************************************
  MDSPollAction ::=
  <SPpdu>
  <ROapdus (ro_type := ROIV_APDU)>
  <ROIVapdu (command_type := CMD_CONFIRMED_ACTION)>
  <ActionArgument
    (managed_object := {NOM_MOC_VMS_MDS, 0, 0},
    action_type := NOM_ACT_POLL_MDIB_DATA)>
  <PollMdibDataReq>

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include <stdio.h>
#include <string.h>
using namespace ph_packet_defs;

class GetPriorityListRequest : public mm_message {
  mm_data_buffer& buffer;
  int buffer_initial_offset;
  OIDType attributeIdListValue;


  AttributeListClass *attribute_list;
  public:
  SPpdu sppdu;
  ROapdus roapdus;
  ROIVapdu roivapdu;
  GetArgument get_arg;
  PollMdibDataReq poll_data_req;
  GetPriorityListRequest(mm_data_buffer& buffer_) ;
  int build_message();

};

#endif // __GETPRIORITYLISTREQUEST_H__
