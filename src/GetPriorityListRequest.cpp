#include "GetPriorityListRequest.h"
#include <stdio.h>

GetPriorityListRequest::GetPriorityListRequest(mm_data_buffer& buffer_) :
  buffer(buffer_)
{ 
  sppdu.session_id = 0xE100;
  sppdu.p_context_id = 2;
  roapdus.ro_type = ROIV_APDU;
  roapdus.length = 0; //to be filled up later
  roivapdu.invoke_id=1;
  roivapdu.command_type = CMD_GET;
  roivapdu.length= 0; //to be filled up later 
  get_arg.managed_object.m_obj_class = NOM_MOC_VMS_MDS;
  get_arg.managed_object.m_obj_inst.context_id = 0;
  get_arg.managed_object.m_obj_inst.handle = 0;
  get_arg.scope = 0;
  get_arg.attributeIdList.count=1;
  get_arg.attributeIdList.length=sizeof(attributeIdListValue)*get_arg.attributeIdList.count;
  attributeIdListValue = NOM_ATTR_POLL_RTSA_PRIO_LIST;

}

int GetPriorityListRequest::build_message() 
{

  //fill up lengths
  roivapdu.length = sizeof(get_arg) + sizeof(attributeIdListValue);
  roapdus.length = roivapdu.length + sizeof(roivapdu);

  buffer.write16((uint16_t *)&sppdu,sizeof(sppdu));
  buffer.write16((uint16_t *)&roapdus,sizeof(roapdus));
  buffer.write16((uint16_t *)&roivapdu,sizeof(roivapdu));
  buffer.write16((uint16_t *)&get_arg.managed_object,
      sizeof(get_arg.managed_object));
  buffer.write32((uint32_t *)&get_arg.scope,
      sizeof(get_arg.scope));
  buffer.write16((uint16_t *)&get_arg.attributeIdList,
      sizeof(get_arg.attributeIdList));
  buffer.write16((uint16_t *)&attributeIdListValue, 
      sizeof(attributeIdListValue));
};






