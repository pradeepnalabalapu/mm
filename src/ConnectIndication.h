#ifndef __CONNECTINDICATION_H__
#define __CONNECTINDICATION_H__
/************************************************************************
  ConnectIndication ::= 
    <Nomenclature>
    <ROapdus (ro_type := ROIVapdu) >
    <ROIVapdu (command_type := CMD_EVENT_REPORT) >
    <EventReportArgument 
        (managed_object :={NOM_MOC_MC|DS_COMPOS_SINGLE_BED,0,0},
         event_type := NOM_NOTI_MDS_CONNECT_INDIC)>
    <ConnectIndInfo>

  typedef u_32 Nomenclature; //0x00_00_major_minor version
  typedef AttributeList ConnectIndInfo;

Attribute ID: NOM_ATTR_SYS_TYPE //system Type
Attribute Type : TYPE

Attribute ID: NOM_ATTR_PCOL_SUPPORT //Protocol support
Attribute Type: ProtoSupport

Attribute ID:  NOM_ATTR_LOCALIZN //System localization
Attribute Type : SystemLocal

Attribute ID:  NOM_ATTR_NET_ADDR_INFO
Attribute Type: IpAddressInfo

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"

class ConnectIndication : public mm_message {
  mm_data_buffer& buffer;
  int buffer_initial_offset;
  ph_packet_defs::nomenclature nomen;
  ph_packet_defs::ROapdus roapdus;
  ph_packet_defs::ROIVapdu roivapdu;
  ph_packet_defs::EventReportArgument event_report_arg;
  AttributeListClass *attribute_list;
  std::string monitor_ip_address;
  int data_export_port_number;
  bool is_data_export_supported;
  public:
  ConnectIndication(mm_data_buffer& buffer_) ;
  void printAttributes();
  bool isDataExportSupported();
  int getDataExportPortNumber();
  std::string getMonitorIpAddress();
  int parse_message();
};

#endif // __CONNECTINDICATION_H__
