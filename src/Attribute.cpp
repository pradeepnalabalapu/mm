
#include "enums.h"
#include "Attribute.h"
#include "math.h"
#include "RelativeTimeClass.h"
#include "AbsoluteTimeClass.h"
#include "MeasurementStateClass.h"

using namespace ph_packet_defs;


Attribute::Attribute(mm_data_buffer& buffer,  size_t word_size_) :
  word_size(word_size_)
{
  buffer.read16((uint16_t *)&type);
  buffer.read16((uint16_t *)&length);
  dataP = (uint8_t *)malloc(length);
  buffer.read(dataP,length);
  set_value();
}

void Attribute::set_word_size(size_t word_size_) {
  word_size = word_size_;
  set_value();
};

void Attribute::set_value(){
  valueP = malloc(length);
  if (word_size == 2){
    for(int i=0; i < int(length/2); i++) {
      *((uint16_t *)valueP+i) = ntohs(*((uint16_t *)dataP+i));
    }
  } else if (word_size == 4) { 
    for(int i=0; i < int(length/4); i++) {
      *((uint32_t *)valueP+i) = ntohl(*((uint32_t *)dataP+i));
    }
  } else if (word_size == 1) {
    valueP = dataP;
  } 
}

void *Attribute::get_valueP(){
  return valueP;
}

void Attribute::print() {
    AttributeId_enum_t attrId = (AttributeId_enum_t)type;
    if(attrId == NOM_ATTR_ID_HANDLE) {
      printf("\n");
    }
    if (enums::AttributeId_map.find(attrId) == enums::AttributeId_map.end()) {
      printf("Attribute type=0x%04x", attrId);
    } else {
      std::string Attribute_enum_str = enums::AttributeId_map[attrId];
      printf("Attribute type=%s",Attribute_enum_str.c_str());
    }
    switch(attrId) {
      case NOM_ATTR_ID_LABEL_STRING:
      case NOM_ATTR_METRIC_INFO_LABEL_STR :
        {
          int str_len = *(uint16_t *)valueP;
          printf("   ");
          for(int i=0; i< str_len ; i++){
            uint16_t c = *((uint16_t *)valueP+1+i);
            if ( c == 0 ) break;
            printf("%c",c);
          }
          break;
        }
      case NOM_ATTR_METRIC_STAT: 
        {
          uint16_t metric_state = *(uint16_t *)valueP;
          printf("    %s",metric_state == 0x8000 ? "OFF" : "ON");
          break;
        }
      case NOM_ATTR_UNIT_CODE : 
        {
          uint16_t unit_code =*(uint16_t *)valueP;
          printf("    0x%04x",unit_code);
          break;
        }
      case NOM_ATTR_ID_HANDLE : 
        {
          unsigned int val;
          if(length == 2) {
             val = *(uint16_t *) valueP;
          } else if (length == 4) {
            val = ntohl(*(uint32_t *)dataP);
          } else {
            val = *(uint8_t *)dataP;
          }
          printf("    0x%x",val);
          break;
        }
      case NOM_ATTR_ID_LABEL :
        {
          TextId textId = ntohl(*(uint32_t *)dataP);
          printf("    0x%08x",textId);
          break;
        }
      case  NOM_ATTR_COLOR :
        {
          SimpleColor_enum_t simplecolor=(SimpleColor_enum_t)*(uint16_t *)valueP;
          if(enums::SimpleColor_map.find(simplecolor) != enums::SimpleColor_map.end()) {
            printf("   %s",enums::SimpleColor_map[simplecolor].c_str());
          }
          break;
        }
      case NOM_ATTR_METRIC_SPECN : 
        {
          
          RelativeTimeClass update_period( ntohl(*((uint32_t *)dataP)) );
          MetricCategory_enum_t category = (MetricCategory_enum_t) *((uint16_t *)valueP+2);
          //MetricAccess_enum_t access = (MetricAccess_enum_t)*( (uint16_t *)valueP+3);
          const char *category_str = enums::MetricCategory_map[category].c_str();

          printf("    Update Period = %s category=%s",update_period.toString(),category_str);
          break;
        }
      case NOM_ATTR_NU_VAL_OBS : 
        {
          NuObsValue nuObjs;
          nuObjs.physio_id = *(uint16_t *)valueP;
          nuObjs.state = *((uint16_t *)valueP+1);
          nuObjs.unit_code = *((uint16_t *)valueP+2);
          FLOATType value(ntohl(*(uint32_t *)(((uint8_t *)dataP)+6)));
          std::string state_str = MeasurementStateClass(nuObjs.state).toString();
          
          
          printf("   physio_id=0x%x state=%s unit_code=0x%x value=%s",
              nuObjs.physio_id,state_str.c_str(),nuObjs.unit_code,value.toString());
          break;
        }
      case  NOM_ATTR_ID_TYPE :
        {
          NomPartition partition = *(uint16_t *)valueP;
          obj_class_enum_t code = (obj_class_enum_t)(*((uint16_t *)valueP+1));
          if ((partition == 1)  && (enums::ObjClassType_map.find(code) != enums::ObjClassType_map.end()) ) {
            std::string ObjClassType_str = enums::ObjClassType_map[code];
            printf("   %s",ObjClassType_str.c_str());
            break;
          } else {
            //else execute default action
          }
        }
      case NOM_ATTR_TIME_STAMP_ABS:
      case NOM_ATTR_TIME_ABS : 
        {
          AbsoluteTimeClass at(*(AbsoluteTime *)dataP);
          printf("     %s",at.toString());
          break;
        }
      case NOM_ATTR_TIME_STAMP_REL:
      case NOM_ATTR_TIME_REL : 
        {
          RelativeTimeClass rel(ntohl(*(uint32_t *)dataP));
          printf("     %s",rel.toString());
          break;
        }
      case NOM_ATTR_TIME_PD_SAMP:
        {
          RelativeTimeClass pd_samp(ntohl(*(uint32_t *)dataP));
          printf("    Sample Period = %s",pd_samp.toString());
          break;
        }
      case NOM_ATTR_SA_FIXED_VAL_SPECN :
        {
          int count = *(uint16_t *)valueP;
          int length = *((uint16_t *)valueP+1);
          for (int i=0; i<count; i++) {
            int id = *((uint16_t *)valueP+2+2*i);

            printf("    ID : %s Val : %x ",
                id == 0 ? "UNSPEC" :
                id == 1 ? "INVALID_MASK" :
                id == 2 ? "PACER_MASK" :
                id == 3 ? "DEFIB_MARKER_MASK" :
                id == 4 ? "SATURATION" :
                id == 5 ? "QRS_MASK" : "ERROR",
                *((uint16_t *)valueP+3+2*i));
          }
          break;
        }
      case NOM_ATTR_SCALE_SPECN_I16:
        {
          FLOATType lower_abs_val(ntohl(*(uint32_t *)dataP));
          FLOATType upper_abs_val(ntohl(*(uint32_t *)dataP+1));
          uint16_t lower_scaled_value = *((uint16_t *)valueP+4);
          uint16_t upper_scaled_value = *((uint16_t *)valueP+5);
          printf("    lower_abs : %s upper_abs: %s lower_scaled : %x upper_scaled : %x",
              lower_abs_val.toString(),upper_abs_val.toString(),
              lower_scaled_value,upper_scaled_value);
          break;
        }
      case NOM_ATTR_GRID_VIS_I16:
        {
          int count = *(uint16_t *)valueP;
          int length = *((uint16_t *)valueP+1);
          for (int i=0; i<count; i++) {
            FLOATType absolute_value(ntohl(*((uint32_t *)dataP+1+2*i)));
            int scaled_value =*((uint16_t *)valueP+2+4*i+2);
            int level =*((uint16_t *)valueP+2+4*i+3);
            printf("\n        abs_val :%s scaled_val: %x level: %d ::",absolute_value.toString(),scaled_value,level);
          }
          break;
        }
      case NOM_ATTR_SA_SPECN:
        {
          uint16_t array_size = *(uint16_t *)valueP;
          uint8_t  sample_size = *((uint8_t *)dataP+2);
          uint8_t  significant_bits = *((uint8_t *)dataP+3); //TODO - ignored for now
          printf("    array_size=%d sample_size=%d significant_bits=%d",
              array_size,sample_size,significant_bits);
          break;
        }
      case NOM_ATTR_SA_VAL_OBS:
        {
  
          SaObsValue saObjs;
          unsigned int physio_id = saObjs.physio_id = *(uint16_t *)valueP;
          saObjs.state = *((uint16_t *)valueP+1);
          uint16_t length = *((uint16_t *)valueP+2);
          std::string state_str = MeasurementStateClass(saObjs.state).toString();
          unsigned int num_bytes=1;
					

          printf("   physio_id=0x%x state=%s length=%d :",
              saObjs.physio_id,state_str.c_str(),length);
          printf("\n   data :");
          for(int i=0; i< length; i+=num_bytes) {
            switch (num_bytes) {
              case 1 : 
                {
                  printf("0x%02x ",*((uint8_t *)dataP + 6 +i));
                  break;
                }
              case 2:
                {
                  printf("0x%04x ",*((uint16_t *)valueP + 3 +(i>>1)) );
                  break;
                }
              case 4:
                {
                  printf("0x%08x ",ntohl(*(uint32_t *)(dataP + 6 + i)) ) ;
                  break;
                }
            }
          }

          break;
        }
     default : 
        {
          printf("   ");
          for(int i=0; i<int(length); i++){
            printf(" 0x%02x",*((uint8_t *)dataP+i));
          }
          printf(" length=%d",length);
        }
    };

}

Attribute::~Attribute(){
  if (dataP) {
    free(dataP);
  };
  if(valueP){
    free(valueP);
  };
}



