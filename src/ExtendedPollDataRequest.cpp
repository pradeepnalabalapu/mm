#include "ExtendedPollDataRequest.h"
#include <stdio.h>

ExtendedPollDataRequest::ExtendedPollDataRequest(mm_data_buffer& buffer_) :
  buffer(buffer_) 
{ 
  sppdu.session_id = 0xE100;
  sppdu.p_context_id = 2;
  roapdus.ro_type = ROIV_APDU;
  roapdus.length = 0; //to be filled up later
  roivapdu.invoke_id=1;
  roivapdu.command_type = CMD_CONFIRMED_ACTION;
  roivapdu.length= 0; //to be filled up later
  action_arg.managed_object.m_obj_class = NOM_MOC_VMS_MDS;
  action_arg.managed_object.m_obj_inst.context_id = 0;
  action_arg.managed_object.m_obj_inst.handle = 0;
  action_arg.scope = 0;
  action_arg.action_type = NOM_ACT_POLL_MDIB_DATA_EXT;
  action_arg.length=0; //to be filled up later
  poll_data_req.poll_number = 1; //default
  poll_data_req.polled_obj_type.partition = NOM_PART_OBJ; //default
  poll_data_req.polled_obj_type.code = NOM_MOC_VMO_METRIC_SA_RT;
  //poll_data_req.polled_attr_grp = NOM_ATTR_GRP_METRIC_VAL_OBS;
  //poll_data_req.polled_obj_type.code = NOM_MOC_VMO_METRIC_NU;
  poll_data_req.polled_attr_grp = 0;
  poll_data_req.poll_ext_attr.count= 1;
  poll_data_req.poll_ext_attr.length=0; //to be filled up later
  

}

int ExtendedPollDataRequest::build_message() 
{

  RelativeTime pollDataReqPeriod=8*60*1000*1000; //8*128us * 60* 10^6 = 1s
  AVAType timePeriodic;
  timePeriodic.attribute_id = NOM_ATTR_TIME_PD_POLL;
  timePeriodic.length = sizeof(pollDataReqPeriod);
  if(poll_data_req.poll_ext_attr.count != 0) {
    poll_data_req.poll_ext_attr.length = sizeof(timePeriodic) + timePeriodic.length;
  } else {
    poll_data_req.poll_ext_attr.length = 0;
  }

  //fill up lengths
  action_arg.length = sizeof(poll_data_req) + poll_data_req.poll_ext_attr.length;
  roivapdu.length = action_arg.length + sizeof(action_arg);
  roapdus.length = roivapdu.length + sizeof(roivapdu);

  buffer.write16((uint16_t *)&sppdu,sizeof(sppdu));
  buffer.write16((uint16_t *)&roapdus,sizeof(roapdus));
  buffer.write16((uint16_t *)&roivapdu,sizeof(roivapdu));
  buffer.write16((uint16_t *)&action_arg.managed_object,
      sizeof(action_arg.managed_object));
  buffer.write32((uint32_t *)&action_arg.scope,
      sizeof(action_arg.scope));
  buffer.write16((uint16_t *)&action_arg.action_type,
      sizeof(action_arg.action_type));
  buffer.write16((uint16_t *)&action_arg.length,
      sizeof(action_arg.length));
  buffer.write16((uint16_t *)&poll_data_req,sizeof(poll_data_req));
  if(poll_data_req.poll_ext_attr.count != 0 ) {
    buffer.write16((uint16_t *)&timePeriodic, sizeof(timePeriodic));
    buffer.write32(&pollDataReqPeriod);
  }

};






