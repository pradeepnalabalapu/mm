#ifndef __BASECONTROLLER_H__
#define __BASECONTROLLER_H__
#include "wave_info.h"
#include "Attribute.h"
#include <map>

class BaseController {

  public :
    std::map<unsigned int, wave_info> wave_info_map;
    wave_info current_wave_info;
    int osd;
    BaseController() {};
		virtual void setAttribute(Attribute& attribute) {}; 
};

#endif// __BASECONTROLLER_H__
