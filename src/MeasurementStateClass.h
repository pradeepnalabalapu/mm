#ifndef __MeasurementStateClass_H__
#define __MeasurementStateClass_H__
#include "ph_packet_defs.h"
#include "enums.h"

class MeasurementStateClass {
  uint16_t state;
  public:
  MeasurementStateClass(uint16_t state_) : state(state_) {};
  std::string toString();
};



#endif //__MeasurementStateClass_H__
