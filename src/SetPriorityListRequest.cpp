#include "SetPriorityListRequest.h"
#include <stdio.h>

SetPriorityListRequest::SetPriorityListRequest(mm_data_buffer& buffer_) :
  buffer(buffer_)
{ 
  sppdu.session_id = 0xE100;
  sppdu.p_context_id = 2;
  roapdus.ro_type = ROIV_APDU;
  roapdus.length = 0; //to be filled up later
  roivapdu.invoke_id=1;
  roivapdu.command_type = CMD_CONFIRMED_SET;
  roivapdu.length= 0; //to be filled up later 
  set_arg.managed_object.m_obj_class = NOM_MOC_VMS_MDS;
  set_arg.managed_object.m_obj_inst.context_id = 0;
  set_arg.managed_object.m_obj_inst.handle = 0;
  set_arg.scope = 0;
  set_arg.modificationList.count=1;
  set_arg.modificationList.length=0; //to be filled up later

}

int SetPriorityListRequest::build_message() 
{

  AttributeModEntry attrModEntry;
  attrModEntry.modifyOperator =  REPLACE;
  attrModEntry.attribute.attribute_id = NOM_ATTR_POLL_RTSA_PRIO_LIST;
  TextIdList textIdList;
  textIdList.count=1;
  TextId textIdListValue1 = NLS_NOM_PULS_OXIM_PLETH; //LABEL for SPO2
  //TextId textIdListValue2 = NOM_PULS_OXIM_SAT_O2; //Value for SPO2
  textIdList.length = sizeof(textIdListValue1) ;// + sizeof(textIdListValue2);
  attrModEntry.attribute.length=sizeof(textIdList) + textIdList.length;
  set_arg.modificationList.length = sizeof(attrModEntry)+attrModEntry.attribute.length;
  //fill up lengths
  roivapdu.length = sizeof(set_arg) + set_arg.modificationList.length;
  roapdus.length = roivapdu.length + sizeof(roivapdu);

  buffer.write16((uint16_t *)&sppdu,sizeof(sppdu));
  buffer.write16((uint16_t *)&roapdus,sizeof(roapdus));
  buffer.write16((uint16_t *)&roivapdu,sizeof(roivapdu));
  buffer.write16((uint16_t *)&set_arg.managed_object,
      sizeof(set_arg.managed_object));
  buffer.write32((uint32_t *)&set_arg.scope, sizeof(set_arg.scope));
  buffer.write16((uint16_t *)&set_arg.modificationList,
      sizeof(set_arg.modificationList));
  buffer.write16((uint16_t *)&attrModEntry, sizeof(attrModEntry));
  buffer.write16((uint16_t *)&textIdList, sizeof(textIdList));
  buffer.write32((uint32_t *)&textIdListValue1, sizeof(textIdListValue1));
  //buffer.write32((uint32_t *)&textIdListValue2, sizeof(textIdListValue2));
};






