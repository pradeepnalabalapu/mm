#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <execinfo.h>
#include <unistd.h>
#include "Udp.h"
#include "generic_functions.h"

Udp::Udp(int port, string address) 
  : m_lsd(0), m_port(port),  m_listening(false),
	m_address(address)
{
};


Udp::~Udp()
{
 	//generic_functions::print_stack_trace(STDOUT_FILENO);
  if (m_lsd > 0){
    close(m_lsd);
		m_lsd=0;
  }
};


int Udp::start() 
{

  if(m_listening == true) {
    return 0;
  }

  m_lsd = socket(AF_INET, SOCK_DGRAM, 0);
	if (m_lsd < 0) {
		perror("ERROR opening socket");
	}
  //Specify socket to reuse address - needed when we restart server
  int reuse_enable = 1; 
  if (setsockopt(m_lsd, SOL_SOCKET, SO_REUSEADDR, &reuse_enable, sizeof(int)) < 0 ) {
    perror("ERROR setting socket option SO_REUSEADDR");
  }
  
  struct sockaddr_in addr;
  bzero((char *)&addr,sizeof(addr));
  addr.sin_family = AF_INET;
	addr.sin_port = htons(m_port);
	if (m_address.size() > 0) {
    inet_pton(AF_INET, m_address.c_str(), &(addr.sin_addr));
  } else {
		addr.sin_addr.s_addr = htonl(INADDR_ANY);
	}

  int result = bind(m_lsd, (struct sockaddr*)&addr, sizeof(addr));
  if (result != 0) {
    perror("bind() failed");
    return result;
  }
  m_listening = true;
  return result;
};



ssize_t Udp::receive(uint8_t *buffer, size_t len, struct sockaddr_in* client_address) 
{
	int retval = 0;
	int flags = 0;
	if (client_address == NULL ) {
		retval= recv(m_lsd,buffer,len,flags);
	} else {
		socklen_t addr_len=sizeof(struct sockaddr_in);
		retval= recvfrom(m_lsd,buffer,len,0,(struct sockaddr*)client_address,&addr_len);
		/*
		if(addr_len > 0 ) {
			printf("\tRECVFROM %s\n",inet_ntoa(client_address->sin_addr));
			printf("\t\t");
			for (int i=0; i < addr_len; i++) {
				printf("%02x ",*(buffer+i));
				if (i%16 == 15) { printf("\n\t\t"); }
			}
		}*/
	}
	return retval;
};

ssize_t Udp::send(uint8_t *buffer, size_t len,struct sockaddr_in* to_addressP)
{
	set_to_address(to_addressP);
  return sendto(m_lsd,buffer,len,0,(struct sockaddr*)&to_address,sizeof(to_address));
}

void Udp::set_to_address(struct sockaddr_in* to_addressP) {
	if (to_addressP != NULL ) {
  	memcpy(&to_address,(void *)to_addressP,sizeof(struct sockaddr_in));
	}
}


int Udp::bind_to_device(const char* if_name) 
{
  int ret_val = setsockopt(m_lsd, SOL_SOCKET, SO_BINDTODEVICE, if_name, strlen(if_name));
  return ret_val;
}

int Udp::enable_broadcast() 
{
  int broadcastEnable=1;
  return setsockopt(m_lsd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable,sizeof(broadcastEnable)) ;
}


int Udp::settimeout(int secs, int usecs) 
{
  struct timeval tv;
  tv.tv_sec = secs;
  tv.tv_usec = usecs;

  return setsockopt(m_lsd, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv));
}
