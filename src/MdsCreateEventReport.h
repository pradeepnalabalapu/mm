#ifndef __MDSCREATEEVENTREPORT_H__
#define __MDSCREATEEVENTREPORT_H__
/************************************************************************
  MDSCreateEventReport ::=
  <SPpdu>
  <ROapdus (ro_type := ROIV_APDU)>
  <ROIVapdu (command_type := CMD_CONFIRMED_EVENT_REPORT) >
  <EventReportArgument (event_type := NOM_NOTI_MDS_CREAT)>
  <MDSCreateInfo>

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include <stdio.h>
#include <string.h>
using namespace ph_packet_defs;

class MdsCreateEventReport : public mm_message {
  mm_data_buffer& buffer;
  int buffer_initial_offset;
  public:
  SPpdu sppdu;
  ROapdus roapdus;
  ROIVapdu roivapdu;
  EventReportArgument event_report_arg;
  ManagedObjectId managed_object;
  AttributeListClass *attribute_list;

  MdsCreateEventReport(mm_data_buffer& buffer_) ;
  int parse_message();
  void print_attributes();

};

#endif // __MDSCREATEEVENTREPORT_H__
