#ifndef __BOOTP_H__
#define __BOOTP_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "ph_packet_defs.h"

#define BOOTP_SERVER_PORT 67
#define BOOTP_CLIENT_PORT 68

// Data offsets within bootp reply payload
#define YIADDR_OFFSET 16
#define SIADDR_OFFSET 20
#define FILE_OFFSET   108
#define VEND_OFFSET   236
#define VEND_HW_ADDR_OFFSET 240

//DHCP MESSAGE 
#define DHCPDISCOVER 1
#define DHCPOFFER 2
#define DHCPREQUEST 3
#define DHCPDECLINE 4
#define DHCPACK 5
#define DHCPNAK 6
#define DHCPRELEASE 7
#define DHCPINFORM 8


// DHCP options
#define DHCP_CODE_END 0xFF
#define DHCP_CODE_NETMASK 0x1
#define DHCP_CODE_ROUTER 0x3
#define DHCP_CODE_VENDORSPECIFIC 0x2B
#define DHCP_CODE_LEASE_TIME 0x33
#define DHCP_CODE_MSGTYPE 0x35
#define DHCP_CODE_SERVER 0x36


struct dhcp_command {
   uint8_t cmd;
   uint8_t length;
   uint8_t *data;
};
   

extern int set_ip_address(char * if_name, const char ip_addr[]);
extern int bootp_server(unsigned int server_port_num, unsigned client_port_num, char *if_name, char *client_ip_addr, char *server_ip_addr, unsigned int debug_level, ph_packet_defs::MACAddress& mac_address) ;
extern void assemble_dhcp_reply(uint8_t *buffer, int num_bytes_recvd, char *client_ip_addr, char *server_ip_addr, 
    ph_packet_defs::MACAddress& mac_address);


#endif //__BOOTP_H__
