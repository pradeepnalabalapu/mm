/****************************************************/
/*  Author : Pradeep Nalabalapu                     */
/* This file implements some Network functions      */
/****************************************************/
#ifndef __NETWORKUTILS_H__
#define __NETWORKUTILS_H__
#include <stdio.h>
#include <vector>
#include <string>
#include <stdint.h>
#include "ph_packet_defs.h"

#define MAX_DHCP_COMMANDS 10
#define BOOTP_BUFFER_SIZE 1024

#define BOOTP_SERVER_PORT 67
#define BOOTP_CLIENT_PORT 68

// Data offsets within bootp reply payload
#define YIADDR_OFFSET 16
#define SIADDR_OFFSET 20
#define FILE_OFFSET   108
#define VEND_OFFSET   236
#define VEND_HW_ADDR_OFFSET 240

//DHCP MESSAGE 
#define DHCPDISCOVER 1
#define DHCPOFFER 2
#define DHCPREQUEST 3
#define DHCPDECLINE 4
#define DHCPACK 5
#define DHCPNAK 6
#define DHCPRELEASE 7
#define DHCPINFORM 8


// DHCP options
#define DHCP_CODE_END 0xFF
#define DHCP_CODE_NETMASK 0x1
#define DHCP_CODE_ROUTER 0x3
#define DHCP_CODE_VENDORSPECIFIC 0x2B
#define DHCP_CODE_LEASE_TIME 0x33
#define DHCP_CODE_MSGTYPE 0x35
#define DHCP_CODE_SERVER 0x36


struct dhcp_command {
   uint8_t cmd;
   uint8_t length;
   uint8_t *data;
};


class NetworkUtils {

	
	public :

		struct iface {
			std::string name;
			std::string ip_addr;
			ph_packet_defs::MACAddress mac_addr;
		};

		static int list_network_interfaces();
	  static std::string get_ip_addr (std::string ifname);
	  static std::string get_netmask (std::string ifname);
		static void get_ethernet_interfaces(std::vector<iface*>& ifacePVec);
		static int set_ip_address(std::string if_name, std::string ip_addr) ;
		static std::string generate_client_ip_addr(std::string server_ip_addr);
		static void assemble_dhcp_reply(uint8_t *buffer,int num_bytes_recvd, std::string client_ip_addr, std::string server_ip_addr, ph_packet_defs::MACAddress& mac_address, std::string netmask); 

		static int bootp_server(unsigned int server_port_num, unsigned int client_port_num, std::string if_name, std::string& client_ip_addr, std::string server_ip_addr, unsigned int debug, ph_packet_defs::MACAddress& mac_address);
};
#endif // __NETWORKUTILSLINUX_H__
