#ifndef __GENERIC_FUNCTIONS_H__
#define __GENERIC_FUNCTIONS_H__

#include <stdio.h>
#include <stdint.h>
namespace generic_functions {
  uint8_t *find_subsequence(uint8_t *haystack, size_t haystack_size,uint8_t * needle, size_t needle_size );
	void print_stack_trace(int out_fd, int NUM_FUNCTIONS=10);
}


#endif // __GENERIC_FUNCTIONS_H__
