/****************************************************/
/*  Author : Pradeep Nalabalapu                     */
/* This file implements some Network functions      */
/****************************************************/
#include "NetworkUtils.h"
#include "Udp.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_link.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/wireless.h>
#define DEBUG_CONNECTION 1
#define DEBUG_DATA 2

int get_if_addrs_NUL()
       {
           struct ifaddrs *ifaddr, *ifa;
           int family, s;
           char host[NI_MAXHOST];

           if (getifaddrs(&ifaddr) == -1) {
               perror("getifaddrs");
               exit(EXIT_FAILURE);
           }

           /* Walk through linked list, maintaining head pointer so we
              can free list later */

           for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
               if (ifa->ifa_addr == NULL)
                   continue;

               family = ifa->ifa_addr->sa_family;
/* Display interface name and family (including symbolic
                  form of the latter for the common families) */

               printf("%-8s %s (%d)\n",
                      ifa->ifa_name,
                      (family == AF_PACKET) ? "AF_PACKET" :
                      (family == AF_INET) ? "AF_INET" :
                      (family == AF_INET6) ? "AF_INET6" : "???",
                      family);

               /* For an AF_INET* interface address, display the address */

               if (family == AF_INET || family == AF_INET6) {
                   s = getnameinfo(ifa->ifa_addr,
                           (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                                 sizeof(struct sockaddr_in6),
                           host, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
                   if (s != 0) {
                       printf("getnameinfo() failed: %s\n", gai_strerror(s));
                       exit(EXIT_FAILURE);
                   }

                   printf("\t\taddress: <%s>\n", host);

								} else if (family == AF_PACKET && ifa->ifa_data != NULL) {
                   struct rtnl_link_stats *stats = (rtnl_link_stats *)ifa->ifa_data;

                   printf("\t\ttx_packets = %10u; rx_packets = %10u\n"
                          "\t\ttx_bytes   = %10u; rx_bytes   = %10u\n",
                          stats->tx_packets, stats->rx_packets,
                          stats->tx_bytes, stats->rx_bytes);
               }
           }

           freeifaddrs(ifaddr);
           return(EXIT_SUCCESS);
       }

/* Returns an array of iface* pointers. Caller needs to free up memory */
void NetworkUtils::get_ethernet_interfaces(std::vector<NetworkUtils::iface*>& ifacePVec) {
	struct ifaddrs *ifaddr, *ifa, *ifb;
	int family, b_family, s;
	char host[NI_MAXHOST];
	char wname[IFNAMSIZ];
	int sock = -1;
	struct iwreq pwrq;
	struct ifreq ifr;
	bool isWifi = false;
	bool hasHwaddr = false;
	unsigned char mac_address[6];
	int count_ethernet_if=0;

	if (getifaddrs(&ifaddr) == -1) {
	 	 perror("getifaddrs");
	 	 exit(EXIT_FAILURE);
	}

	/* Walk through linked list, maintaining head pointer so we
	 	can free list later */

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		//look at AF_PACKET interfaces to get all interface names
	 	if (ifa->ifa_addr == NULL ||
		 	ifa->ifa_name == NULL )
	 	 	continue;

	 	family = ifa->ifa_addr->sa_family;
		//discard interfaces that are probably virtual
		if ( family != AF_PACKET )
			continue;


		if ( strcmp(ifa->ifa_name,"lo") == 0 ||
			strncmp(ifa->ifa_name,"docker",5) == 0 ||
		 	strncmp(ifa->ifa_name,"vir",3) == 0)
			continue;
		



		//create a socket in order to do some ioctl commands
		if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
			perror("socket");
			return;
		}

		//probe for wireless name to determine if interfaces is wireless
		isWifi = false;
		memset(&pwrq, 0, sizeof(pwrq));
		strncpy(pwrq.ifr_name, ifa->ifa_name, IFNAMSIZ);
		if (ioctl(sock, SIOCGIWNAME, &pwrq) != -1) {
			strncpy(wname, pwrq.u.name, IFNAMSIZ);
			isWifi=true;
			close(sock);
			continue;
		}

		//get mac address of interface.
		strncpy(ifr.ifr_name,ifa->ifa_name,IFNAMSIZ);
		hasHwaddr=false;
		if(ioctl(sock, SIOCGIFHWADDR,&ifr) == 0) {
			memcpy(mac_address,ifr.ifr_hwaddr.sa_data,6);
			hasHwaddr=true;
			//discard interfaces with mac_addr starting with 0:0:0
			if (strcmp((const char*)mac_address,"\0\0\0") == 0 ) {
				close(sock);
				continue;
			}
		} 
		close(sock);
		
		//if we reach here, we found an ethernet iface
		iface* m_if = new iface();
		m_if->name = (const char*)ifa->ifa_name;
		memcpy(&m_if->mac_addr, &mac_address,6);

		count_ethernet_if++;
		/*
		if (family == AF_PACKET && ifa->ifa_data != NULL) {
			if (hasHwaddr) {
				printf("%-8s\t\t %02x:%02x:%02x:%02x:%02x:%02x\n",
						ifa->ifa_name,mac_address[0],mac_address[1],mac_address[2],mac_address[3],mac_address[4],mac_address[5]);
			} 
		}
		*/

		//get ip_address by looking for AF_INET or AF_INET6 with same name
		for (ifb = ifaddr; ifb != NULL; ifb = ifb->ifa_next) {
			if (strcmp(ifa->ifa_name,ifb->ifa_name) != 0 ||
					ifb->ifa_addr->sa_family != AF_INET)

				continue;
			b_family = ifb->ifa_addr->sa_family;
			if (b_family == AF_INET ) {
			 	 s = getnameinfo(ifb->ifa_addr,
			 					 (b_family == AF_INET) ? sizeof(struct sockaddr_in) :
			 																 sizeof(struct sockaddr_in6),
			 					 host, NI_MAXHOST,
			 					 NULL, 0, NI_NUMERICHOST);
			 	 if (s != 0) {
			 			 printf("getnameinfo() failed: %s\n", gai_strerror(s));
			 			 exit(EXIT_FAILURE);
			 	 }

			 	 //printf("\t\taddress: %s\n", host);
				 m_if->ip_addr = std::string(host);
			}
		}
		ifacePVec.push_back(m_if);
	}


	freeifaddrs(ifaddr);
	return ;
}


int NetworkUtils::list_network_interfaces(){
           struct ifaddrs *ifaddr, *ifa;
           int family, s;
           char host[NI_MAXHOST];

           if (getifaddrs(&ifaddr) == -1) {
               perror("getifaddrs");
               exit(EXIT_FAILURE);
           }

           /* Walk through linked list, maintaining head pointer so we
              can free list later */

           for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
               if (ifa->ifa_addr == NULL)
                   continue;

               family = ifa->ifa_addr->sa_family;
/* Display interface name and family (including symbolic
                  form of the latter for the common families) */

               printf("%-8s %s (%d)\n",
                      ifa->ifa_name,
                      (family == AF_PACKET) ? "AF_PACKET" :
                      (family == AF_INET) ? "AF_INET" :
                      (family == AF_INET6) ? "AF_INET6" : "???",
                      family);

               /* For an AF_INET* interface address, display the address */

               if (family == AF_INET || family == AF_INET6) {
                   s = getnameinfo(ifa->ifa_addr,
                           (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                                 sizeof(struct sockaddr_in6),
                           host, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
                   if (s != 0) {
                       printf("getnameinfo() failed: %s\n", gai_strerror(s));
                       exit(EXIT_FAILURE);
                   }

                   printf("\t\taddress: <%s>\n", host);

								} else if (family == AF_PACKET && ifa->ifa_data != NULL) {
                   struct rtnl_link_stats *stats = (rtnl_link_stats *)ifa->ifa_data;

                   printf("\t\ttx_packets = %10u; rx_packets = %10u\n"
                          "\t\ttx_bytes   = %10u; rx_bytes   = %10u\n",
                          stats->tx_packets, stats->rx_packets,
                          stats->tx_bytes, stats->rx_bytes);
               }
           }

           freeifaddrs(ifaddr);
           return(EXIT_SUCCESS);
       }

string NetworkUtils::get_ip_addr (string ifname) 
{
	struct ifaddrs *ifaddr, *ifa;
	int family, s;
  char host[NI_MAXHOST]={0};

	if (getifaddrs(&ifaddr) == -1) {
	 	 perror("getifaddrs");
	 	 exit(EXIT_FAILURE);
	}

	/* Walk through linked list, maintaining head pointer so we
	 	can free list later */

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL)
			continue;
		if(ifname.compare(ifa->ifa_name) != 0 ) 
			continue;

		family = ifa->ifa_addr->sa_family;
		if (family != AF_INET) 
			continue;
		/*Display interface name and family (including symbolic
		 	form of the latter for the common families) */
		
		/* For an AF_INET* interface address, display the address */
		
		s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), host, NI_MAXHOST,
		 	NULL, 0, NI_NUMERICHOST);
		if (s != 0) {
		 	printf("getnameinfo() failed: %s\n", gai_strerror(s));
		 	exit(EXIT_FAILURE);
		}
		
		return (string(host));
		
	}

	freeifaddrs(ifaddr);
	return(string(""));
}

string NetworkUtils::get_netmask (string ifname) 
{
	struct ifaddrs *ifaddr, *ifa;
	int family, s;
  char host[NI_MAXHOST]={0};
	struct sockaddr_in *sa;

	if (getifaddrs(&ifaddr) == -1) {
	 	 perror("getifaddrs");
	 	 exit(EXIT_FAILURE);
	}

	/* Walk through linked list, maintaining head pointer so we
	 	can free list later */

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL)
			continue;
		if(ifname.compare(ifa->ifa_name) != 0 ) 
			continue;

		family = ifa->ifa_addr->sa_family;
		if (family != AF_INET) 
			continue;
		/*Display interface name and family (including symbolic
		 	form of the latter for the common families) */
		
		/* For an AF_INET* interface address, display the address */
		sa = (struct sockaddr_in *) ifa->ifa_netmask;
		freeifaddrs(ifaddr);
    return string(inet_ntoa(sa->sin_addr));
		
	}

	return(string(""));
}

int NetworkUtils::set_ip_address(std::string if_name, std::string ip_addr) {
  if (if_name.empty()) {
    printf("ERROR : Please supply interface name\n");
    return -1;
  }
  int sockfd;
  const char * NETMASK="255.255.255.0";
  struct ifreq ifr;
  char * if_addr, *if_netmask;
  struct sockaddr_in *addr;

  /* AF_INET to define IPV4 */
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd > 0 ) {
    //printf("SOCKET created successfully\n");
  } else {
    printf("ERROR : socket creation failed\n");
    return -1;
  }

  ifr.ifr_addr.sa_family = AF_INET;
  // set interface name
  strncpy(ifr.ifr_name, if_name.c_str(), IFNAMSIZ);

  // get current address of interface
  ioctl(sockfd, SIOCGIFADDR, &ifr);
  addr = (struct sockaddr_in *)&ifr.ifr_addr;
  if_addr = inet_ntoa(addr->sin_addr);
  //printf("Curernt if_addr=%s\n",if_addr);
  //if current interface address is same as intended ip_addr, don't do anything
  if (ip_addr.compare(if_addr)) {
    // convert IP from string to network address
    inet_pton(AF_INET,ip_addr.c_str(), &addr->sin_addr);
    // set ip address of the interface
    ioctl(sockfd, SIOCSIFADDR, &ifr);
    //set netmask in sin_addr
    inet_pton(AF_INET, NETMASK, &addr->sin_addr);
    ioctl(sockfd, SIOCSIFNETMASK, &ifr);
  }
  
  close(sockfd);
  return 0;
}

string NetworkUtils::generate_client_ip_addr(string server_ip_addr) {
	in_addr_t address;
	struct in_addr address_struct;
 	int ret_val = inet_aton(server_ip_addr.c_str(), &address_struct);
	if( ret_val == 0 ) {
		char msg[1024];
		sprintf(msg,"Invalid server_ip_addr=%s given to NetworkUtils::generate_client_ip_addr\n",server_ip_addr.c_str());
		perror(msg);
	}

	// add one to the value (making sure to get the correct byte orders)
	address = ntohl(address_struct.s_addr);
	if ((address&255) < 250 ) { 
		address += 1;
	} else {
		address -= 1;
	}
	address = htonl(address);
	
	// pack the address into the struct inet_ntoa expects
	address_struct.s_addr = address;
	
	// convert back to a string
	return string(inet_ntoa(address_struct));
}

void NetworkUtils::assemble_dhcp_reply(uint8_t *buffer,int num_bytes_recvd, std::string client_ip_addr, std::string server_ip_addr, ph_packet_defs::MACAddress& mac_address, std::string netmask) 
{
  struct dhcp_command *dhcp_req_commands[MAX_DHCP_COMMANDS],*cmd;
  struct dhcp_command dhcp_reply_commands[MAX_DHCP_COMMANDS];
  int dhcp_command_index=0,current_offset;
	const char *netmask_cstr;

  if (buffer[VEND_HW_ADDR_OFFSET] != DHCP_CODE_VENDORSPECIFIC) {
    printf("ERROR : Did not find 0x2b at offset 0x%03x\n",VEND_HW_ADDR_OFFSET);
    exit(1);
  }

  int MAC_OFFSET = 28;
  int mac_size = sizeof(mac_address);
  for (int i=0;i < mac_size; i++) {
    *((uint8_t *)(&mac_address)+i) = buffer[MAC_OFFSET+i];
  }


  current_offset = VEND_HW_ADDR_OFFSET;
  while(current_offset < num_bytes_recvd) {
    dhcp_req_commands[dhcp_command_index] =cmd= (struct dhcp_command *) malloc(sizeof(struct dhcp_command));
    dhcp_command_index++;
    cmd->cmd = buffer[current_offset];
    if (cmd->cmd == DHCP_CODE_END ) {
      cmd->length = 0;
      break;
    }
    cmd->length = buffer[current_offset+1];
    cmd->data = (uint8_t *)malloc(sizeof(uint8_t)*cmd->length);
    memcpy(cmd->data,&buffer[current_offset+2],cmd->length);
    current_offset = current_offset+2+cmd->length;
  };


  /* setup dhcp_reply */
  // convert IP from string to network address
  inet_pton(AF_INET,client_ip_addr.c_str(), &buffer[YIADDR_OFFSET]);
  inet_pton(AF_INET,server_ip_addr.c_str(), &buffer[SIADDR_OFFSET]);
  //
	netmask_cstr = netmask.c_str();
  dhcp_reply_commands[0].cmd = DHCP_CODE_NETMASK;
  dhcp_reply_commands[0].length=4;
  dhcp_reply_commands[0].data = (uint8_t *)malloc(4);
	inet_pton(AF_INET,netmask.c_str(),dhcp_reply_commands[0].data);
  //dhcp_reply_commands[0].data[0] = *netmask_cstr;
  //dhcp_reply_commands[0].data[1] = *(netmask_cstr+1);
  //dhcp_reply_commands[0].data[2] = *(netmask_cstr+2);
  //dhcp_reply_commands[0].data[3] = *(netmask_cstr+3);
	printf("DHCP reply netmask= %x %x %x %x\n",dhcp_reply_commands[0].data[0],dhcp_reply_commands[0].data[1],
		dhcp_reply_commands[0].data[2],dhcp_reply_commands[0].data[3]);
  //
  dhcp_reply_commands[1].cmd = DHCP_CODE_ROUTER;
  dhcp_reply_commands[1].length=4;
  dhcp_reply_commands[1].data = (uint8_t *)malloc(4);
  bzero(dhcp_reply_commands[1].data,4);
  //
  dhcp_reply_commands[2].cmd = DHCP_CODE_MSGTYPE;
  dhcp_reply_commands[2].length=1;
  dhcp_reply_commands[2].data = (uint8_t *)malloc(1);
  dhcp_reply_commands[2].data[0] = DHCPACK;
  //
  dhcp_reply_commands[3].cmd = DHCP_CODE_LEASE_TIME;
  dhcp_reply_commands[3].length=4;
  dhcp_reply_commands[3].data = (uint8_t *)malloc(4);
  dhcp_reply_commands[3].data[0] = 0xFF;
  dhcp_reply_commands[3].data[1] = 0xFF;
  dhcp_reply_commands[3].data[2] = 0xFF;
  dhcp_reply_commands[3].data[3] = 0xFF;
  //
  dhcp_reply_commands[4].cmd = DHCP_CODE_SERVER;
  dhcp_reply_commands[4].length=4;
  dhcp_reply_commands[4].data = (uint8_t *)malloc(4);
  inet_pton(AF_INET,server_ip_addr.c_str(), dhcp_reply_commands[4].data);

  for(int i=0;i<dhcp_command_index;i++){
    dhcp_reply_commands[5+i].cmd = dhcp_req_commands[i]->cmd;
    dhcp_reply_commands[5+i].length = dhcp_req_commands[i]->length;
    if (dhcp_reply_commands[5+i].length > 0 ) {
      dhcp_reply_commands[5+i].data = dhcp_req_commands[i]->data;
    }
  }

  current_offset = VEND_HW_ADDR_OFFSET;
  for(int i=0;i<5+dhcp_command_index;i++){
    buffer[current_offset] = dhcp_reply_commands[i].cmd;
    if (buffer[current_offset] == DHCP_CODE_END) {
      continue;
    }
    buffer[current_offset+1] = dhcp_reply_commands[i].length;
    memcpy(&buffer[current_offset+2],dhcp_reply_commands[i].data,dhcp_reply_commands[i].length);
    current_offset += dhcp_reply_commands[i].length+2;
  }



  //mark op code as BOOTREPLY
  buffer[0] = 2;


}

int NetworkUtils::bootp_server(unsigned int server_port_num, unsigned int client_port_num, std::string if_name, std::string& client_ip_addr, std::string _server_ip_addr, unsigned int debug, ph_packet_defs::MACAddress& mac_address) {
	struct sockaddr_in cli_addr ;
	uint8_t buffer[BOOTP_BUFFER_SIZE];
  int num_bytes_recvd, num_bytes_sent;
  Udp udp(server_port_num);
	std::string server_ip_addr ;

  if (debug & DEBUG_CONNECTION) {
    printf("Starting bootp\n");
  }

	bzero((char *) &cli_addr, sizeof(cli_addr));
	cli_addr.sin_family = AF_INET;
	cli_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	cli_addr.sin_port = htons(client_port_num);

  udp.start();

	bzero(buffer,BOOTP_BUFFER_SIZE);
  num_bytes_recvd = udp.receive(buffer, BOOTP_BUFFER_SIZE);
	if (debug & DEBUG_CONNECTION) {
		printf("bind to socket successful\n");
	}

	if (num_bytes_recvd < 0 ) {
		perror("ERROR on recvfrom");
	} else if (debug & DEBUG_CONNECTION) {
		printf("Received %d bytes from client\n",num_bytes_recvd);
	}
  if (debug & DEBUG_DATA) {
	  printf("Client data = \n");
	  for(int i=0;i<num_bytes_recvd;i++) {
		  printf("%02x/%03d ",buffer[i],buffer[i]);
		  if ( (i%16) == 15 ) {
		 	  printf("\n");
		  }
	  }
	  printf("\n");
  }

	//once we get here ethernet interface must be up. 
	// Find out if it has an ip address
	string ip_addr = get_ip_addr(if_name);
	string netmask = get_netmask(if_name);
	printf("Got netmask=%s ip_addr=%s for if_name=%s\n",netmask.c_str(),ip_addr.c_str(),if_name.c_str());

	if (!ip_addr.empty()) {
		server_ip_addr =  ip_addr;
		if (debug & DEBUG_CONNECTION) {
			printf("get_ip_addr returned %s as ip_addr for interface %s\n",
					server_ip_addr.c_str(), if_name.c_str());
		}
	} else {
		server_ip_addr = _server_ip_addr;
		set_ip_address(if_name,server_ip_addr);
	}

	if (client_ip_addr.empty()) {
		client_ip_addr = generate_client_ip_addr(server_ip_addr);
		if (debug & DEBUG_CONNECTION) {
			printf("generate_client_ip_addr gave %s\n",client_ip_addr.c_str());
		}
	}


  assemble_dhcp_reply(buffer,num_bytes_recvd, client_ip_addr, server_ip_addr, mac_address, netmask);


  if(debug & DEBUG_DATA) {
	  printf("Server data = \n");
	  for(int i=0;i<num_bytes_recvd;i++) {
		  printf("%02x/%03d ",buffer[i],buffer[i]);
		  if ( (i%16) == 15 ) {
		 	  printf("\n");
		  }
	  }
	  printf("\n");
  }


  udp.enable_broadcast();
  udp.bind_to_device(if_name.c_str());


    
  num_bytes_sent=udp.send(buffer,num_bytes_recvd,&cli_addr);
  if( num_bytes_sent != num_bytes_recvd ) {
    printf("ERROR : sendto failed. expected to send %d bytes, sent %d bytes\n",num_bytes_recvd,num_bytes_sent);
    return -1;
  }

	return 0;
}
