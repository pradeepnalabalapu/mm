#ifndef __ATTRIBUTE_H__
#define __ATTRIBUTE_H__

#include "mm_message.h"
#include "ph_packet_defs.h"
#include "FLOATType.h"
#include "mm_data_buffer.h"
#include <cstdlib>
#include <stdio.h>
#include <string.h>


using namespace ph_packet_defs;
class Attribute : public mm_message 
{
  ph_packet_defs::OIDType type;
  uint16_t length;
  uint8_t  *dataP;
  void *valueP;
  size_t word_size;
  void set_value();
  public :
  Attribute(mm_data_buffer& buffer_, size_t word_size_=2);
  ph_packet_defs::OIDType get_attribute_id(){
    return type;
  }
  void set_word_size(size_t word_size_);
  size_t get_word_size(){
    return word_size;
  };
  int get_value_length() {
    return length;
  };
  void *get_valueP();
  void *get_rawDataP() { return dataP; }
  ~Attribute();
  void print();
};
#endif // __ATTRIBUTE_H__
