#include "MdsCreateEventReport.h"
#include <stdio.h>

MdsCreateEventReport::MdsCreateEventReport(mm_data_buffer& buffer_) :
  buffer(buffer_)
{ 
}

int MdsCreateEventReport::parse_message() 
{
  buffer_initial_offset = buffer.get_offset();
  //read SPpdu
  buffer.read16((uint16_t *)&sppdu,sizeof(sppdu));
  if(sppdu.session_id != 0xE100) {
    printf("ERROR : MdsCreateEventReport::parse_message sppdu.session_id=0x%02x NOT 0xE100\n",
        sppdu.session_id);
    return -1;
  }

  //read ROapdus
  buffer.read16((uint16_t *)&roapdus, sizeof(roapdus));
  if(roapdus.ro_type != ROIV_APDU) {
    printf("ERROR : MdsCreateEventReport::parse_message roapdus.ro_type is not ROIV_APDU\n");
    return -1;
  }

  //read ROIVapdu
  buffer.read16((uint16_t *)&roivapdu, sizeof(roivapdu));
  if (roivapdu.command_type != CMD_CONFIRMED_EVENT_REPORT) {
    printf("ERROR : MdsCreateEventReport::parse_message roivapdu.command_type=0x%02x, not CMD_CONFIFMED_EVENT_REPORT\n",
        roivapdu.command_type);
    return -1;
  }

  //read  Event Report Argument
  buffer.read16((uint16_t *)&event_report_arg.managed_object,sizeof(event_report_arg.managed_object));
  buffer.read32((uint32_t *)&event_report_arg.event_time, sizeof(event_report_arg.event_time));
  buffer.read16((uint16_t *)&event_report_arg.event_type, sizeof(event_report_arg.event_type));
  buffer.read16((uint16_t *)&event_report_arg.length, sizeof(event_report_arg.length));
  if(event_report_arg.managed_object.m_obj_class != NOM_MOC_VMS_MDS) {
    buffer.set_offset ( buffer_initial_offset);
    printf("ERROR : MdsCreateEventReport event_report_arg.managed_object class=0x%02x, not NOM_MOC_VMS_MDS\n",
        event_report_arg.managed_object.m_obj_class);
    return -1;
  }
  if(event_report_arg.event_type != NOM_NOTI_MDS_CREAT) {
    printf("ERROR : MdsCreateEventReport event_report_arg.event_type=0x%02x, \
        not NOM_NOTI_MDS_CREAT\n",event_report_arg.event_type);
    buffer.set_offset ( buffer_initial_offset);
    return -1;
  }
  //read MDSCreateInfo
  buffer.read16((uint16_t *)&managed_object,sizeof(managed_object));
  //read attributeList
  attribute_list = new AttributeListClass(buffer);
  attribute_list->parse_message();
  //attribute_list->print_attributes();
};

void MdsCreateEventReport::print_attributes() {
  attribute_list->print_attributes();
};





