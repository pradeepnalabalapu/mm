#include "ph_packet_defs.h"
#include "FLOATType.h"
#include <math.h>
#include <string.h>

//FLOATType::NEG_MAX = -1 << 24;
//FLOATType::NAN_MANTISSA =          0x7fffff;
//FLOATType::NRES_MANTISSA =         0x800000;
//FLOATType::INFINITY_MANTISSA =     0x7ffffe;
//FLOATType::NEG_INFINITY_MANTISSA = 0x800002;
using namespace ph_packet_defs;


FLOATType::FLOATType(int bits) {
  isNaN = false;
  isNRes = false;
  isInfinity = false;
  isNegInfinity = false;

  exponent = bits >> 24;
  mantissa = bits & 0xffffff;
  if(mantissa & 0x800000 ) { //if mantissa is negative
    mantissa -= NEG_MAX;
  }
  if(mantissa == NAN_MANTISSA) {
    isNaN = true;
    strcpy(buf,"NaN");
  } else if(mantissa == NRES_MANTISSA) {
    isNRes = true;
    strcpy(buf, "NRes");
  } else if (mantissa == INFINITY_MANTISSA) {
    isInfinity = true;
    strcpy(buf, "Infinity");
  } else if (mantissa == NEG_INFINITY_MANTISSA) {
    isNegInfinity = true;
    strcpy(buf, "-Infinity");
  } else {
    value = mantissa * pow(2,exponent);
    sprintf(buf,"%f",value);
  }
}

const char* const FLOATType::toString() {
  return buf;
}
