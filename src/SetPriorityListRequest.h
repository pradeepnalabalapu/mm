#ifndef __SETPRIORITYLISTREQUEST_H__
#define __SETPRIORITYLISTREQUEST_H__
/************************************************************************
  MDSSetPriorityList ::=
  <SPpdu>
  <ROapdus (ro_type := ROIV_APDU)>
  <ROIVapdu (command_type := CMD_CONFIRMED_SET)>
  <SetArgument
    (managed_object := {NOM_MOC_VMS_MDS, 0, 0})>
  <PollMdibDataReq>

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include <stdio.h>
#include <string.h>
using namespace ph_packet_defs;

class SetPriorityListRequest : public mm_message {
  mm_data_buffer& buffer;
  int buffer_initial_offset;


  std::vector<AttributeModEntry*> attributeModEntryVec;
  public:
  SPpdu sppdu;
  ROapdus roapdus;
  ROIVapdu roivapdu;
  SetArgument set_arg;
  SetPriorityListRequest(mm_data_buffer& buffer_) ;
  int build_message();

};

#endif // __SETPRIORITYLISTREQUEST_H__
