#ifndef __FLOATType_H__
#define __FLOATType_H__
#include <stdio.h>
  class FLOATType {
    float value;
    int exponent;
    int mantissa;
    static const int NEG_MAX = - (1 << 24);
    static const int NAN_MANTISSA =          0x7fffff;
    static const int NRES_MANTISSA =         0x800000;
    static const int INFINITY_MANTISSA =     0x7ffffe;
    static const int NEG_INFINITY_MANTISSA = 0x800002;
    bool isNaN;
    bool isNRes;
    bool isInfinity;
    bool isNegInfinity;
    char buf[256];
    public :
    FLOATType(int bits);
    const char* const toString();
  };
#endif // __FLOATType_H__
