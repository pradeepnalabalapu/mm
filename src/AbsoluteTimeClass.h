#ifndef __AbsoluteTimeClass_H__
#define __AbsoluteTimeClass_H__
#include "ph_packet_defs.h"
#include <stdio.h>

using namespace ph_packet_defs;

  class AbsoluteTimeClass {
    AbsoluteTime at;
    char buf[256];
    public :
    AbsoluteTimeClass(AbsoluteTime at_) ;
    const char* const toString();
  };




#endif //__AbsoluteTimeClass_H__
