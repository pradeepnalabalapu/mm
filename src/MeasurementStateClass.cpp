#include "MeasurementStateClass.h"

std::string MeasurementStateClass::toString() {
  std::string result("");
  for (uint16_t state_bit=0x8000; ; state_bit >>= 1) {
    if (enums::MeasurementState_map.find((MeasurementState_enum_t)state_bit) != enums::MeasurementState_map.end()
      && ((state&state_bit) != 0 ) ) {
      result += enums::MeasurementState_map[(MeasurementState_enum_t)state_bit];
      if (state_bit == 1) {
        break;
      } 
      result += std::string("|");
    }
    if (state_bit == 1) {
      break;
    }
  }
  return result;
};




