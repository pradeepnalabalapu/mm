#include "ReleaseResponse.h"
#include <stdio.h>


int ReleaseResponse::parse_message() 
{
  buffer_initial_offset = buffer.get_offset();
  uint8_t *initial_ptr = buffer.get_ptr();;
  //read sessionHeader.type
  buffer.read((uint8_t *)&sessionHeader.type,1);
  if(sessionHeader.type != DN_SPDU_SI) {
    printf("ERROR : ReleaseResponse::parse_message sessionHeader.type=0x%02x\n",
        sessionHeader.type);
    return -1;
  }
  //Find beginning of User Data
	/*
  uint8_t marker1[] = {0xbe, 0x80, 0x28, 0x80, 0x81};
  uint8_t marker2[] = {0xbe, 0x80, 0x28, 0x80, 0x02, 0x01, 0x02, 0x81};
  size_t buffer_size = buffer.get_length() - buffer.get_offset();
  uint8_t *userdata_ptr  = generic_functions::find_subsequence(initial_ptr,buffer_size,marker1,sizeof(marker1));
  size_t userdata_offset;
  if (userdata_ptr != NULL) {
    userdata_offset = (size_t)(userdata_ptr - initial_ptr) + sizeof(marker1);
  } else {
    userdata_ptr = generic_functions::find_subsequence(initial_ptr, buffer_size, marker2, sizeof(marker2));
    if (userdata_ptr == NULL) {
      printf("ERROR : AssoctionResponse::parse_message could not find marker for UserData\n");
      return -1;
    };
    userdata_offset = (size_t)(userdata_ptr - initial_ptr) + sizeof(marker2);
  }
  buffer.set_offset(userdata_offset);

  uint8_t asnlength;
  buffer.read(&asnlength,1);
  while (asnlength == 0xff) {
    buffer.read(&asnlength, 1);
  }

  buffer.read32((uint32_t *)&protocol_version);
  buffer.read32((uint32_t *)&nomenclature_version);
  buffer.read32((uint32_t *)&functional_units);
  buffer.read32((uint32_t *)&system_type);
  buffer.read32((uint32_t *)&startup_mode);
  option_listP = new AttributeListClass(buffer);
  option_listP->parse_message();
  supported_aprofilesP = new AttributeListClass(buffer);
  supported_aprofilesP->parse_message();


  //// Process Attributes
  std::vector<Attribute*>& attributes = supported_aprofilesP->get_attributes();
  Attribute* attributeP = *attributes.begin();
  OIDType type = attributeP->get_attribute_id();
  if(type != NOM_POLL_PROFILE_SUPPORT) {
    printf("ERROR : ReleaseResponse::parse_message - supported_aprofiles attribute_id is not NOM_POLL_PROFILE_SUPPORT\n");
    return -1;
  } 
  uint8_t *valueP = (uint8_t *)attributeP->get_rawDataP();

  mm_data_buffer aprofileBuf(valueP[0],attributeP->get_value_length());

  aprofileBuf.read32((uint32_t *) &ppsupport.poll_profile_revision);
  aprofileBuf.read32((uint32_t *) &ppsupport.min_poll_period);
  aprofileBuf.read32((uint32_t *) &ppsupport.max_mtu_rx);
  aprofileBuf.read32((uint32_t *) &ppsupport.max_mtu_tx);
  aprofileBuf.read32((uint32_t *) &ppsupport.max_bw_tx);
  aprofileBuf.read32((uint32_t *) &ppsupport.options);
  AttributeListClass *opt_pkgP = new AttributeListClass(aprofileBuf);
  opt_pkgP->parse_message();
  
  std::vector<Attribute*>& opt_attrs = opt_pkgP->get_attributes();
  if(opt_attrs.begin() != opt_attrs.end()) {
    attributeP = *opt_attrs.begin();
    type = attributeP->get_attribute_id();
    if(type != NOM_ATTR_POLL_PROFILE_EXT) {
      printf("ERROR : ReleaseResponse::parse_message - supported_aprofiles.optional_packages attribute_id is not NOM_ATTR_POLL_PROFILE_EXT\n");
      return -1;
    } else {
      printf("Found NOM_ATTR_POLL_PROFILE_EXT\n");
    }
    uint8_t *ext_poll_attrP = (uint8_t *)attributeP->get_rawDataP();
    mm_data_buffer ext_poll_buf(ext_poll_attrP[0],attributeP->get_value_length());
    ext_poll_buf.read32((uint32_t *)&ext_poll_attr_val.options,4);
  }
  //printf("ext_poll_attr_val.options=0x%08x\n",ext_poll_attr_val.options);

	*/
 

};



