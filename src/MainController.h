#ifndef __MAINCONTROLLER_H__
#define __MAINCONTROLLER_H__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <string>
#include "mm_data_buffer.h"
#include "ph_packet_defs.h"
#include "ConnectIndication.h"
#include "AssociationRequest.h"
#include "AssociationResponse.h"
#include "MdsCreateEventReport.h"
#include "MdsCreateEventResult.h"
#include "SinglePollDataRequest.h"
#include "SinglePollDataResult.h"
#include "GetPriorityListRequest.h"
#include "SetPriorityListRequest.h"
#include "ExtendedPollDataRequest.h"
#include "ExtendedPollDataResult.h"
#include "Udp.h"
#include "ReleaseRequest.h"
#include "ReleaseResponse.h"
#include "wave_info.h"
#include "BaseController.h"

#define CONNECT_INDICATION_SERVER_PORT 24005
#define CONNECT_INDICATION_CLIENT_PORT 24005

int main_control_program(std::string server_ip_addr, unsigned int client_port_num,  std::string client_ip_addr,  unsigned int debug) ;

extern void error(const char* msg);

class MainController : public BaseController {
  struct sockaddr_in cli_addr, out_server_addr;
  uint8_t buffer[BUFFER_SIZE];
  uint8_t write_buffer[BUFFER_SIZE];
  Udp* udpP;
  mm_data_buffer mbuf,wmbuf;
  int debug;
  ManagedObjectId cur_managed_object;
  SPpdu cur_sppdu;
  u_16 cur_invoke_id;
  int ext_poll_num;
  unsigned int output_port_num;
  unsigned int client_port_num;
  string client_ip_addr;
  unsigned int timeout_secs, timeout_usecs;
  time_t last_osd_check_timestamp;
  const time_t OUTPUT_SERVER_CHECK_PERIOD = 30; //in seconds
  const char VITAL = 0;
  const char WAVE = 1;

  public :
  MainController(string server_ip_addr, 
      unsigned int timeout_secs, unsigned int timeout_usecs,
      unsigned int output_port_num, unsigned int debug, string _client_ip_addr=string(""), unsigned int _client_port_num=0);
  ~MainController();
  void initialize_udp_agent(string client_ip_addr, unsigned int client_port_num);
  void connect_to_output_server(void);
  int whole_process();
  int sendAssocReq();
  int receive();
  void print_buffer(int num_bytes);
  int disconnect();
  int do_connect_indication();
  int create_association();
  int single_poll_data_request();
  int set_priority_list();
  int extended_poll_data_request();
  int extended_poll_data_result();
  virtual void setAttribute(Attribute& attribute); 
  unsigned int get_client_port_num() { return client_port_num; }
  

};

#endif // __MAINCONTROLLER_H__
