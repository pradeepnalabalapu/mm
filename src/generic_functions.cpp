#include <execinfo.h>
#include "generic_functions.h"

uint8_t *generic_functions::find_subsequence(uint8_t *haystack,
    size_t haystack_size, uint8_t *needle, size_t needle_size) {
  for(int i=0; i<= haystack_size - needle_size; i++ ){
    int j=0;
    for (j=0; haystack[i+j] == needle[j] ; j++);
    if ( j == needle_size) {
      //printf("found sub_sequence at i=0x%x\n",i);
      return &haystack[i];
    }
  }
  return NULL;
}

void generic_functions::print_stack_trace(int out_fd, int NUM_FUNCTIONS) {
	void *array[NUM_FUNCTIONS];
	size_t size;

	size = backtrace(array,NUM_FUNCTIONS);
	backtrace_symbols_fd(array,size,out_fd);
}
