#ifndef __EXTENDEDPOLLDATARESULT_H__
#define __EXTENDEDPOLLDATARESULT_H__
/************************************************************************

	MDSPollActionResult ::=
 	<SPpdu>
 	<ROapdus (ro_type := RORS_APDU)>
 	<RORSapdu (invoke_id := "mirrored from request message"
		command_type := CMD_CONFIRMED_ACTION)>
	<ActionResult
 		(managed_object := {NOM_MOC_VMS_MDS, 0, 0},
		action_type := NOM_ACT_POLL_MDIB_DATA_EXT)>
	<PollMdibDataReplyExt>


************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "RelativeTimeClass.h"
#include "AbsoluteTimeClass.h"
#include "AttributeListClass.h"
#include "BaseController.h"
#include <stdio.h>
#include <string.h>
using namespace ph_packet_defs;

class ExtendedPollDataResult : public mm_message {
  mm_data_buffer& buffer;
  int buffer_initial_offset;
  BaseController& mc;
  public:
  SPpdu sppdu;
  ROapdus roapdus;
  RORSapdu rorsapdu;
  ROLRSapdu rolrsapdu;
  PollMdibDataReplyExt data_reply;
  ActionResult action_res; 
  ManagedObjectId managed_object;

  ExtendedPollDataResult(mm_data_buffer& buffer_, BaseController& mc_) ;
  int parse_message();
  //void print_attributes();
  int readSingleContextData();

};

#endif // __EXTENDEDPOLLDATARESULT_H__
