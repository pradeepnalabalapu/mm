#include "ReleaseRequest.h"
#include <stdio.h>
//ReleaseRequest::ReleaseRequest(mm_data_buffer& buffer_) : buffer(buffer_) {
//}

uint8_t ReleaseRequest::ReleaseReqPresentationHeader[20] = {
  0xC1,0x16,0x61,0x80,0x30,0x80,0x02,0x01,
  0x01,0xA0,0x80,0x62,0x80,0x80,0x01,0x00,
  0x00,0x00,0x00,0x00 };

uint8_t ReleaseRequest::ReleaseReqPresentationTrailer[4] = {
  0x00,0x00,0x00,0x00 };

int ReleaseRequest::build_message() {


	sessionHeader.type = FN_SPDU_SI;
	sessionHeader.length = sizeof(ReleaseReqPresentationHeader) + 
    sizeof(ReleaseReqPresentationTrailer);
	printf("sessionHeader.length=0x%x\n",sessionHeader.length);
	buffer.write((uint8_t *)&sessionHeader,sizeof(sessionHeader));
	buffer.write((uint8_t *)&ReleaseReqPresentationHeader,sizeof(ReleaseReqPresentationHeader));
	buffer.write((uint8_t *)&ReleaseReqPresentationTrailer,
			sizeof(ReleaseReqPresentationTrailer));

	return 0;

}


