#ifndef __RELEASERESPONSE_H__
#define __RELEASERESPONSE_H__
/************************************************************************
  ReleaseResponseMessage ::=
  <AssocRespSessionHeader>
  <AssocRespSessionData>
  <AssocRespPresentationHeader>
  <AssocRespUserData>
  <AssorRespPresentationTrailer>

  AssocRespSessionHeader ::=
  <SessionHead (type := AC_SPDU_SI)>

  AssocReqUserData ::=
  <ASNLength>
  <MDSEUserInfoStd>

************************************************************************/
#include "mm_message.h"
#include "ph_packet_defs.h"
#include "generic_functions.h"
#include "mm_data_buffer.h"
#include "AttributeListClass.h"
#include <stdio.h>
#include <string.h>
#include "AssociationResponse.h"

using namespace ph_packet_defs;

class ReleaseResponse : public AssociationResponse {
  public :
    ReleaseResponse(mm_data_buffer& buffer_) : AssociationResponse(buffer_){
    };
    int parse_message();

};

#endif // __RELEASERESPONSE_H__
