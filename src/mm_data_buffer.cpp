#include "mm_data_buffer.h"
#include <cstdlib>
#include <stdint.h>
#include <stdio.h>

mm_data_buffer::mm_data_buffer(uint8_t& buffer, size_t length_) {
  data = &buffer;
  length = length_;
  offset = 0;
};

mm_data_buffer::mm_data_buffer(uint8_t* buffer, size_t length_) {
  data = buffer;
  length = length_;
  offset = 0;
};


uint8_t *mm_data_buffer::get_ptr(){
  return (uint8_t *)&(data[offset]);
};



uint8_t &mm_data_buffer::operator[](int i){
  if((i+offset)<0 || (i+offset) > length-1){
    cout << "ERROR : Index out of bounds : ";
    cout << "index="<<i<<"+"<<offset<<" size="<<length;
    exit(1);
  }
  return data[i+offset];
};

size_t mm_data_buffer::read(uint8_t * data_struct, size_t len)
{
  for(int i=0; i < len; i++) {
    *(((uint8_t *)data_struct)+i) = (*this)[i]; 
  }
  offset += len;
  return len;
}

size_t mm_data_buffer::read32(uint32_t * data_struct, size_t len)
{
  //std::cout << "read32 : len = "<<len<<std::endl;
  if((offset)<0 || (offset+len-1) > length-1){
    cout << "ERROR : Index out of bounds : ";
    cout << "index="<<(len-1)<<"+"<<offset<<" size="<<length;
    exit(1);
  }

  for(int i=0; i < int (len/4); i++) {
    data_struct[i] = ntohl(*((uint32_t *)(data+offset) + i)); 
  }
  offset += int(len/4)*4;
  return int(len/4)*4;
}

size_t mm_data_buffer::read16(uint16_t * data_struct, size_t len)
{
  //std::cout << "read16 : len = "<<len<<std::endl;
  if((offset)<0 || (offset+len-1) > length-1){
    cout << "ERROR : Index out of bounds : ";
    cout << "index="<<(len-1)<<"+"<<offset<<" size="<<length;
    exit(1);
  }

  for(int i=0; i < int(len/2); i++) {
    data_struct[i] = ntohs(*((uint16_t *)(data+offset) + i)); 
  }
  offset += int(len/2)*2;
  return int(len/2)*2;
}

size_t mm_data_buffer::write(uint8_t * data_struct, size_t len)
{
  for(int i=0; i < len; i++) {
    (*this)[i] = *(((uint8_t *)data_struct)+i) ; 
  }
  offset += len;
  return len;
}

size_t mm_data_buffer::write32(uint32_t * data_struct, size_t len)
{
  //std::cout << "read32 : len = "<<len<<std::endl;
  if((offset)<0 || (offset+len-1) > length-1){
    cout << "ERROR : Index out of bounds : ";
    cout << "index="<<(len-1)<<"+"<<offset<<" size="<<length;
    exit(1);
  }

  for(int i=0; i < int (len/4); i++) {
    *((uint32_t *)(data+offset) + i) = htonl(data_struct[i]);
  }
  offset += int(len/4)*4;
  return int(len/4)*4;
}

size_t mm_data_buffer::write16(uint16_t * data_struct, size_t len)
{
  //std::cout << "read16 : len = "<<len<<std::endl;
  if((offset)<0 || (offset+len-1) > length-1){
    cout << "ERROR : Index out of bounds : ";
    cout << "index="<<(len-1)<<"+"<<offset<<" size="<<length;
    exit(1);
  }

  for(int i=0; i < int(len/2); i++) {
    *((uint16_t *)(data+offset) + i) = htons(data_struct[i]); 
  }
  offset += int(len/2)*2;
  return int(len/2)*2;
};


void mm_data_buffer::print(){
  printf("Buffer data = \n");
  for(int i=0;i<offset;i++) {
    printf("%02x/%03d ",data[i],data[i]);
    if ( (i%16) == 15 ) {
      printf("\n");
    } else if ((i%8)==7) {
      printf("|| ");
    }
  }
  printf("\n");
};

