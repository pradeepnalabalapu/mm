#!/usr/bin/env python3

from websocket import create_connection
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import argparse
import json

TCP_IP = '127.0.0.1'
TCP_HOST = 'localhost'
TCP_PORT=3000
BUFFER_SIZE = 1024

if __name__ == "__main__" :

  parser = argparse.ArgumentParser(description='Process arguments')
  parser.add_argument('--ip', type=str, help='ip address of node server')
  parser.add_argument('--debug',dest='debug', action='store_true')
  parser.set_defaults(debug=False)
  args = parser.parse_args()
  if args.ip :
    TCP_IP = args.ip

  win = pg.GraphicsWindow()
  win.setWindowTitle('Pleth')
  p1 = win.addPlot()
  p1.setRange(yRange=[0,0xfff])
  plotdata = {}
  curves = {}
  ptr = 0
  ws = create_connection("ws://"+str(TCP_IP)+":"+str(TCP_PORT))
  def update1() :
    global plotdata, curves, ptr
    newpoint_json = ws.recv()
    newpoint = json.loads(newpoint_json)
    physio_id = newpoint['physio_id']
    newdata = newpoint['data']
    ts = newpoint['ts']
    
    if physio_id  not in plotdata :
      plotdata[physio_id] = [0]*1000
      curves[physio_id] = p1.plot()

    data = plotdata[physio_id]
    curve = curves[physio_id]
    if args.debug : print("Data : ",newpoint)
    data[:-1] = data[1:] #shift data to left by one sample
    data[-1] = int(newdata) # make value the last sample of data
    curve.setData(data)
    curve.setPos(ptr, 0)

  timer = pg.QtCore.QTimer()
  timer.timeout.connect(update1)
  timer.start(0)

  import sys

  if (sys.flags.interactive !=1) or not hasattr(QtCore, 'PYQT_VERSION'):
    QtGui.QApplication.instance().exec_()


