const net = require('net');
var Queue = require('./Queue.js');
const WebSocket = require('ws');

const PORT_INCOMING = 5000;

const VITAL = 0;
const WAVE = 1;

var queue = new Queue();



var server = net.createServer(function (socket) {
    socket.on('data', function(data) {
        var length = data.length;
        var i= 0;
        while (i < length) {
          var physio_id = data.readUInt16BE(i);
          var data_type_int = data.readUInt8BE(i+2);
          var data_type_str = data_type_int == WAVE ? 'WAVE' :
                              data_type_int == VITAL ? 'VITAL' : 
                              'UNKNOWN';


          i+=3
          process.stdout.write('physio_id  '+physio_id.toString(16)+' data_type '+data_type_str);
          for (var count=0; count < 32; count+= 1) {
            process.stdout.write(' '+data.readInt16BE(i).toString(16))
            queue.enqueue(JSON.stringify({'physio_id': physio_id, 'data': data.readInt16BE(i), 'ts' : Date.now()} ));
            i+=2
          }
          process.stdout.write('\n');
          console.log('');
        }
    });

  });

server.listen(PORT_INCOMING);

const wss = new WebSocket.Server({port : 3000});

var wss_connected = false;
var timer;
wss.on('connection', function connection(ws) {
    console.log('browser connected');
		ws.on('error', function() {
				console.log('errored');
				if(timer ) { clearInterval(timer);}
			}
		);
    wss_connected = true;
    timer = setInterval(function() {
				if(!queue.isEmpty()) {
					var val = queue.dequeue();
					ws.send(val, function ack(error) {
							if (typeof error !== 'undefined'  && error ) {
								console.log('websocket send encountered error : ', error);
								if (timer) {clearInterval(timer);}
							}
						});
				}
      }, 8);


  });




    



